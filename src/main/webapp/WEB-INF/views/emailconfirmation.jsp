<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Email Confirmation - KashmirConnect</title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="${web}/all.min.css">
        <link href="${css}/inform.css" rel="stylesheet">
            
    </head>
    <body>
    
    	<div class=container>
    		<div class="row">
    			<div class="col-md-3"></div>
    			<div class="col-md-6">
    				
    				<div class="outer">
    					<i class="fas fa-envelope head"></i>
    					<h2>Email Confirmation</h2><hr>
    						<p>${status}</p>
    						
    						<form action="/KashmirConnect/resend" method="POST">
    							<input type="hidden" name="firstName" value="${firstName}">
    							<input type="hidden" name="lastName" value="${lastName }">
    							<input type="hidden" name="password" value="${password}">
    							<input type="hidden" name="email" value="${email}">
    							<input type="hidden" name="uniqueCode" value="${uniqueCode}">
    							
    							<button type="submit" class="btn btn-primary">Resend Email</button>
    							
    						</form>
    					
   					</div>
    			</div>
    			<div class="col-md-3"></div>
    		</div>
    	</div>
         
    </body>
</html>
