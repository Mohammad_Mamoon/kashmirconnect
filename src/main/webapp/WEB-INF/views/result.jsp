

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Search Result - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		
		<link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
		
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
				
	</head>
	
	<body>
		
		<!-- Starting of Navbar -->
			<%@include file="./shared/navbar.jsp" %>
		
		<!-- navbar close-->
			
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>		
				<!-- side bar close -->
				
				<!-- body middle column-->
				
				<div class="col-md-7 middle_section">
						<form>
							 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 	<input type="hidden" name="role" id="user_role" value="${role}" class="form control">
						</form>
					
					
					<c:forEach items="${TourismList}" var="record">
						
				  		<div class="question_list">
											  			
							
							 <h5>
							 	<a href="/KashmirConnect/userprofile?email=${record.realUser.email }"><b>${record.realUser.userName}</b></a>
							 	<span>added Question at &nbsp;</span>${record.timeAsked}
							 </h5>	
   							
					 		<form action="/KashmirConnect/viewdetails" method="post" >
					 		    <input type="hidden" name="question" value="${record.question}">
								<input type="hidden" name="questionId" value="${record.questionId}">
								<input type="hidden" name="realUser.email" value="${record.realUser.email}">
								<input type="hidden" name="realUser.role" value="${record.realUser.role}">
								<input type="hidden" name="realUser.userName" value="${record.realUser.userName}">
								<input type="hidden" name="timeAsked" value="${record.timeAsked}">
					 		<button type="submit" class="btn btn-link btn-lg" >${record.question}</button>
					 		
					 		</form>
					 		
					 		
					 		
						 </div>
					</c:forEach>
					
					
					<c:forEach items="${MedicineList}" var="record">
						
				  		<div class="question_list">
											  			
							
							 <h5>
							 	<a href="/KashmirConnect/userprofile?email=${record.realUser.email }"><b>${record.realUser.userName}</b></a>
							 	<span>added Question at &nbsp;</span>${record.timeAsked}
							 </h5>	
   							
					 		<form action="/KashmirConnect/viewdetails" method="post" >
					 		    <input type="hidden" name="question" value="${record.question}">
								<input type="hidden" name="questionId" value="${record.questionId}">
								<input type="hidden" name="realUser.email" value="${record.realUser.email}">
								<input type="hidden" name="realUser.role" value="${record.realUser.role}">
								<input type="hidden" name="realUser.userName" value="${record.realUser.userName}">
								<input type="hidden" name="timeAsked" value="${record.timeAsked}">
					 		<button type="submit" class="btn btn-link btn-lg ">${record.question}</button>
					 		
					 		</form>
					 		
					 		
					 		
						 </div>
					</c:forEach>
					
							
				</div>
				
				<!-- body middle column close -->
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<!-- Model  -->
			<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		<script src="${js}/homee.js"></script>
					
	</body>
	
</html>

