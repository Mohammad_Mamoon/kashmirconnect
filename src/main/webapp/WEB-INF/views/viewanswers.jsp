<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>



<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>View Answers - KashmirConnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet"> 
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
		
	</head>
	
	<body>
		<!-- Starting of Navbar -->
			<%@include file="./shared/navbar.jsp" %>
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				<div class="col-md-7 middle_section">
					<div class="answer_space">
						<h5>Question Posted by <a href="#">${userName}</a></h5>
						<div class="ques">
							<h2>${record.question} </h2>
						</div>
					</div>
						
						
						<c:forEach items="${MedicineList}" var="answerRecords">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${answerRecords.realUser.userName}</b></a> <span>at</span> ${answerRecords.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									<p class="para_answer">${answerRecords.answer} </p>
						
						    	</div>
							
								
																		
								
							</div>
							</div>
						</c:forEach>
						
						<c:forEach items="${TourismList}" var="answerRecords">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${answerRecords.realUser.userName}</b></a> <span>at</span> ${answerRecords.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									<p class="para_answer">${answerRecords.answer} </p>
						
						    	</div>
							
								
																		
								
							</div>
							</div>
						</c:forEach>
						
						
						
						
						
					
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<script src="${js}/homee.js"></script>	
	</body>
</html>