<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Languages - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>

	</head>
	
	<body>
		
		<!-- Starting of Navbar -->
			<%@include file="./shared/navbar.jsp" %>
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				
				<!-- body middle column-->
				
				<div class="col-md-7 middle_section">
						
						
						 <form>
						 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 <input type="hidden" name="role" id="user_role" value="${role}" class="form control">
						 </form>
					
					
					<c:forEach items="${list}" var="record">
						
				  		<div class="question_list">
							<h5><a href="#">${record.realUser.userName}</a>&nbsp; <span>added Question at &nbsp;</span>${record.timeAsked}</h5>	
   							<h3><a href="#">${record.question}</a></h3>
					 		
					 		<form action="/KashmirConnect/viewanswers" method="POST">
								<input type="hidden" name="question" value="${record.question}">
								<input type="hidden" name="questionId" value="${record.questionId}">
								<input type="hidden" name="realUser.email" value="${record.realUser.email}">
								<input type="hidden" name="realUser.role" value="${record.realUser.role}">
								<input type="hidden" name="realUser.userName" value="${record.realUser.userName}">
								<input type="hidden" name="timeAsked" value="${record.timeAsked}">
								<input type="hidden" name="loggedInUserEmail" value="${email}">
								<input type="hidden" name="loggedInUserName" value="${userName}">
								<input type="hidden" name="tourismCommunity" value="">
								<input type="hidden" name="medicineCommunity" value="">
								<input type="hidden" name="environmentCommunity" value="">
								<input type="hidden" name="artsCommunity" value="">
								<input type="hidden" name="businessCommunity" value="">
								<input type="hidden" name="languagesCommunity" value="languages">
								<sec:csrfInput/>
								<button type="submit" class="btn btn-link btn-sm"style="margin-bottom: 10px; font-size: 18px;color: #444;"><span>View Answers</span></button>
							</form>
							
							<div class="form-group error_message">
								<c:if test="${not empty error}">
									<p>! ${error}</p>
								</c:if>
							</div>
						 </div>
					</c:forEach>		
				</div>
				
				<!-- body middle column close -->
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<!-- Model  -->
			<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		<script src="${js}/userrole.js"></script>	
					
	</body>
	
</html>

