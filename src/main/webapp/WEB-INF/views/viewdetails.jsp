<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>



<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ViewDetails - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet"> 
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
		
	</head>
	
	<body>
		<!-- Starting of Navbar -->
			<%@include file="./shared/navbar.jsp" %>
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				<div class="col-md-7 middle_section">
					
					<div class="answer_space">
						<h5>Question Posted by <a href="#">${userName}</a></h5>
						<div class="ques">
							<h2>${question} </h2>
						</div>
					</div>
						
						
						<c:forEach items="${TourismList}" var="answerRecords">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${answerRecords.realUser.userName}</b></a> <span>at</span> ${answerRecords.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									<p class="para_answer">${answerRecords.answer} </p>
									<form style="display:inline-block;" action ="/KashmirConnect/editcomment" method="POST">
									<c:set var="principalEmail" scope="session" value="${principalEmail}" />
									<c:if test = "${principalEmail == answerRecords.realUser.email }" >
										<div class="div_txtarea_answer" style="display:none; "><textarea class="form-control  newcomment" name="editComment" >${answerRecords.answer}</textarea></div>	
										<input type="hidden" id="preanswer" name="answerId" value="${answerRecords.answerId}">
										<button type="button" style="display:none;" class="btnCancel btn btn-link" onclick="canceleditAnswer(this)">Cancel</button>
										<button type="button"  class="btnEdit btn btn-link edit_btn"  onclick="editAnswer(this)">Edit</button>
										<button type="submit" style="display:none;" onclick= "editComment(this)"  class="btnUpdate btn btn-link">Save</button>
									</c:if>
									</form>
									<form  style="display:inline-block;" action="/KashmirConnect/deletecomment" method="POST">
									<c:set var="principalEmail" scope="session" value="${principalEmail}" />
									<c:if test = "${principalEmail == answerRecords.realUser.email }" >
									
										<input type="hidden"  name="answerId" value="${answerRecords.answerId}">
										<input type="hidden"  name="questionId" value="${answerRecords.tourismQuestions.questionId}">
										<button type="submit"  class="btnDelete btn btn-link delete_btn">Delete</button>
										
									</c:if>	
									</form>
									

								

							</div>
							
								
																		
								
							</div>
							</div>
						</c:forEach>				
					
					
					
					
					
					
						
						
						<c:forEach items="${MedicineList}" var="answerRecords">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${answerRecords.realUser.userName}</b></a> <span>at</span> ${answerRecords.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									<p class="para_answer">${answerRecords.answer} </p>
									<form action="/KashmirConnect/editcomment" method="POST">
									<c:set var="principalEmail" scope="session" value="${principalEmail}" />
									<c:if test = "${principalEmail == answerRecords.realUser.email }" >
										<div class="div_txtarea_answer" style="display:none;"><textarea class="form-control" name="editComment">${answerRecords.answer}</textarea></div>	
										<input type="hidden" name="questionId" value="${answerRecords.medicineQuestions.questionId}">
										<input type="hidden" name="answer" value="${answerRecords.answer}">
										<input type="hidden" name="question" value="${answerRecords.medicineQuestions.question}">
										<input type="hidden" name="realUser.userName" value="${answerRecords.realUser.userName}">
										<button type="button" style="display:none;" class="btnCancel btn btn-link" onclick="canceleditAnswer(this)">Cancel</button>
										<button type="button" class="btnEdit btn btn-link edit_btn"  onclick="editAnswer(this)">Edit</button>
										<button type="submit" style="display:none;" class="btnUpdate btn btn-link">Save</button>
									</c:if>
									</form>
									<form action="/KashmirConnect/deletecomment" method="POST">
									<c:set var="principalEmail" scope="session" value="${principalEmail}" />
									<c:if test = "${principalEmail == answerRecords.realUser.email }" >
									
										<input type="hidden" name="answer" value="${answerRecords.answer}">
										<input type="hidden" name="loggedInUserEmail" value="${loggedInUserEmail}">
										<input type="hidden" name="questionId" value="${answerRecords.medicineQuestions.questionId}">
										<input type="hidden" name="question" value="${answerRecords.medicineQuestions.question}">
										
										<button type="submit" class="btnDelete btn btn-link delete_btn">Delete</button>
										
									</c:if>	
									</form>

								

							</div>
							
								
																		
								
							</div>
							</div>
						</c:forEach>
						
						
						
						
						
							 
						
					
						<c:if test = "${principalRole == role }" >
						<div class="post_here">
							<div class="postanswer">
							<textarea name="answer" class="form-control add_answer" placeholder="Write your comment here."></textarea>
							<button type="submit" class="btn btn-primary" onclick= "saveAnswer()" >Add Comment</button>
							</div>
							<form>
							<input type="hidden" name="question" value="${question}">
							<input type="hidden" id="question_id" name="questionId" value="${questionId}">
							<input type="hidden" id="loggedInUserEmail" name="loggedInUserEmail" value="${loggedInUserEmail}">
							</form>
							
						</div>
						</c:if>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<script src="${js}/answer.js"></script>
	
	</body>
</html>