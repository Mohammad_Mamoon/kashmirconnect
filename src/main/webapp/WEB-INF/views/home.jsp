

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<title>Home - KashmirConnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		<link href="${css}/home.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
		
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/jquery.autocomplete.min.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		<style type="text/css">
			.autocomplete-suggestions { border: 1px solid #999;  background: #FFF; overflow: auto; }
			.autocomplete-suggestion { padding: 5px 5px; white-space: nowrap; overflow: hidden; font-size:14px}
			.autocomplete-selected { background: #F0F0F0; }
			.autocomplete-suggestions strong { font-weight: bold; color: #3399FF; }
		</style>
		
	</head>
	
	<body>
		
		<!-- Starting of Navbar -->
		
			<%@include file="./shared/navbar.jsp" %>
		
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				
				<!-- body middle column-->
				
				<div class="col-md-7 middle_section">
					<div class="question_space">
						<h5><a href="/KashmirConnect/userprofile?email=${email }" data-id="1"   class="anchor_user_profile" target="_blank"> <b >${userName}</b></a><span>  from </span> <b> ${role}</b></h5>
						
						<h3><a href="#" class="anchor_ask_question"><b>What is your Question?</b></a></h3>
						<form>
							 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 	
						</form>
					</div>
					
					<c:forEach items="${list}" var="record">
						
				  		<div class="question_list">
											  			
							
							 <h5>
							 	<a href="/KashmirConnect/userprofile?email=${record.realUser.email }"><b>${record.realUser.userName}</b></a>
							 	<span>added Question at &nbsp;</span>${record.timeAsked}
							 	<c:if test="${email == record.realUser.email }">
							 	<span><a href="" class="glyphicon glyphicon-remove pull-right " ></a></span>
							 	</c:if>
							 </h5>	
   							<div class="question_post">
   							<h3><a href="#" class="equestion">${record.question}</a></h3>
					 			
								<form style="display:inline-block;">
									<c:if test="${email == record.realUser.email }">		
										<div class="div_txtarea_answer" style="display:none; "><textarea class="form-control  newcomment" name="editQuestion" >${record.question}</textarea></div>	
										<input type="hidden" id="lastmn" name="questionId" value="${record.questionId}">
										<button type="button" style="display:none;" class="btnCancel btn btn-link" onclick="canceleditAnswer(this)">Cancel</button>
										<button type="button"  class="btnEdit btn btn-link edit_btn"  onclick="editAnswer(this)">Edit</button>
										<button type="submit" style="display:none;" onclick= "editquestions(this)"  class="btnUpdate btn btn-link">Save</button>
									</c:if>
								</form> 	
						 	
					 			
								<form style="display:inline-block;" action="/KashmirConnect/answer" method="GET" >
								
								<input type="hidden" name="questionId" value="${record.questionId}">
								<input type="hidden" id="community" name="realUser.role" value="${record.realUser.role}">
								
									<button type="submit" class="btn btn-link btn-sm"style="margin-bottom: 10px; font-size: 18px;color: #444;"><span class="glyphicon glyphicon-pencil">Answer</span></button>
								</form> 
							</div>	
							<div class="quesvotes">
									<a href="#" onclick = "vote(this,'like',${record.questionId})"><span class="glyphicon glyphicon-thumbs-up up">Like<span class="badge">${record.upvotesCount}</span></span></a>
									<a href="#" onclick="vote(this,'unlike',${record.questionId})"><span class="glyphicon glyphicon-thumbs-down down">Dislike<span class="badge">${record.downvotesCount}</span></span></a>
									<c:if test="${email != record.realUser.email }">	
							 			<a href="#" onclick="vote(this,'flag',${record.questionId})"><span class="glyphicon glyphicon-flag pull-right flags">Flag<span class="badge">${record.flagCount}</span></span></a>
									</c:if>
									<a href="#" onclick="vote(this,'bookmark',${record.questionId})"><span class="down">Bookmark Question<span class="badge">${record.bookmarkCount}</span></span></a>
							</div>
							<div class="form-group error_message">
									<c:if test="${not empty error}">
										<p>! ${error}</p>
									</c:if>
							</div>
						 </div>
					</c:forEach>		
				</div>
				
				<!-- body middle column close -->
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<!-- Model  -->
				<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		
			
					
		
		
		<script src="${js}/homee.js"></script>			
	</body>
	
</html>

