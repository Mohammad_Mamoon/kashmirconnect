

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css"/>

<!DOCTYPE html>
<html>
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login - Kashmir Connect</title>
        <link href="${css}/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="${web}/all.min.css">
        <link href="${css}/logincss.css" rel="stylesheet">
       	
       	
       	<script src="${js}/jquery.js"></script>
       	<script src="${js}/bootstrap.min.js"></script>
       	<script src="${js}/jquery.validate.min.js"></script>
       	<script src="${js}/additional-methods.js"></script>
       	<script src="${js}/loginscript.js"></script>
       	
       	
    </head>

    <body>
        <div class="top-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
            	        <div class="col-md-5">
                	        <div class="textouter">
                    	        <div class="text-top">
                        	        <h1>KASHMIR CONNECT</h1>
                            	    <hr>
                            	</div>
                            	<div class="text-middle">
                                	<p>KashmirConnect is about connecting people of specific
										professions and fields together so that there is ready help when
										a person comes across with any problem.The idea is to ask 
										questions which are specific to Kashmir and even questions
										which are pertinent to other places can also be asked.
										It is a place where we can come and find answers who are meant to be
										 precise and accurate through your contribution.</p>
                            	</div>
                            	<div class="navbox">
                                	<br>
                                	<p>Copyright &copy;&nbsp;&nbsp;&nbsp;2018 KashmirConnect</p>
                                	<p><a href="/KashmirConnect/contact">Contact</a>&nbsp;&nbsp;&nbsp;
                                    <a href="/KashmirConnect/about">About</a></p>  
                            	</div>
                        	</div>
                    	</div>
                    	<div class="col-md-5">
                        	<div class="outer">
                        		<c:url value="/login"  var="loginVar"/>
                            	<form role="form" action="${loginVar}" method="POST" id="registration-form" >
                                	<fieldset>
                                    	<div class="form-top">
                                        	<div class="form-top-left">
                                            	<h1>LogIn</h1>
                                        	</div>
                                        	<div class="form-top-right">
                                            	<i class="fas fa-user-lock"></i>
                                        	</div>
                                    	</div>
                                    	<div class="form-bottom">
                                        	<div class="form-group">
                                            	<label for="email">Email</label>
                                            	<input type="email" name="email" placeholder="Enter Valid Email..." class="email form-control" id="email" autocomplete="on">
                                        	</div>
                                        	<div class="form-group inner-addon right-addon">
                                            	<label for="password">Password</label>
                                            	<input type="password" name="password" placeholder="Enter Password..." class="password form-control" id="password">
                                            	<span class="glyphicon glyphicon-eye-open"></span>
                                        	</div>

											<c:if test="${not empty error}">
												<div class="error">${error}</div>
											</c:if>

											<c:if test="${not empty msg}">
												<div class="msg">${msg}</div>
											</c:if>
											<sec:csrfInput/>
                                        	<button type="submit" class="btn btn-success btn-block">Login</button>
                                    	</div>            
                                	</fieldset>
                            	</form>
                            	<div class="login-end">
                                	<a href="#">Forget Password?</a>
                            	</div>
                        	</div>
                    	</div>
                    	<div class="col-md-1"></div>
                	</div>
            	</div>
        	</div>
                
                
    </body>
</html>
