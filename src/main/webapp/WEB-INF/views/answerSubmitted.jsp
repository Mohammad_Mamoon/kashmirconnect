
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>



<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />

<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Answer - Kashmir connect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		<link href="${css}/home.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
				
	</head>
	<body>
		<!-- Navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
				  	</button>
					<a class="navbar-brand" href="home.jsp">KashmirConnect</a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active"><a href="home.jsp">Home</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-pencil"></span>Answer</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-bell"></span>Notifications</a></li>
						<li>
							<form class="navbar-form" method="POST" role="search">
								<div class="input-group search">
									<input type="text" class="form-control" placeholder="Search" name="searches" required>
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							</form>
						</li>
						<li>
							<p class="navbar-btn">
								<a href="#" class="anchor_ask_question btn btn-danger">Add Question</a>
							</p>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><span class="glyphicon glyphicon-user blue"></span><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Profile</a> </li>
								<li><a href="#">Blogs</a> </li>
								<li><a href="#">Messages</a> </li>
								<li><a href="#">Setting</a> </li>
								<li><a href="<c:url value="/logout" />">Logout</a> </li>
							</ul>
						</li>
					</ul>
					
				</div>
			</div>
		</nav>
		<!-- end Navbar -->
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!--Side Bar-->
				<div class="col-md-3 content-left">
					<div class="recent">
						<h4>Feeds</h4>
						<ul id="feedlist">
							<li><a href="home.html">Top Stories</a></li>
							<li><a href="newquestions.html">New Questions</a></li>
							<li><a href="bookmarked.html">Bookmarked Answers</a></li>
							<li><a href="links.html">Links</a></li>
						</ul>
					</div>
					<div class="archives">
						 <h4>Communities</h4>
						 <ul id="communitylist">
							 <li><a href="#">Environment</a></li>
							 <li><a href="#">Tourism</a></li>
							 <li><a href="#">Arts</a></li>
							 <li><a href="#">Medicine</a></li>
							 <li><a href="#">Languages</a></li>
							 <li><a href="#">Business</a></li>
						 </ul>
					 </div>
				</div>
				<!-- side bar end -->
				<div class="col-md-8 middle">
					<div id="addanswer" class="content-grid-info">
						<div class="post-info add" >
							<h4>${userName}</h4>
							<h2>${question} </h2>
							<form action="/KashmirConnect/submitanswer" method="POST">
								<div class="page-wrapper box-content">
									<!--  <textarea class="cont" name="answer" required=""></textarea>-->
									<div >
										<textarea name="answer" ></textarea>
									</div>
									<input type="hidden" name="question" value="${question}">
									<input type="hidden" name="questionId" value="${questionId}">
									<input type="hidden" name="emailId" value="${emailId}">
									<input type="hidden" name="userName" value="${userName}">
									<input type="hidden" name="timeAsked" value="${timeAsked}">
									<input type="hidden" name="loggedInUserEmail" value="${loggedInUserEmail}">
									
								</div>
									
								<c:if test="${status != true }">
									
							  		 
								</c:if>
							</form>
						</div>
					</div>
						
					<c:forEach items="${list}" var="answerRecords">
						<div class="content-grid-info">
							<div class="post-info">
								<h4><b>${answerRecords.realUser.userName}</b> <span> &nbsp;added this &nbsp;</span> ${answerRecords.timeAnswered}</h4>
								<h2>${answerRecords.answer} </h2>
								<c:set var="principalEmail" scope="session" value="${principalEmail}" />
								<c:if test = "${principalEmail == answerRecords.realUser.email }" >
									
									<button type="button" class="btn btn-danger">Delete</button>
									<button type="button" class="btn btn-primary">Edit</button> 
								</c:if>	
							</div>
						</div>
					</c:forEach>													
				</div>
			</div>
		</div>
			
		<script>
			jQuery(document).ready(function($) {
				$('.content').richText();
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
				
			function saveQuestion(){
				
				var question = $('.content').richText()[0].value;
					
				$.ajax({
					type  :  "POST",
					url   :  "/KashmirConnect/submitquestion",
					data  : {question:question}
				});
			}
				
			$('#myModal').on('hidden.bs.modal', function () {
				$('.content').unRichText();
					
			});
				
			$(".anchor_ask_question").click(function(){
				$('.content').richText();
					
				$("#myModal").modal();
			});
			
			
		</script>
			
	</body>
</html>