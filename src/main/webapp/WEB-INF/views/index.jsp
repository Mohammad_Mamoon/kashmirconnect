<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />

<!DOCTYPE html>
<html>
	<head>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>SignUp - KashmirConnect</title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="${web}/all.min.css">
		<link href="${css}/custom.css" rel="stylesheet">


		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/jquery.validate.min.js"></script>
		<script src="${js}/additional-methods.js"></script>
		<script src="${js}/signupscript.js"></script>
	
	<style>
		#errormessage{
			padding:15px 25px 15px 25px;
		}
	</style>
	</head>

	<body>
		<div class="top-content">
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
						<div class="col-md-5">
							<div class="textouter">
								<div class="text-top">
									<h1>KashmirConnect</h1>
									<h5>Answers to your Questions!</h5>
									<hr>
								</div>
								<div class="text-middle">
									<p>KashmirConnect is about connecting people of specific
										professions and fields together so that there is ready help when
										a person comes across with any problem.The idea is to ask 
										questions which are specific to Kashmir and even questions
										which are pertinent to other places can also be asked.
										It is a place where we can come and find answers who are meant to be
										 precise and accurate through your contribution.</p>
								</div>
								<div class="navbox">
									<br>
									<p>Copyright &copy;2018 KashmirConnect</p>
									<p>
									<a href="/KashmirConnect/contact">Contact</a>&nbsp;&nbsp;&nbsp;
									<a href="/KashmirConnect/about">About</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="outer">
							
							<form role="form" action="/KashmirConnect/signup" method="post" id="registration-form">
								<fieldset>
									<div class="form-top">
										<div class="form-top-left">
											<h1>Sign Up</h1>
											
										</div>
										<div class="form-top-right">
											<i class="fa fa-user"></i>
										</div>
									</div>
									<div class="form-bottom">
										<div class="form-group">
											<label for="firstName">First name</label> <input type="text"
											name="firstName" placeholder="Enter First name..."
											class="firstName form-control" id="firstName">
										</div>

										<div class="form-group">
											<label for="lastName">Last name</label> <input type="text"
											name="lastName" placeholder="Enter Last name..."
											class="lastName form-control" id="lastName">
										</div>
										<div class="form-group">
											<label for="email">Email</label> <input type="email"
											name="email" placeholder="Enter Valid Email..."
											class="email form-control" id="email">
										</div>
										<div class="form-group error_message">
											<c:if test="${not empty message}">
												<p>! ${message}</p>
											</c:if>
											<c:if test="${not empty error}">
												<p>! ${error}</p>
											</c:if>
										</div>
										<div class="form-group inner-addon right-addon">
											<label for="password">Password</label> <input type="password"
											name="password" placeholder="Enter Password..."
											class="password form-control" id="password">
											<span class="glyphicon glyphicon-eye-open"></span>
										</div>
										<div class="form-group">
											<label for="conpassword">Confirm password</label> <input
											type="password" name="conpassword"
											placeholder="Enter Password Again..."
											class="conpassword form-control" id="conpassword">
											
										</div>
										<sec:csrfInput />
										<button type="submit" class="btn btn-success btn-block">Sign Up</button>
									</div>
								</fieldset>
							</form>
							<div class="login-end">
								<b style="color: #337ab7;">Already an existing user?</b><a href="/KashmirConnect/home"><button class="btn btn-info">Login</button></a>
								
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
</body>
</html>
