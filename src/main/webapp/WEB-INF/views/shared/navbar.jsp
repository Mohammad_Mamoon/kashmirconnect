
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
				  	</button>
					<a class="navbar-brand" href="${contextRoot }/KashmirConnect/home"><b>KashmirConnect</b></a>
				</div>
				
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li><a href="${contextRoot }/KashmirConnect/home">Home</a></li>
						<li><a href="${contextRoot }/KashmirConnect/unanswered"><span class="glyphicon glyphicon-pencil"></span>Answer</a></li>

					<!-- notify -->
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"><span class="glyphicon glyphicon-bell"></span>Notification
							</a>
						<ul class="dropdown-menu notify-drop">
							<div class="notify-drop-title">
							
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										Notifications
									</div>
									
								</div>
							</div>
							<!-- end notify title -->
							<!-- notify content -->
							<div class="drop-content">
								<li>
									<c:forEach items="${tourismNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.tourismQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

									</div>
									<hr>
									
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.tourismQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

									</div>
									<hr>
									
									</c:if>
									</c:forEach>
									
									<c:forEach items="${medicineNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="/KashmirConnect/answer?questionId=${answerRecords.medicineQuestions.questionId}&realUser.role=${answerRecords.realUser.role}">You have answered your own question ${answerRecords.medicineQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

									</div>
									<hr>
									</c:if>
									
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="/KashmirConnect/answer?questionId=${answerRecords.medicineQuestions.questionId}&realUser.role=${answerRecords.realUser.role}">${answerRecords.realUser.userName} has answered your question ${answerRecords.medicineQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

									</div>
									<hr>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${businessNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.businessQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

									</div>
									<hr>
									</c:if>
									
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.businessQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									
									</c:forEach>
									
									<c:forEach items="${environmentNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.environmentQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.environmentQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>


									</div>
									<hr>
									</c:if>
									</c:forEach>
									
									
									<c:forEach items="${artsNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.artsQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.artsQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${languagesNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.languagesQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.languagesQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										
									</div>
									<hr>
									</c:if>
									</c:forEach>
								</li>

							</div>
							
							<div class="notify-drop-footer text-center">
								<a href="/KashmirConnect/notifications"><i class="fa fa-eye"></i> See All</a>
							</div>
						</ul></li>
					<!-- nototifyend -->
						
						<li>
							<form action="/KashmirConnect/searchstring" class="navbar-form" method="POST" role="search">
								<div class="input-group search">
									<input type="text" class="form-control" placeholder="Search" name="searchString" autocomplete="ON" required>
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							</form>
						</li>
						<li>
							<p class="navbar-btn">
								<a href="#" class="anchor_ask_question btn btn-danger">Add Question</a>
							</p>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
							<span><img src="${imag }/profilesetting.png" class="img-circle"></span></a> 
							<ul class="dropdown-menu">
								<li class="bg-danger"><a href="#">Profile</a> </li>
								<li class="bg-info"><a href="#">Blogs</a> </li>
								<li class="bg-danger"><a href="#">Messages</a> </li>
								<li class="bg-info"><a href="#">Setting</a> </li>
								<li class="bg-danger"><a href="<c:url value="/logout" />">Logout</a> </li>
							</ul>
						</li>
					</ul>
					
				</div>
				
			</div>
		</nav>