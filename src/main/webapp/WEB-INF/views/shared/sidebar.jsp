
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<div class="col-md-2 side_bar">
	<div class="story_list">
		<h4>Feeds</h4>
		<a href="/KashmirConnect/mostlikedquestions">Most Liked Questions</a><br>
		<a href="/KashmirConnect/oldquestions">Old Questions</a><br>
		<a href="/KashmirConnect/viewbookmarkedquestions">Bookmark Questions</a><br>
	</div>
	<div class="community_list">
		 <h4>Communities</h4>
						 
		 <c:if test = "${role == 'environment' }" >
		 <a href="/KashmirConnect/tourism">Tourism</a><br>
		 <a href="/KashmirConnect/arts">Arts</a><br>
		 <a href="/KashmirConnect/medicine">Medicine</a><br>
		 <a href="/KashmirConnect/languages">Languages</a><br>
		 <a href="/KashmirConnect/business">Business</a><br>		
		 </c:if>
					 
		 <c:if test = "${role == 'tourism' }" >
		 <a href="/KashmirConnect/environment">Environment</a><br>	
		 <a href="/KashmirConnect/arts">Arts</a><br>
		 <a href="/KashmirConnect/medicine">Medicine</a><br>
		 <a href="/KashmirConnect/languages">Languages</a><br>
		 <a href="/KashmirConnect/business">Business</a><br>		
		 </c:if>
		 
		 
		 <c:if test = "${role == 'arts'}" >
		 <a href="/KashmirConnect/tourism">Tourism</a><br>
		 <a href="/KashmirConnect/environment">Environment</a><br>	
		 <a href="/KashmirConnect/medicine">Medicine</a><br>
		 <a href="/KashmirConnect/languages">Languages</a><br>
		 <a href="/KashmirConnect/business">Business</a><br>			
		 </c:if>
		 
		 
		 <c:if test = "${role == 'medicine' }" >
		<a href="/KashmirConnect/tourism">Tourism</a><br>
		 <a href="/KashmirConnect/arts">Arts</a><br>
		 <a href="/KashmirConnect/environment">Environment</a><br>	
		 <a href="/KashmirConnect/languages">Languages</a><br>
		 <a href="/KashmirConnect/business">Business</a><br>			
		 </c:if>
		 
		 <c:if test = "${role == 'languages' }" >
		<a href="/KashmirConnect/tourism">Tourism</a><br>
		 <a href="/KashmirConnect/arts">Arts</a><br>
		 <a href="/KashmirConnect/medicine">Medicine</a><br>
		 <a href="/KashmirConnect/environment">Environment</a><br>	
		 <a href="/KashmirConnect/business">Business</a><br>		
		 </c:if>	
		 
		 <c:if test = "${role == 'business' }" >
		 <a href="/KashmirConnect/tourism">Tourism</a><br>
		 <a href="/KashmirConnect/arts">Arts</a><br>
		 <a href="/KashmirConnect/medicine">Medicine</a><br>
		 <a href="/KashmirConnect/languages">Languages</a><br>
		 <a href="/KashmirConnect/environment">Environment</a><br>			
		 </c:if>
					
	</div>
</div>
				