<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>



<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Notifications - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		
		
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet"> 
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
		
	</head>
	
	<body>
		<!-- Starting of Navbar -->
		
			<%@include file="./shared/navbar.jsp" %>
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				<div class="col-md-7 middle_section">
				
					<div class="titlenotify">
						<h3>Notification Timeline</h3>
						<form>
							 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 	<input type="hidden" name="role" id="user_role" value="${role}" class="form control">
						</form>
					</div>
				<br>
				
					<div class="drop-content">
								
									<c:forEach items="${tourismNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.tourismQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.tourismQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${medicineNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="/KashmirConnect/answer?questionId=${answerRecords.medicineQuestions.questionId}&realUser.role=${answerRecords.realUser.role}">You have answered your own question ${answerRecords.medicineQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="/KashmirConnect/answer?questionId=${answerRecords.medicineQuestions.questionId}&realUser.role=${answerRecords.realUser.role}">${answerRecords.realUser.userName} has answered your question ${answerRecords.medicineQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${businessNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.businessQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.businessQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${environmentNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.environmentQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.environmentQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${artsNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.artsQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.artsQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
									
									<c:forEach items="${languagesNotifyList}" var="answerRecords">
									
									<c:if test="${email == answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">You have answered your own question ${answerRecords.languagesQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									<c:if test="${email != answerRecords.realUser.email }">
									<div class="col-md-9 col-sm-9 col-xs-9 pd-l0">
										<a href="">${answerRecords.realUser.userName} has answered your question ${answerRecords.languagesQuestions.question} , at this 
										time ${answerRecords.timeAnswered} </a> <a href="" class="rIcon"><i
											class="fa fa-dot-circle-o"></i></a>

										<hr>
										
										
									</div>
									</c:if>
									</c:forEach>
								

							</div>
					

						
					
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		
		
		<!-- Model  -->
				<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		
		<script src="${js}/homee.js"></script>
	</body>
</html>