



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />

<!DOCTYPE html>
<html>
	<head>

		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/communities.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet">
	
		<script src="${js}/jquery.js"></script>
       	<script src="${js}/bootstrap.min.js"></script>
       	<script src="${js}/jquery.validate.min.js"></script>
       	<script src="${js}/additional-methods.js"></script>
       	<script src="${js}/communities.js"></script>
	
	
	<style>
		#errormessage{
			padding:15px 25px 15px 25px;
		}
	</style>
	</head>

	<body>
		<div class="top-content">
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
						<div class="col-md-5">
							<div class="textouter">
								<div class="text-top">
									<h1>KashmirConnect</h1>
									<h5>Answers to your Questions!</h5>
									<hr>
								</div>
								<div class="text-middle">
									<h4>You're almost done!</h4>
									<p>Share a little more about yourself, Select a community that you are interested in.
						You are about to create a new account on Kashmir Connect using a login from
						 email address <b>(${email})</b></p>
								</div>
								<div class="navbox">
									<br>
									<p>Copyright &copy;2018 KashmirConnect</p>
									<p>
									<a href="/KashmirConnect/contact">Contact</a>&nbsp;&nbsp;&nbsp;
									<a href="/KashmirConnect/about">About</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="outer">
							
							<form action="/KashmirConnect/generateuserprofile" id="community" method="POST">
								<div class="form-group">
                                        <label for="userName">What Should we call you</label>
                                            <input type="text" name="userName" id="userName" value="${name}" class="userName form-control">
                                    </div>
                                    
                                    <div class="form-group" >
                                        <label for="status">Current Status</label>
                                            <select id="status" name="status" class="status form-control" required>
											<option value="">Select</option>
											<option value="student">Student</option>
											<option value="employee">Employee</option>
											<option value="freelancer">Freelancer</option>
											<option value="business">Entrepreneur</option>
											
										</select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="profile">Current Job Profile(if any)</label>
                                            <input type="text" name="profile" id="profile" class="profile form-control">
                                    </div>
                                    
                                    <div class="form-group">
										<label for="role">Select Community</label> 
										<select id="role" name="role" class="role form-control" required>
											<option value="">Select</option>
											<option value="medicine">Medicine</option>
											<option value="tourism">Tourism</option>
											<option value="languages">Languages</option>
											<option value="business">Business</option>
											<option value="environment">Environment</option>
											<option value="arts">Arts</option>
										</select>
									</div>
									
									<div class="form-group">
                                        <label for="mySite">My Website(if any)</label>
                                            <input type="text" id="mySite" class="mySite form-control" name="mySite" placeholder="http//:www.example.com">
                                    </div>
									
									<div class="form-group">
                                        <label for="description">Description</label>
                                            <textarea id="description" class="description form-control" name="description"></textarea>
                                    </div>
									
									<div class=form-group>
										<input type="hidden" name="email" value="${email}">
									</div>
									
									<button type="submit" class="btn btn-primary btn-block">Create my account</button>
							</form>
							
						</div>
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
		</div>
</body>
</html>
