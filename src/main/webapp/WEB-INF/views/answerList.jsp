<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>



<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>AnswerList - Kashmir connect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link href="${css}/home.css" rel="stylesheet">
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		<link href="${css}/home.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Oswald:100,400,300,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
		
	</head>
	
	<body>
		<!-- Starting of Navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
				  	</button>
					<a class="navbar-brand" href="/KashmirConnect/home"><b>KashmirConnect</b></a>
				</div>
				
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li><a href="home.jsp">Home</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-pencil"></span>Answer</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-bell"></span>Notifications</a></li>
						<li>
							<form class="navbar-form" method="POST" role="search">
								<div class="input-group search">
									<input type="text" class="form-control" placeholder="Search" name="searches" required>
									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</div>
								</div>
							</form>
						</li>
						<li>
							<p class="navbar-btn">
								<a href="#" class="anchor_ask_question btn btn-danger">Add Question</a>
							</p>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
							<span><img src="${imag }/profilesetting.png" class="img-circle"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">Profile</a> </li>
								<li><a href="#">Blogs</a> </li>
								<li><a href="#">Messages</a> </li>
								<li><a href="#">Setting</a> </li>
								<li><a href="<c:url value="/logout" />">Logout</a> </li>
							</ul>
						</li>
					</ul>
					
				</div>
				
			</div>
		</nav>
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
				<div class="col-md-2 side_bar">
					<div class="story_list">
						<h4>Feeds</h4>
						<a href="home.html">Top Stories</a><br>
						<a href="newquestions.html">New Questions</a><br>
						<a href="bookmarked.html">Bookmarked Answers</a><br>
						<a href="links.html">Links</a><br>
					</div>
					<div class="community_list">
						 <h4>Communities</h4>
						 <a href="#">Environment</a><br>
						 <a href="#">Tourism</a><br>
						 <a href="#">Arts</a><br>
						 <a href="#">Medicine</a><br>
						 <a href="#">Languages</a><br>
						 <a href="#">Business</a><br>
					</div>
				</div>
				<!-- side bar close -->
				<div class="col-md-7 middle_section">
					<div class="answer_space">
						<h5>Question Posted by <a href="#">${userName}</a></h5>
						<div class="ques">
							<h2>${question} </h2>
						</div>
					</div>
						
						
						
						<c:forEach items="${TourismList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
						
						
						<c:forEach items="${MedicineList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
						
						
						<c:forEach items="${ArtsList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
						
						
						<c:forEach items="${BusinessList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
						
						<c:forEach items="${LanguagesList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
						
						<c:forEach items="${EnvironmentList}" var="record">
						<div class="comment_list">
							<div class="answer_list">
								<div class="user_comment">
									<h5>Commented By <a href="#"><b>${record.realUser.userName}</b></a> <span>at</span> ${record.timeAnswered}</h5>
								</div>
								<div class="answer_post">
									
									<p class="para_answer">${record.answer} </p>
									
								</div>
							</div>
						</div>
						</c:forEach>
					
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		
		
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
					});
			});
			
			function editAnswer(_el){
				$(_el).closest(".answer_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").show();
				$(_el).closest(".answer_post").find(".para_answer,.btnEdit,.btnDelete").hide();
			}
			
			function canceleditAnswer(_el){
				$(_el).closest(".answer_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").hide();
				$(_el).closest(".answer_post").find(".para_answer,.btnEdit,.btnDelete").show();
			}
		</script>
	</body>
</html>