

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>UserProfile - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		<link href="${css}/home.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
		
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
	</head>
	
	<body>
		
		<!-- Starting of Navbar -->
		
			<%@include file="./shared/navbar.jsp" %>
		
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<div class="col-md-2 side_bar">
						<div class="story_list">
						<h4>User Feeds</h4>
							<a href="#">Questions</a><br>
							<a href="#">Answers</a><br>
							<a href="#">Liked Questions</a><br>	
						</div>
					</div>
				<!-- side bar close -->
				
				<!-- body middle column-->
				
				<div class="col-md-7 middle_section">
						<form>
							 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 	<input type="hidden" name="role" id="user_role" value="${role}" class="form control">
						</form>
					
					
					</div>
				
				<!-- body middle column close -->
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<!-- Model  -->
				<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		
		<script src="${js}/homee.js"></script>			
	</body>
	
</html>

