<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Email Confirmation</title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="${web}/all.min.css">
        <link href="${css}/error.css" rel="stylesheet">
            
    </head>
    <body>
    
    	<div class=container>
    		<div class="row">
    			<div class="col-md-3"></div>
    			<div class="col-md-6">
    				
    				<div class="outer">
    					
    					<h1>403</h1>
    					<h3>Access forbidden! </h3>
    					<hr>
    					<p>Please try the following options instead</p>
    					
    					<a href="/KashmirConnect/login"><button id="sign" class="btn btn-danger ">Sign up</button></a>
    					<a href="/KashmirConnect/login"><button id="log" class="btn btn-danger ">Login</button></a>
    					
    					
   					</div>
    			</div>
    			<div class="col-md-3"></div>
    		</div>
    	</div>
         
    </body>
</html>
