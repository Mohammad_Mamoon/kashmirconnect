

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="web" value="/resources/fonts/css" />
<spring:url var="ric" value="/resources/richtext" />
<spring:url var="imag" value="resources/images" />

<!DOCTYPE html>

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>LikedQuestions - Kashmirconnect </title>
		<link href="${css}/bootstrap.min.css" rel="stylesheet">
		
		<link href="${ric}/rich.css" rel="stylesheet">
		<link href="${web}/all.min.css" rel="stylesheet" >
		<link href="${css}/home.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Domine" rel="stylesheet">
		
		
		<script src="${js}/jquery.js"></script>
		<script src="${js}/bootstrap.min.js"></script>
		<script src="${js}/richt.js"></script>
		
		
	</head>
	
	<body>
		
		<!-- Starting of Navbar -->
		
			<%@include file="./shared/navbar.jsp" %>
		
		<!-- navbar close-->
		
		<!-- body -->
		
		<div class="container">
			<div class="row">
				<!-- Side bar -->
					<%@include file="./shared/sidebar.jsp" %>
				<!-- side bar close -->
				
				<!-- body middle column-->
				
				<div class="col-md-7 middle_section">
						<form>
							 <input type="hidden" name="email" id="user_email" value="${email}" class="form control">
						 	<input type="hidden" name="role" id="user_role" value="${role}" class="form control">
						</form>
					
					
					<c:forEach items="${list}" var="record">
						
				  		<div class="question_list">
											  			
							
							 <h5>
							 	<a href="/KashmirConnect/userprofile?email=${record.realUser.email }"><b>${record.realUser.userName}</b></a>
							 	<span>added Question at &nbsp;</span>${record.timeAsked}
							 	
							 </h5>	
   							<h3><a href="#">${record.question}</a></h3>
					 		
					 			
								<form action="/KashmirConnect/answer" method="GET">
								
								<input type="hidden" name="questionId" value="${record.questionId}">
								<input type="hidden" id="community" name="realUser.role" value="${record.realUser.role}">
								
									<button type="submit" class="btn btn-link btn-sm"style="margin-bottom: 10px; font-size: 18px;color: #444;"><span class="glyphicon glyphicon-pencil">Answer</span></button>
								</form> 
								
							<div class="quesvotes">
									<a href="#" onclick = "vote(this,'like',${record.questionId})"><span class="glyphicon glyphicon-thumbs-up up">Like<span class="badge">${record.upvotesCount}</span></span></a>
									<a href="#" onclick="vote(this,'unlike',${record.questionId})"><span class="glyphicon glyphicon-thumbs-down down">Dislike<span class="badge">${record.downvotesCount}</span></span></a>
									<a href="#" onclick="vote(this,'bookmark',${record.questionId})"><span class="down">Bookmark Question<span class="badge">${record.bookmarkCount}</span></span></a>
							</div>
							<div class="form-group error_message">
									<c:if test="${not empty error}">
										<p>! ${error}</p>
									</c:if>
							</div>
						 </div>
					</c:forEach>		
				</div>
				
				<!-- body middle column close -->
				<div class="col-md-3"></div>
			</div>
		</div>
		
		<!-- Model  -->
				<%@include file="./shared/model.jsp" %>
		<!-- End of Model -->
		
		
		<script src="${js}/homee.js"></script>			
	</body>
	
</html>

