$(document).ready(function(){
	
	$(".glyphicon-eye-open").on("click", function() {
		$(this).toggleClass("glyphicon-eye-close");
		var type = $("#password").attr("type");
		if (type == "text"){ 
		$("#password").prop('type','password');}
		else{ 
		$("#password").prop('type','text'); }
		});
		
	$(".glyphicon-eye-open").on("click", function() {
		$(this).toggleClass("glyphicon-eye-close");
		var type = $("#conpassword").attr("type");
		if (type == "text"){ 
		$("#conpassword").prop('type','password');}
		else{ 
		$("#conpassword").prop('type','text'); }
		});	
	
        $.validator.addMethod('strongPassword', function(value, element){
    		return this.optional(element)
        	|| /\d/.test(value)
        	&& /[a-z]/i.test(value);
        	}, ' contain at least one number and one character');
        	
        	
       	
       $.validator.addMethod("laxEmail", function(value, element) {
		  // allow any non-whitespace characters as the host part
		  return this.optional( element ) || /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i.test( value ); 
		  }, 'Please enter a valid email address.');

        	
    	$("#registration-form").validate({
			rules: {
				email: {
					required: true,
					email: true,
					laxEmail: true
				},
				password:{
					required: true,
					strongPassword:true,
		
					minlength: "8"
				},
				conpassword: {
					required: true,
					equalTo: "#password"
				},
				firstName:{
					required: true,
					nowhitespace: true,
					lettersonly: true,
					minlength: "3"
				},
				lastName:{
					required: true,
					minlength: "3"
				}
			},
			messages:{
				email: {
					required: '**Please enter your email address',
					email: '**Please enter a valid email address',
					laxEmail: '**Enter valid email address'
				},
				password:{
					required: '**Please enter password',
					strongPassword: '**Password contain at least one character and one digit',
		
					minlength: '**Enter at least 8 characters'
				},
				conpassword:{
					required: '**Please enter password again',
					equalTo: '**Password is not matching'
				},
				firstName:{
					required: '**This field is required',
					nowhitespace: '**spaces are not allowed',
					lettersonly: '**Only letters are accepted',
					minlength: '**Enter at least 3 characters'
				},
				lastName:{
					required: '**This field is required',
					minlength: '**Enter at least 3 characters'
				}
			}
    	});
    });
            
     


        