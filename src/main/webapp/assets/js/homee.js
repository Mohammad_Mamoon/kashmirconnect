$(document).ready(function($) {
			
		$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
		});
	});
			
			
	$(".anchor_ask_question").click(function(){
				
		$("#myModal").modal();
	});
		
	
	
	

	$('#myModal').on('hidden.bs.modal', function () {
		$('.add_question').unRichText();
	});
			
	
	function editAnswer(_el){
		
		$(_el).closest(".question_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").show();
		$(_el).closest(".question_post").find(".equestion,.btnEdit").hide();
	}
	
	function canceleditAnswer(_el){
		$(_el).closest(".question_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").hide();
		$(_el).closest(".question_post").find(".equestion,.btnEdit").show();
	}
			
			
	function vote(_anchor,action,questionid){
		$(_anchor).find("span").css({
			'background' : 'rgb(250,144,0)',
		});
		
		var press = action;
		var questionId = questionid;
		
		if(press == "like"){
			
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/questionupvote",
				data  : {actionpressed:press,questionId:questionId},
			
				success : function(response){
					location.reload(true);
				}					
						
			});
		}
		
		else if(press == "unlike"){
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/questiondownvote",
				data  : {actionpressed:press,questionId:questionId},
				success : function(response){
					location.reload(true);
				}
			});
		}
		
		else if(press == "flag"){
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/questionflag",
				data  : {actionpressed:press,questionId:questionId},
				success : function(response){
					location.reload(true);
				}
				
							
			});
		}
		else if(press == "bookmark")
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/bookmarkquestion",
				data  : {questionId:questionId,community:$("#community").val()},
				success : function(response){
					location.reload(true);
				}
				
							
			});
		} 
		
		function saveQuestion(){
				
			var question = $('.add_question')[0].value;
			var e = $("#user_email").val();
			var r = $("#user_role").val();
				
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/submitquestion",
				data  : {question:question, email:$("#user_email").val()},
				success : function(response){
					
					location.reload();
				}
					
								
			});
		}
		
		
		function removequestion(){
			
			var question = $('.add_question')[0].value;
			var e = $("#user_email").val();
			var r = $("#user_role").val();
				
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/submitquestion",
				data  : {question:question, email:$("#user_email").val(), role:$("#user_role").val()},
				success : function(response){
					
					location.reload();
				}
					
								
			});
		}
		
		
		$('#w-input-search').autocomplete({
			serviceUrl:"/KashmirConnect/autocomplete",
			paramName: "tagName",
			delimiter: ",",
		   transformResult: function(response) {
			   var r = $("#user_role").val();
			return {      	
				
			  //must convert json to javascript object before process
			  suggestions: $.map($.parseJSON(response), function(object) {
			            	
			      return { value: "<a href='answer?questionId="+object.questionId+"&realUser.role="+r+"' >"+object.question+"</a>"  , data: object.questionId };
			   })
			            
			 };
			    
	            }
			   
		 });
					
		function editquestions(){
			
			var question = $('.newcomment')[0].value;
			
				
			$.ajax({
				type  :  "POST",
				url   :  "/KashmirConnect/editquestion",
				data  : {editQuestion:question, questionId:$("#lastmn").val()},
				success : function(response){
					location.reload();
				}
					
								
			});
		}
		
			
/*$(".anchor_user_profile").hover(function(){
				
});*/
