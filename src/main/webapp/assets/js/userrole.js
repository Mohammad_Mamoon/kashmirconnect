jQuery(document).ready(function($) {
			
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
			
			
			$(".anchor_ask_question").click(function(){
				$("#myModal").find("textarea").focus();
				$("#myModal").modal();
			});
			

			$('#myModal').on('hidden.bs.modal', function () {
				$('.add_question').unRichText();
			});
			
			function saveQuestion(){
				var question = $('.add_question')[0].value;
				
				$.ajax({
					type  :  "POST",
					url   :  "/KashmirConnect/submitquestion",
					data  : {question:question, email:$("#user_email").val(), role:$("#user_role").val()},
					success : function(response){
						alert("Question Posted Successfully");
						location.reload(true);
					}
					
								
				});
			}