jQuery(document).ready(function($) {
				
					$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
					});
			});
			
			function editAnswer(_el){
				
				$(_el).closest(".answer_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").show();
				$(_el).closest(".answer_post").find(".para_answer,.btnEdit,.btnDelete").hide();
			}
			
			function canceleditAnswer(_el){
				$(_el).closest(".answer_post").find(".div_txtarea_answer,.btnUpdate,.btnCancel").hide();
				$(_el).closest(".answer_post").find(".para_answer,.btnEdit,.btnDelete").show();
			}
			
		
			
			function vote(_anchor,action,answerid){
				$(_anchor).find("span").css({
					'background' : 'rgb(250,144,0)',
				});
				
				var press = action;
				var answerId = answerid;
				if(press == "like"){
				$.ajax({
					type  :  "POST",
					url   :  "/KashmirConnect/commentupvote",
					data  : {actionpressed:press,answerId:answerId},
					success : function(response){
						location.reload(true);
					}
					
								
				});
				}
				else if(press == "dislike"){
					$.ajax({
						type  :  "POST",
						url   :  "/KashmirConnect/commentdownvote",
						data  : {actionpressed:press,answerId:answerId},
						success : function(response){
							location.reload(true);
						}
						
									
					});
				}
				
				else if(press == "flag"){
					$.ajax({
						type  :  "POST",
						url   :  "/KashmirConnect/commentflag",
						data  : {actionpressed:press,answerId:answerId},
						success : function(response){
							location.reload(true);
						}
						
									
					});
				}
				
				
				
				}
			
			function saveAnswer(){
				   
				var answer = $('.add_answer')[0].value;
				$.ajax({
					type  :  "POST",
					url   :  "/KashmirConnect/submitanswer",
					data  : {answer:answer,questionId :$("#question_id").val()},
					success : function(response){
						location.reload();
					}
					
								
				});
			}
			
		