package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.BusinessQuestions;



public interface BusinessQuestionsDao {

	void save(BusinessQuestions businessQuestions);

	List<BusinessQuestions> pullAllQuestions();

	BusinessQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<BusinessQuestions> getQuestionList(String principalEmail);
	
	List<BusinessQuestions> getQuestions(String principalEmail);

	void update(BusinessQuestions businessQuestions);

	void delete(BusinessQuestions businessQuestions);
}
