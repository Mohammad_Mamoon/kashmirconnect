package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.BusinessQuestionVotes;


@Repository("businessQuestionVotesDao")
public class BusinessQuestionVotesDaoImpl extends AbstractDao<Integer, BusinessQuestionVotes> implements BusinessQuestionVotesDao {


	@Override
	public void save(BusinessQuestionVotes businessQuestionVotes) {
		persist(businessQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("businessQuestions.questionId", questionId));
		return (List<BusinessQuestionVotes>) crit.list();
	}

	@Override
	public BusinessQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("businessQuestions.questionId",questionId));
		return (BusinessQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(BusinessQuestionVotes businessQuestionVotes) {
		getSession().update(businessQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Business_Question_Votes_Table where businessQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}
}
