package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.ArtsQuestions;




@Repository("artsQuestionsDao")
public class ArtsQuestionsDaoImpl extends AbstractDao<Integer, ArtsQuestions> implements ArtsQuestionsDao {


	@Override
	public void save(ArtsQuestions artsQuestions) {
		persist(artsQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<ArtsQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<ArtsQuestions>) crit.list();
	}


	@Override
	public ArtsQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (ArtsQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<ArtsQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<ArtsQuestions>) crit.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ArtsQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<ArtsQuestions>) crit.list();
	}


	@Override
	public void update(ArtsQuestions artsQuestions) {
		getSession().update(artsQuestions);
		
	}

	@Override
	public void delete(ArtsQuestions artsQuestions) {
		getSession().delete(artsQuestions);
		
	}
	
	
}
