package com.KashmirConnect.dao;

import java.util.List;
import com.KashmirConnect.model.MedicineQuestions;



public interface MedicineQuestionsDao {


	void save(MedicineQuestions medicineQuestions);

	List<MedicineQuestions> pullAllQuestions();

	MedicineQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<MedicineQuestions> getQuestionList(String principalEmail);
	
	List<MedicineQuestions> getQuestions(String principalEmail);

	void update(MedicineQuestions medicineQuestions);

	void delete(MedicineQuestions medicineQuestions);
}
