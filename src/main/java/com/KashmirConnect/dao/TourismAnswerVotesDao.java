package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.TourismAnswerVotes;

public interface TourismAnswerVotesDao {

	void save(TourismAnswerVotes tourismAnswerVotes);

	List<TourismAnswerVotes> findUpvoteCount(long answerId);

	TourismAnswerVotes checkEntry(String email, long answerId);

	void update(TourismAnswerVotes tourismAnswerVotes);

	void delete(long questionId);
}
