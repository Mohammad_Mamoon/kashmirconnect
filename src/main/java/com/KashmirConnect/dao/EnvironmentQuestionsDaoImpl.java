package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.EnvironmentQuestions;



@Repository("environmentQuestionsDao")
public class EnvironmentQuestionsDaoImpl extends AbstractDao<Integer, EnvironmentQuestions> implements EnvironmentQuestionsDao  {


	@Override
	public void save(EnvironmentQuestions environmentQuestions) {
		persist(environmentQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<EnvironmentQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<EnvironmentQuestions>) crit.list();
	}


	@Override
	public EnvironmentQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (EnvironmentQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<EnvironmentQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<EnvironmentQuestions>) crit.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<EnvironmentQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<EnvironmentQuestions>) crit.list();
	}


	@Override
	public void update(EnvironmentQuestions environmentQuestions) {
		getSession().update(environmentQuestions);
		
	}

	@Override
	public void delete(EnvironmentQuestions environmentQuestions) {
		getSession().delete(environmentQuestions);
		
	}
}
