package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.EnvironmentAnswerVotes;


@Repository("environmentAnswerVotesDao")
public class EnvironmentAnswerVotesDaoImpl extends AbstractDao<Integer, EnvironmentAnswerVotes> implements EnvironmentAnswerVotesDao  {

	@Override
	public void save(EnvironmentAnswerVotes environmentAnswerVotes) {
		persist(environmentAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EnvironmentAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("environmentAnswers.answerId", answerId));
		return (List<EnvironmentAnswerVotes>) crit.list();
	}

	@Override
	public EnvironmentAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("environmentAnswers.answerId",answerId));
		return (EnvironmentAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(EnvironmentAnswerVotes environmentAnswerVotes) {
		getSession().update(environmentAnswerVotes);
		
	}

}
