package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.BusinessQuestions;



@Repository("businessQuestionsDao")
public class BusinessQuestionsDaoImpl extends AbstractDao<Integer, BusinessQuestions> implements BusinessQuestionsDao{

	@Override
	public void save(BusinessQuestions businessQuestions) {
		persist(businessQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<BusinessQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<BusinessQuestions>) crit.list();
	}


	@Override
	public BusinessQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (BusinessQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<BusinessQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<BusinessQuestions>) crit.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<BusinessQuestions>) crit.list();
	}


	@Override
	public void update(BusinessQuestions businessQuestions) {
		getSession().update(businessQuestions);
		
	}
	@Override
	public void delete(BusinessQuestions businessQuestions) {
		getSession().delete(businessQuestions);
		
	}

}
