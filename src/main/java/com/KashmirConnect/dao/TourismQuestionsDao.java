package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.TourismQuestions;

public interface TourismQuestionsDao {
	
	void save(TourismQuestions tourismQuestions);

	List<TourismQuestions> pullAllQuestions();

	TourismQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<TourismQuestions> getQuestionList(String principalEmail);
	
	List<TourismQuestions> getQuestions(String principalEmail);

	void update(TourismQuestions tourismQuestions);

	void delete(TourismQuestions tourismQuestions);



	
	
	
}
