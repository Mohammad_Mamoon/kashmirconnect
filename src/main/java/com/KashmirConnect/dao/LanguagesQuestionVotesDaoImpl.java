package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.LanguagesQuestionVotes;


@Repository("languagesQuestionVotesDao")
public class LanguagesQuestionVotesDaoImpl  extends AbstractDao<Integer, LanguagesQuestionVotes> implements LanguagesQuestionVotesDao {

	@Override
	public void save(LanguagesQuestionVotes languagesQuestionVotes) {
		persist(languagesQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LanguagesQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("languagesQuestions.questionId", questionId));
		return (List<LanguagesQuestionVotes>) crit.list();
	}

	@Override
	public LanguagesQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("languagesQuestions.questionId",questionId));
		return (LanguagesQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(LanguagesQuestionVotes languagesQuestionVotes) {
		getSession().update(languagesQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Languages_Question_Votes_Table where languagesQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}

}
