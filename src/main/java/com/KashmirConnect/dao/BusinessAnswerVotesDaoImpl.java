package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.BusinessAnswerVotes;


@Repository("businessAnswerVotesDao")
public class BusinessAnswerVotesDaoImpl extends AbstractDao<Integer, BusinessAnswerVotes> implements BusinessAnswerVotesDao {


	@Override
	public void save(BusinessAnswerVotes businessAnswerVotes) {
		persist(businessAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("businessAnswers.answerId", answerId));
		return (List<BusinessAnswerVotes>) crit.list();
	}

	@Override
	public BusinessAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("businessAnswers.answerId",answerId));
		return (BusinessAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(BusinessAnswerVotes businessAnswerVotes) {
		getSession().update(businessAnswerVotes);
		
	}
}
