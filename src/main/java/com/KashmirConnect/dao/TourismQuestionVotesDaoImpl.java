package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.TourismQuestionVotes;

@Repository("tourismQuestionVotesDao")
public class TourismQuestionVotesDaoImpl extends AbstractDao<Integer, TourismQuestionVotes> implements TourismQuestionVotesDao {


	@Override
	public void save(TourismQuestionVotes tourismQuestionVotes) {
		persist(tourismQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TourismQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismQuestions.questionId", questionId));
		return (List<TourismQuestionVotes>) crit.list();
	}

	@Override
	public TourismQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("tourismQuestions.questionId",questionId));
		return (TourismQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(TourismQuestionVotes tourismQuestionVotes) {
		getSession().update(tourismQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Tourism_Question_Votes_Table where tourismQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}

}
