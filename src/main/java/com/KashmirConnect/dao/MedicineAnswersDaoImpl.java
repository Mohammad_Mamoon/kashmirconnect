package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.KashmirConnect.model.MedicineAnswers;


@Repository("medicineAnswerssDao")
public class MedicineAnswersDaoImpl extends AbstractDao<Integer, MedicineAnswers> implements MedicineAnswersDao {

	@Override
	public void save(MedicineAnswers medicineAnswers) {
		persist(medicineAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<MedicineAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("medicineQuestions.questionId",questionId)));
		return (List<MedicineAnswers>) crit.list();
	}


	@Override
	public void update(MedicineAnswers medicineAnswers) {
		getSession().update(medicineAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Medicine_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("tourismQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public MedicineAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (MedicineAnswers)crit.uniqueResult();
	}


	@Override
	public MedicineAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (MedicineAnswers) crit.uniqueResult();
	}



}
