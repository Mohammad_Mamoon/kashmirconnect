package com.KashmirConnect.dao;

import java.util.List;


import com.KashmirConnect.model.TourismAnswers;

public interface TourismAnswersDao {
	
	void save(TourismAnswers tourismAnswers);
	List<TourismAnswers> pullAllAnswers(long questionId);
	TourismAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(TourismAnswers tourismAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	TourismAnswers findAnswer(long answerId);
	void remove(long questionId);
	
	
}
