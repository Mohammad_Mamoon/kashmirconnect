package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.BusinessAnswerVotes;


public interface BusinessAnswerVotesDao {

	void save(BusinessAnswerVotes businessAnswerVotes);

	List<BusinessAnswerVotes> findUpvoteCount(long answerId);

	BusinessAnswerVotes checkEntry(String email, long answerId);

	void update(BusinessAnswerVotes businessAnswerVotes);
}
