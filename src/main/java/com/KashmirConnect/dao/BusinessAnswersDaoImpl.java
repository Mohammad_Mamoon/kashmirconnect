package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.BusinessAnswers;



@Repository("businessAnswerssDao")
public class BusinessAnswersDaoImpl extends AbstractDao<Integer, BusinessAnswers>implements BusinessAnswersDao {

	@Override
	public void save(BusinessAnswers businessAnswers) {
		persist(businessAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<BusinessAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("businessQuestions.questionId",questionId)));
		return (List<BusinessAnswers>) crit.list();
	}


	@Override
	public void update(BusinessAnswers businessAnswers) {
		getSession().update(businessAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Business_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("businessQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public BusinessAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (BusinessAnswers)crit.uniqueResult();
	}


	@Override
	public BusinessAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (BusinessAnswers) crit.uniqueResult();
	}


}
