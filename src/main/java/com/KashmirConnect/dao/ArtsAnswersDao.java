package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.ArtsAnswers;



public interface ArtsAnswersDao {

	void save(ArtsAnswers artsAnswers);
	List<ArtsAnswers> pullAllAnswers(long questionId);
	ArtsAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(ArtsAnswers artsAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	ArtsAnswers findAnswer(long answerId);
	
}
