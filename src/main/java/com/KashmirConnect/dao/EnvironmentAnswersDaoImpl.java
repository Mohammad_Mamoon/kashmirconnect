package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.EnvironmentAnswers;



@Repository("environmentAnswerssDao")
public class EnvironmentAnswersDaoImpl extends AbstractDao<Integer, EnvironmentAnswers>implements EnvironmentAnswersDao  {

	
	@Override
	public void save(EnvironmentAnswers environmentAnswers) {
		persist(environmentAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<EnvironmentAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("environmentQuestions.questionId",questionId)));
		return (List<EnvironmentAnswers>) crit.list();
	}


	@Override
	public void update(EnvironmentAnswers environmentAnswers) {
		getSession().update(environmentAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Environment_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("environmentQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public EnvironmentAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (EnvironmentAnswers)crit.uniqueResult();
	}


	@Override
	public EnvironmentAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (EnvironmentAnswers) crit.uniqueResult();
	}


}
