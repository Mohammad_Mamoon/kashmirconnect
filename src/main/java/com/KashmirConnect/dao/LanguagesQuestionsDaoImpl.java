package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.LanguagesQuestions;



@Repository("languagesQuestionsDao")
public class LanguagesQuestionsDaoImpl extends AbstractDao<Integer, LanguagesQuestions> implements LanguagesQuestionsDao {

	@Override
	public void save(LanguagesQuestions languagesQuestions) {
		persist(languagesQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<LanguagesQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<LanguagesQuestions>) crit.list();
	}


	@Override
	public LanguagesQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (LanguagesQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<LanguagesQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<LanguagesQuestions>) crit.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<LanguagesQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<LanguagesQuestions>) crit.list();
	}


	@Override
	public void update(LanguagesQuestions languagesQuestions) {
		getSession().update(languagesQuestions);
		
	}
	@Override
	public void delete(LanguagesQuestions languagesQuestions) {
		getSession().delete(languagesQuestions);
		
	}
}
