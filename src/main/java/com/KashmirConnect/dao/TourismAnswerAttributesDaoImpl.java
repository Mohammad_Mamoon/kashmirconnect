package com.KashmirConnect.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.TourismAnswerAttributes;


@Repository("tourismAnswerAttributesDao")
public class TourismAnswerAttributesDaoImpl extends AbstractDao<Integer, TourismAnswerAttributes>implements TourismAnswerAttributesDao {

	@Override
	public void increaseUpvoteCountValue(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismAnswers.answerId", answerId));
		 TourismAnswerAttributes tourismAnswerAttributes =  (TourismAnswerAttributes) crit.uniqueResult();
		 int newValue = tourismAnswerAttributes.getUpvotesCount() + 1;
		 
		 String hql = "Update Tourism_Answer_Attributes_Table set UpvoteCount = :newValue "  + 
	             "WHERE tourismAnswers.answerId = :answerId";
	     Query query = getSession().createQuery(hql);
	     query.setParameter("newValue", newValue);
	     query.setParameter("answerId", answerId);
	     query.executeUpdate();
		
	}

	@Override
	public void increaseDownvoteCountValue(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismAnswers.answerId", answerId));
		 TourismAnswerAttributes tourismAnswerAttributes =  (TourismAnswerAttributes) crit.uniqueResult();
		 int newValue = tourismAnswerAttributes.getUpvotesCount() + 1;
		 
		 String hql = "Update Tourism_Answer_Attributes_Table set DownvoteCount = :newValue "  + 
	             "WHERE tourismAnswers.answerId = :answerId";
	     Query query = getSession().createQuery(hql);
	     query.setParameter("newValue", newValue);
	     query.setParameter("answerId", answerId);
	     query.executeUpdate();
	}


	@Override
	public void update(TourismAnswerAttributes tourismAnswerAttributes) {
		getSession().update(tourismAnswerAttributes);
		
	}

	@Override
	public void save(TourismAnswerAttributes tourismAnswerAttributes) {
		persist(tourismAnswerAttributes);
		
	}

	@Override
	public TourismAnswerAttributes findDetails(long answerId) {
		Criteria crit = createEntityCriteria();
	    crit.add((Restrictions.eq("tourismAnswers.answerId",answerId)));
		return (TourismAnswerAttributes) crit.uniqueResult();
	}

	@Override
	public TourismAnswerAttributes findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
	    crit.add((Restrictions.eq("tourismAnswers.answerId",answerId)));
		return (TourismAnswerAttributes) crit.uniqueResult();
	}
	

	
}
