package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.ArtsQuestions;



public interface ArtsQuestionsDao {

	void save(ArtsQuestions artsQuestions);

	List<ArtsQuestions> pullAllQuestions();

	ArtsQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<ArtsQuestions> getQuestionList(String principalEmail);
	
	List<ArtsQuestions> getQuestions(String principalEmail);

	void update(ArtsQuestions artsQuestions);

	void delete(ArtsQuestions artsQuestions);
	
}
