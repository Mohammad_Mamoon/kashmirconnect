package com.KashmirConnect.dao;

import com.KashmirConnect.model.TourismAnswerAttributes;


public interface TourismAnswerAttributesDao {

	void increaseUpvoteCountValue(long answerId);
	void increaseDownvoteCountValue(long answerId);
	void update(TourismAnswerAttributes tourismAnswerAttributes);
	void save(TourismAnswerAttributes tourismAnswerAttributes);
	TourismAnswerAttributes findDetails(long answerId);
	TourismAnswerAttributes findAnswer(long answerId);
}
