package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.BookmarkQuestions;




@Repository("bookmarkQuestionsDao")
public class BookmarkQuestionsDaoImpl extends AbstractDao<Integer, BookmarkQuestions> implements BookmarkQuestionsDao {


	@Override
	public void save(BookmarkQuestions bookmarkQuestions) {
		persist(bookmarkQuestions);

	}

	@Override
	public void delete(long questionId, String community) {
		String hql = "delete from Bookmark_Question_Table where questionId = :questionId and community = :community ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.setParameter("community", community)
					.executeUpdate();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BookmarkQuestions> findByEmail(String email) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", email));
		return (List<BookmarkQuestions>) crit.list();
	}

}
