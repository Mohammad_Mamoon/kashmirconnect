package com.KashmirConnect.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.KashmirConnect.model.TemporaryUser;


@Repository("temporaryUserDao")
public class TemporaryUserDaoImpl extends AbstractDao<Integer, TemporaryUser> implements TemporaryUserDao {
	

	@Override
	public void save(TemporaryUser temporaryUser) {
		persist(temporaryUser);
		
	}

	@Override
	public TemporaryUser findByUnqiueCode(String confirmCode) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("uniqueCode", confirmCode));
		return (TemporaryUser) crit.uniqueResult();
	}

	@Override
	public TemporaryUser findByEmail(String checkEmail) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("email", checkEmail)));
		return (TemporaryUser) crit.uniqueResult();
	}
	

	@Override
	public void delete(String email) {
		String hql = "delete from Temporary_User where email = :email";
		getSession().createQuery(hql).setString("email", email)
		.executeUpdate();
	}
	
}