package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.LanguagesQuestions;



public interface LanguagesQuestionsDao {

	void save(LanguagesQuestions languagesQuestions);

	List<LanguagesQuestions> pullAllQuestions();

	LanguagesQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<LanguagesQuestions> getQuestionList(String principalEmail);
	
	List<LanguagesQuestions> getQuestions(String principalEmail);

	void update(LanguagesQuestions languagesQuestions);

	void delete(LanguagesQuestions languagesQuestions);
}
