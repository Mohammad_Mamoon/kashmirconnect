package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.EnvironmentQuestionVotes;


@Repository("environmentQuestionVotesDao")
public class EnvironmentQuestionVotesDaoImpl  extends AbstractDao<Integer, EnvironmentQuestionVotes> implements EnvironmentQuestionVotesDao {

	@Override
	public void save(EnvironmentQuestionVotes environmentQuestionVotes) {
		persist(environmentQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EnvironmentQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("environmentQuestions.questionId", questionId));
		return (List<EnvironmentQuestionVotes>) crit.list();
	}

	@Override
	public EnvironmentQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("environmentQuestions.questionId",questionId));
		return (EnvironmentQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(EnvironmentQuestionVotes environmentQuestionVotes) {
		getSession().update(environmentQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Environment_Question_Votes_Table where environmentQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}

}
