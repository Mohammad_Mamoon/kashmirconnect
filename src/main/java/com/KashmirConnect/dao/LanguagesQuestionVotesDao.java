package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.LanguagesQuestionVotes;


public interface LanguagesQuestionVotesDao {

	void save(LanguagesQuestionVotes languagesQuestionVotes);

	List<LanguagesQuestionVotes> findUpvoteCount(long questionId);

	LanguagesQuestionVotes checkEntry(String email, long questionId);

	void update(LanguagesQuestionVotes languagesQuestionVotes);

	void delete(long questionId);

}
