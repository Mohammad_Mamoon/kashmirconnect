package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.ArtsQuestionVotes;


public interface ArtsQuestionVotesDao {

	void save(ArtsQuestionVotes artsQuestionVotes);

	List<ArtsQuestionVotes> findUpvoteCount(long questionId);

	ArtsQuestionVotes checkEntry(String email, long questionId);

	void update(ArtsQuestionVotes artsQuestionVotes);

	void delete(long questionId);
}
