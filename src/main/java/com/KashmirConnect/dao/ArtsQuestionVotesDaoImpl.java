package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.ArtsQuestionVotes;

@Repository("artsQuestionVotesDao")
public class ArtsQuestionVotesDaoImpl  extends AbstractDao<Integer, ArtsQuestionVotes> implements ArtsQuestionVotesDao {


	@Override
	public void save(ArtsQuestionVotes artsQuestionVotes) {
		persist(artsQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ArtsQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("artsQuestions.questionId", questionId));
		return (List<ArtsQuestionVotes>) crit.list();
	}

	@Override
	public ArtsQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("artsQuestions.questionId",questionId));
		return (ArtsQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(ArtsQuestionVotes artsQuestionVotes) {
		getSession().update(artsQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Arts_Question_Votes_Table where artsQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}
}
