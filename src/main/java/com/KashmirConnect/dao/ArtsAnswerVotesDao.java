package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.ArtsAnswerVotes;


public interface ArtsAnswerVotesDao {

	void save(ArtsAnswerVotes artsAnswerVotes);

	List<ArtsAnswerVotes> findUpvoteCount(long answerId);

	ArtsAnswerVotes checkEntry(String email, long answerId);

	void update(ArtsAnswerVotes artsAnswerVotes);
}
