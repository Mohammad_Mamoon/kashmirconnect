package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.KashmirConnect.model.MedicineQuestions;



@Repository("medicineQuestionsDao")
public class MedicineQuestionsDaoImpl extends AbstractDao<Integer, MedicineQuestions> implements MedicineQuestionsDao {


	@Override
	public void save(MedicineQuestions medicineQuestions) {
		persist(medicineQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<MedicineQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<MedicineQuestions>) crit.list();
	}


	@Override
	public MedicineQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (MedicineQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<MedicineQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<MedicineQuestions>) crit.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MedicineQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<MedicineQuestions>) crit.list();
	}


	@Override
	public void update(MedicineQuestions medicineQuestions) {
		getSession().update(medicineQuestions);
		
	}

	@Override
	public void delete(MedicineQuestions medicineQuestions) {
		getSession().delete(medicineQuestions);
		
	}
	

	


	

}


