package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.MedicineAnswerVotes;


public interface MedicineAnswerVotesDao {

	void save(MedicineAnswerVotes medicineAnswerVotes);

	List<MedicineAnswerVotes> findUpvoteCount(long answerId);

	MedicineAnswerVotes checkEntry(String email, long answerId);

	void update(MedicineAnswerVotes medicineAnswerVotes);
}
