package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.EnvironmentAnswerVotes;


public interface EnvironmentAnswerVotesDao {

	void save(EnvironmentAnswerVotes environmentAnswerVotes);

	List<EnvironmentAnswerVotes> findUpvoteCount(long answerId);

	EnvironmentAnswerVotes checkEntry(String email, long answerId);

	void update(EnvironmentAnswerVotes environmentAnswerVotes);
}
