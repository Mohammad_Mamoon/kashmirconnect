package com.KashmirConnect.dao;

import java.util.List;
import com.KashmirConnect.model.MedicineAnswers;



public interface MedicineAnswersDao {
	void save(MedicineAnswers medicineAnswers);
	List<MedicineAnswers> pullAllAnswers(long questionId);
	MedicineAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(MedicineAnswers medicineAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	MedicineAnswers findAnswer(long answerId);

}
