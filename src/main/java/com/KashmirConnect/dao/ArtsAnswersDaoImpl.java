package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.ArtsAnswers;



@Repository("artsAnswersDao")
public class ArtsAnswersDaoImpl extends AbstractDao<Integer, ArtsAnswers>implements ArtsAnswersDao {
	

	@Override
	public void save(ArtsAnswers artsAnswers) {
		persist(artsAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<ArtsAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("artsQuestions.questionId",questionId)));
		return (List<ArtsAnswers>) crit.list();
	}


	@Override
	public void update(ArtsAnswers artsAnswers) {
		getSession().update(artsAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Arts_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("artsQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public ArtsAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (ArtsAnswers)crit.uniqueResult();
	}


	@Override
	public ArtsAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (ArtsAnswers) crit.uniqueResult();
	}


}
