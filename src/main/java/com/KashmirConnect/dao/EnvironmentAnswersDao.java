package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.EnvironmentAnswers;



public interface EnvironmentAnswersDao {

	void save(EnvironmentAnswers environmentAnswers);
	List<EnvironmentAnswers> pullAllAnswers(long questionId);
	EnvironmentAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(EnvironmentAnswers environmentAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	EnvironmentAnswers findAnswer(long answerId);
	
}
