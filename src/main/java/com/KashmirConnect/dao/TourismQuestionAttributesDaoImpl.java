package com.KashmirConnect.dao;



import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.KashmirConnect.model.TourismQuestionAttributes;

@Repository("tourismQuestionAttributesDao")
public class TourismQuestionAttributesDaoImpl extends AbstractDao<Integer, TourismQuestionAttributes>implements TourismQuestionAttributesDao {
	

	@Override
	public void increaseUpvoteCountValue(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismQuestions.questionId", questionId));
		 TourismQuestionAttributes tourismQuestionAttributes =  (TourismQuestionAttributes) crit.uniqueResult();
		 int newValue = tourismQuestionAttributes.getUpvotesCount() + 1;
		 
		 String hql = "Update Tourism_Question_Attributes_Table set UpvoteCount = :newValue "  + 
	             "WHERE tourismQuestions.questionId = :questionId";
	     Query query = getSession().createQuery(hql);
	     query.setParameter("newValue", newValue);
	     query.setParameter("questionId", questionId);
	     query.executeUpdate();
		
	}

	@Override
	public void increaseDownvoteCountValue(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismQuestions.questionId", questionId));
		 TourismQuestionAttributes tourismQuestionAttributes =  (TourismQuestionAttributes) crit.uniqueResult();
		 int newValue = tourismQuestionAttributes.getUpvotesCount() + 1;
		 
		 String hql = "Update Tourism_Question_Attributes_Table set DownvoteCount = :newValue "  + 
	             "WHERE tourismQuestions.questionId = :questionId";
	     Query query = getSession().createQuery(hql);
	     query.setParameter("newValue", newValue);
	     query.setParameter("questionId", questionId);
	     query.executeUpdate();
		
	}

	@Override
	public TourismQuestionAttributes findQuestion(long questionId) {
		 Criteria crit = createEntityCriteria();
		    crit.add((Restrictions.eq("tourismQuestions.questionId",questionId)));
			return (TourismQuestionAttributes) crit.uniqueResult();
	}

	@Override
	public void update(TourismQuestionAttributes tourismQuestionAttributes) {
		getSession().update(tourismQuestionAttributes);
		
	}

	@Override
	public void save(TourismQuestionAttributes tourismQuestionAttributes) {
		persist(tourismQuestionAttributes);
		
	}

	@Override
	public TourismQuestionAttributes findDetails(long questionId) {
		 Criteria crit = createEntityCriteria();
		    crit.add((Restrictions.eq("tourismQuestions.questionId",questionId)));
			return (TourismQuestionAttributes) crit.uniqueResult();
	}

}
