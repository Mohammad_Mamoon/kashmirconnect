package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.LanguagesAnswers;



@Repository("languagesAnswerssDao")
public class LanguagesAnswersDaoImpl extends AbstractDao<Integer, LanguagesAnswers>implements LanguagesAnswersDao {


	@Override
	public void save(LanguagesAnswers languagesAnswers) {
		persist(languagesAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<LanguagesAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("languagesQuestions.questionId",questionId)));
		return (List<LanguagesAnswers>) crit.list();
	}


	@Override
	public void update(LanguagesAnswers languagesAnswers) {
		getSession().update(languagesAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Languages_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("languagesQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public LanguagesAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (LanguagesAnswers)crit.uniqueResult();
	}


	@Override
	public LanguagesAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (LanguagesAnswers) crit.uniqueResult();
	}


	
}
