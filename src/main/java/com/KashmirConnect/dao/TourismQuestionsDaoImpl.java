package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;


import com.KashmirConnect.model.TourismQuestions;

@Repository("tourismQuestionsDao")
public class TourismQuestionsDaoImpl extends AbstractDao<Integer, TourismQuestions> implements TourismQuestionsDao {
		

	@Override
	public void save(TourismQuestions tourismQuestions) {
		persist(tourismQuestions);

	}

	
	@SuppressWarnings("unchecked")
	public List<TourismQuestions> pullAllQuestions() {
		Criteria crit = createEntityCriteria();
		return (List<TourismQuestions>) crit.list();
	}


	@Override
	public TourismQuestions findQuestion(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("questionId", questionId));
		return (TourismQuestions) crit.uniqueResult();
	}


	@Override
	public long[] getQuestionIds(String principalEmail) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@SuppressWarnings("unchecked")
	public List<TourismQuestions> getQuestionList(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.ne("realUser.email", principalEmail));
		return (List<TourismQuestions>) crit.list();
	}
	

	@SuppressWarnings("unchecked")
	public List<TourismQuestions> getQuestions(String principalEmail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email", principalEmail));
		return (List<TourismQuestions>) crit.list();
	}


	@Override
	public void update(TourismQuestions tourismQuestions) {
		getSession().update(tourismQuestions);
		
	}

	@Override
	public void delete(TourismQuestions tourismQuestions) {
		getSession().delete(tourismQuestions);
		
	}
	

}

