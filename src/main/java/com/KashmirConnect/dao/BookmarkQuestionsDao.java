package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.BookmarkQuestions;

public interface BookmarkQuestionsDao {
	void save(BookmarkQuestions bookmarkQuestions);

	void delete(long questionId, String community);

	List<BookmarkQuestions> findByEmail(String email);

}
