package com.KashmirConnect.dao;

import com.KashmirConnect.model.TemporaryUser;

public interface TemporaryUserDao {

	TemporaryUser findByUnqiueCode(String confirmCode);
	void save(TemporaryUser temporaryUser);
	TemporaryUser findByEmail(String checkEmail);
	void delete(String email);
		
}