package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.RealUser;

public interface RealUserDao {
	
	RealUser findByEmail(String email);
	void save(RealUser realUser);
	void update(RealUser realUser);
	List<RealUser> sendEmailToGroup(String role);

}
