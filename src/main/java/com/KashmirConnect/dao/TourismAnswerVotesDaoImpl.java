package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.TourismAnswerVotes;


@Repository("tourismAnswerVotesDao")
public class TourismAnswerVotesDaoImpl extends AbstractDao<Integer, TourismAnswerVotes> implements TourismAnswerVotesDao {


	@Override
	public void save(TourismAnswerVotes tourismAnswerVotes) {
		persist(tourismAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TourismAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("tourismAnswers.answerId", answerId));
		return (List<TourismAnswerVotes>) crit.list();
	}

	@Override
	public TourismAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("tourismAnswers.answerId",answerId));
		return (TourismAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(TourismAnswerVotes tourismAnswerVotes) {
		getSession().update(tourismAnswerVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Tourism_Answer_Votes_Table where tourismQuestions.questionId = :questionId";
		getSession().createQuery(hql)
		.setParameter("questionId", questionId)
		.executeUpdate();
	}

}