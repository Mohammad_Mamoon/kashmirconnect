package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;


import com.KashmirConnect.model.TourismAnswers;


@Repository("tourismAnswerssDao")
public class TourismAnswersDaoImpl extends AbstractDao<Integer, TourismAnswers>implements TourismAnswersDao {

	
	
	@Override
	public void save(TourismAnswers tourismAnswers) {
		persist(tourismAnswers);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<TourismAnswers> pullAllAnswers(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("tourismQuestions.questionId",questionId)));
		return (List<TourismAnswers>) crit.list();
	}


	@Override
	public void update(TourismAnswers tourismAnswers) {
		getSession().update(tourismAnswers);
	}


	@Override
	public void delete(String email, long answerId) {
		String hql = "delete from Tourism_Answers_Table where realUser.email = :email and answerId = :answerId ";
		getSession().createQuery(hql)
					.setParameter("email", email)
					.setParameter("answerId", answerId)
					.executeUpdate();
		
	}


	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		
	    Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",principalEmail))
			.add(Restrictions.eq("tourismQuestions.questionId",questionId));
		if (crit.uniqueResult() != null ) {
			return true;
		}
		
		else {
			return false;
		}
		
	}


	@Override
	public TourismAnswers findByEmailAndAnswerId(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("answerId",answerId));
		return (TourismAnswers)crit.uniqueResult();
	}


	@Override
	public TourismAnswers findAnswer(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("answerId", answerId));
		return (TourismAnswers) crit.uniqueResult();
	}


	@Override
	public void remove(long questionId) {
		String hql = "delete from Tourism_Answers_Table where tourismQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
		
	}


	
	
	

}
