package com.KashmirConnect.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.RealUser;

@Repository("realUserDao")
public class RealUserDaoImpl extends AbstractDao<Integer, RealUser> implements RealUserDao {

	
	
	@Override
	public RealUser findByEmail(String email) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("email", email)));
		return (RealUser) crit.uniqueResult();
	}

	@Override
	public void save(RealUser realUser) {
		persist(realUser);
		
	}
	
	public void update(RealUser realUser) {
		getSession().update(realUser);
	}


	@SuppressWarnings("unchecked")
	public List<RealUser> sendEmailToGroup(String role) {
		Criteria crit = createEntityCriteria();
		crit.add((Restrictions.eq("role",role)));
		return (List<RealUser>) crit.list();
		
	}

}
