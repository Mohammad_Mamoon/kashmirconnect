package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.ArtsAnswerVotes;


@Repository("artsAnswerVotesDao")
public class ArtsAnswerVotesDaoImpl extends AbstractDao<Integer, ArtsAnswerVotes> implements ArtsAnswerVotesDao {

	@Override
	public void save(ArtsAnswerVotes artsAnswerVotes) {
		persist(artsAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ArtsAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("artsAnswers.answerId", answerId));
		return (List<ArtsAnswerVotes>) crit.list();
	}

	@Override
	public ArtsAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("artsAnswers.answerId",answerId));
		return (ArtsAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(ArtsAnswerVotes artsAnswerVotes) {
		getSession().update(artsAnswerVotes);
		
	}
}
