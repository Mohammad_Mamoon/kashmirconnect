package com.KashmirConnect.dao;

import com.KashmirConnect.model.TourismQuestionAttributes;

public interface TourismQuestionAttributesDao {

	void increaseUpvoteCountValue(long questionId);
	void increaseDownvoteCountValue(long questionId);
	TourismQuestionAttributes findQuestion(long questionId);
	void update(TourismQuestionAttributes tourismQuestionAttributes);
	void save(TourismQuestionAttributes tourismQuestionAttributes);
	TourismQuestionAttributes findDetails(long questionId);

}
