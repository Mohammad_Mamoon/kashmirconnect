package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.EnvironmentQuestions;



public interface EnvironmentQuestionsDao {

	void save(EnvironmentQuestions environmentQuestions);

	List<EnvironmentQuestions> pullAllQuestions();

	EnvironmentQuestions findQuestion(long questionId);

	long[] getQuestionIds(String principalEmail);

	List<EnvironmentQuestions> getQuestionList(String principalEmail);
	
	List<EnvironmentQuestions> getQuestions(String principalEmail);

	void update(EnvironmentQuestions environmentQuestions);

	void delete(EnvironmentQuestions environmentQuestions);
}
