package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.LanguagesAnswerVotes;


public interface LanguagesAnswerVotesDao {

	void save(LanguagesAnswerVotes languagesAnswerVotes);

	List<LanguagesAnswerVotes> findUpvoteCount(long answerId);

	LanguagesAnswerVotes checkEntry(String email, long answerId);

	void update(LanguagesAnswerVotes languagesAnswerVotes);
}
