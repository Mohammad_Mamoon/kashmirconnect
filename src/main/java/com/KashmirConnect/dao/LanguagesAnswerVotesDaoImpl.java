package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.LanguagesAnswerVotes;


@Repository("languagesAnswerVotesDao")
public class LanguagesAnswerVotesDaoImpl extends AbstractDao<Integer, LanguagesAnswerVotes> implements LanguagesAnswerVotesDao  {

	@Override
	public void save(LanguagesAnswerVotes languagesAnswerVotes) {
		persist(languagesAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LanguagesAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("languagesAnswers.answerId", answerId));
		return (List<LanguagesAnswerVotes>) crit.list();
	}

	@Override
	public LanguagesAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("languagesAnswers.answerId",answerId));
		return (LanguagesAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(LanguagesAnswerVotes languagesAnswerVotes) {
		getSession().update(languagesAnswerVotes);
		
	}

}
