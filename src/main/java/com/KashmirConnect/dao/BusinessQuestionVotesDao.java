package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.BusinessQuestionVotes;


public interface BusinessQuestionVotesDao {

	void save(BusinessQuestionVotes businessQuestionVotes);

	List<BusinessQuestionVotes> findUpvoteCount(long questionId);

	BusinessQuestionVotes checkEntry(String email, long questionId);

	void update(BusinessQuestionVotes businessQuestionVotes);

	void delete(long questionId);

}
