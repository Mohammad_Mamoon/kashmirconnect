package com.KashmirConnect.dao;

import java.util.List;

import com.KashmirConnect.model.TourismQuestionVotes;

public interface TourismQuestionVotesDao {
	
	void save(TourismQuestionVotes tourismQuestionVotes);

	List<TourismQuestionVotes> findUpvoteCount(long questionId);

	TourismQuestionVotes checkEntry(String email, long questionId);

	void update(TourismQuestionVotes tourismQuestionVotes);

	void delete(long questionId);

}
