package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.MedicineQuestionVotes;

@Repository("medicineQuestionVotesDao")
public class MedicineQuestionVotesDaoImpl extends AbstractDao<Integer, MedicineQuestionVotes> implements MedicineQuestionVotesDao {

	@Override
	public void save(MedicineQuestionVotes medicineQuestionVotes) {
		persist(medicineQuestionVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicineQuestionVotes> findUpvoteCount(long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("medicineQuestions.questionId", questionId));
		return (List<MedicineQuestionVotes>) crit.list();
	}

	@Override
	public MedicineQuestionVotes checkEntry(String email, long questionId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("medicineQuestions.questionId",questionId));
		return (MedicineQuestionVotes)crit.uniqueResult();
	}

	@Override
	public void update(MedicineQuestionVotes medicineQuestionVotes) {
		getSession().update(medicineQuestionVotes);
		
	}

	@Override
	public void delete(long questionId) {
		String hql = "delete from Medicine_Question_Votes_Table where medicineQuestions.questionId = :questionId ";
		getSession().createQuery(hql)
					.setParameter("questionId", questionId)
					.executeUpdate();
		
	}

}
