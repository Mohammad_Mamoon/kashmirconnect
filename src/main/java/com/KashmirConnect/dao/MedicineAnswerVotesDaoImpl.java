package com.KashmirConnect.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.KashmirConnect.model.MedicineAnswerVotes;

@Repository("medicineAnswerVotesDao")
public class MedicineAnswerVotesDaoImpl  extends AbstractDao<Integer, MedicineAnswerVotes> implements MedicineAnswerVotesDao {

	@Override
	public void save(MedicineAnswerVotes medicineAnswerVotes) {
		persist(medicineAnswerVotes);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicineAnswerVotes> findUpvoteCount(long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("medicineAnswers.answerId", answerId));
		return (List<MedicineAnswerVotes>) crit.list();
	}

	@Override
	public MedicineAnswerVotes checkEntry(String email, long answerId) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("realUser.email",email))
			.add(Restrictions.eq("medicineAnswers.answerId",answerId));
		return (MedicineAnswerVotes)crit.uniqueResult();
	}

	@Override
	public void update(MedicineAnswerVotes medicineAnswerVotes) {
		getSession().update(medicineAnswerVotes);
		
	}
}
