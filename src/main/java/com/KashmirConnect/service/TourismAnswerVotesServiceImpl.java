package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TourismAnswerVotesDao;

import com.KashmirConnect.model.TourismAnswerVotes;


@Service("tourismAnswerVotesService")
@Transactional
public class TourismAnswerVotesServiceImpl implements TourismAnswerVotesService {

	@Autowired 
	TourismAnswerVotesDao dao;


	@Override
	public void save(TourismAnswerVotes tourismAnswerVotes) {
		dao.save(tourismAnswerVotes);
		
	}


	@Override
	public List<TourismAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public TourismAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(TourismAnswerVotes tourismAnswerVotes) {
		dao.update(tourismAnswerVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	

}