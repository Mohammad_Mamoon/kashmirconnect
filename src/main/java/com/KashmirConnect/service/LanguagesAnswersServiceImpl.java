package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.LanguagesAnswersDao;

import com.KashmirConnect.model.LanguagesAnswers;


@Service("languagesAnswersService")
@Transactional
public class LanguagesAnswersServiceImpl implements LanguagesAnswersService {

	@Autowired
	LanguagesAnswersDao dao;

	

	@Override
	public void save(LanguagesAnswers languagesAnswers) {
		dao.save(languagesAnswers);
		
	}

	@Override
	public List<LanguagesAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(LanguagesAnswers languagesAnswers) {
		dao.update(languagesAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public LanguagesAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public LanguagesAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	
}
