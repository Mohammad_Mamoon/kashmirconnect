package com.KashmirConnect.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TourismAnswerAttributesDao;
import com.KashmirConnect.model.TourismAnswerAttributes;


@Service("tourismAnswerAttributesService")
@Transactional
public class TourismAnswerAttributesServiceImpl implements TourismAnswerAttributesService  {

	
	@Autowired 
	TourismAnswerAttributesDao dao;

	@Override
	public void increaseUpvoteCountValue(long answerId) {
		dao.increaseUpvoteCountValue(answerId);
		
	}

	@Override
	public void increaseDownvoteCountValue(long answerId) {
		dao.increaseDownvoteCountValue(answerId);
		
	}


	@Override
	public void update(TourismAnswerAttributes tourismAnswerAttributes) {
		dao.update(tourismAnswerAttributes);
		
	}

	@Override
	public void save(TourismAnswerAttributes tourismAnswerAttributes) {
		dao.save(tourismAnswerAttributes);
		
	}

	@Override
	public TourismAnswerAttributes findDetails(long answerId) {
		return dao.findDetails(answerId);
	}

	@Override
	public TourismAnswerAttributes findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}
	
	


	
}
