package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.ArtsQuestionVotes;


public interface ArtsQuestionVotesService {

	void save(ArtsQuestionVotes artsQuestionVotes);

	List<ArtsQuestionVotes> findUpvoteCount(long questionId);

	ArtsQuestionVotes checkEntry(String email, long questionId);

	void update(ArtsQuestionVotes artsQuestionVotes);

	void delete(long questionId);
}
