package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.BusinessAnswers;



public interface BusinessAnswersService {

	void save(BusinessAnswers businessAnswers);
	List<BusinessAnswers> pullAllAnswers(long questionId);
	BusinessAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(BusinessAnswers businessAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	BusinessAnswers findAnswer(long answerId);
}
