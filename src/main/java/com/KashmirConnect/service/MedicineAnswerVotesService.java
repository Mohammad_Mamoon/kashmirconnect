package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.MedicineAnswerVotes;


public interface MedicineAnswerVotesService {

	void save(MedicineAnswerVotes medicineAnswerVotes);

	List<MedicineAnswerVotes> findUpvoteCount(long answerId);

	MedicineAnswerVotes checkEntry(String email, long answerId);

	void update(MedicineAnswerVotes medicineAnswerVotes);
}
