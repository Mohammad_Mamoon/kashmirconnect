package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.LanguagesQuestionsDao;

import com.KashmirConnect.model.LanguagesQuestions;



@Service("languagesQuestionsService")
@Transactional
public class LanguagesQuestionsServiceImpl implements LanguagesQuestionsService {

	@Autowired 
	LanguagesQuestionsDao dao;
	
	@Override
	public void save(LanguagesQuestions languagesQuestions) {
		dao.save(languagesQuestions);
		
	}

	
	public List<LanguagesQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public LanguagesQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<LanguagesQuestions> getQuestionList(String principalEmail) {
		return (List<LanguagesQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<LanguagesQuestions> getQuestions(String principalEmail) {
		return (List<LanguagesQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(LanguagesQuestions languagesQuestions) {
		dao.update(languagesQuestions);
		
	}


	@Override
	public void delete(LanguagesQuestions languagesQuestions) {
		dao.delete(languagesQuestions);
		
	}

}
