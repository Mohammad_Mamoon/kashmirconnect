package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TourismQuestionVotesDao;
import com.KashmirConnect.model.TourismQuestionVotes;


@Service("tourismQuestionVotesService")
@Transactional
public class TourismQuestionVotesServiceImpl implements TourismQuestionVotesService {

	@Autowired 
	TourismQuestionVotesDao dao;


	@Override
	public void save(TourismQuestionVotes tourismQuestionVotes) {
		dao.save(tourismQuestionVotes);
		
	}


	@Override
	public List<TourismQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public TourismQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(TourismQuestionVotes tourismQuestionVotes) {
		dao.update(tourismQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	

}
