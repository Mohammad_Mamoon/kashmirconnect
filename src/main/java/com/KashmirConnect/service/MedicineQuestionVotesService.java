package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.MedicineQuestionVotes;


public interface MedicineQuestionVotesService {

	void save(MedicineQuestionVotes medicineQuestionVotes);

	List<MedicineQuestionVotes> findUpvoteCount(long questionId);

	MedicineQuestionVotes checkEntry(String email, long questionId);

	void update(MedicineQuestionVotes medicineQuestionVotes);

	void delete(long questionId);
}
