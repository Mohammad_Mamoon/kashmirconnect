package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.MedicineQuestionVotesDao;

import com.KashmirConnect.model.MedicineQuestionVotes;


@Service("medicineQuestionVotesService")
@Transactional
public class MedicineQuestionVotesServiceImpl implements MedicineQuestionVotesService  {

	@Autowired 
	MedicineQuestionVotesDao dao;


	@Override
	public void save(MedicineQuestionVotes medicineQuestionVotes) {
		dao.save(medicineQuestionVotes);
		
	}


	@Override
	public List<MedicineQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public MedicineQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(MedicineQuestionVotes medicineQuestionVotes) {
		dao.update(medicineQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	
}
