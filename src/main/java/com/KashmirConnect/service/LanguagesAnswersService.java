package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.LanguagesAnswers;



public interface LanguagesAnswersService {

	void save(LanguagesAnswers languagesAnswers);
	List<LanguagesAnswers> pullAllAnswers(long questionId);
	LanguagesAnswers findByEmailAndAnswerId(String email, long answerId);
	void update(LanguagesAnswers languagesAnswers);
	void delete(String email, long answerId);
	boolean findIfAnswered(long questionId, String principalEmail);
	LanguagesAnswers findAnswer(long answerId);
}
