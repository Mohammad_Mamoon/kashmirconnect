package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.BookmarkQuestions;

public interface BookmarkQuestionsService {

     void save(BookmarkQuestions bookmarkQuestions);

	void delete(long questionId, String community);

	List<BookmarkQuestions> findByEmail(String email);
}
