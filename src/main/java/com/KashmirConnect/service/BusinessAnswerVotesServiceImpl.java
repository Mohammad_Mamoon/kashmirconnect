package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.BusinessAnswerVotesDao;

import com.KashmirConnect.model.BusinessAnswerVotes;


@Service("businessAnswerVotesService")
@Transactional
public class BusinessAnswerVotesServiceImpl implements BusinessAnswerVotesService  {

	@Autowired 
	BusinessAnswerVotesDao dao;


	@Override
	public void save(BusinessAnswerVotes businessAnswerVotes) {
		dao.save(businessAnswerVotes);
		
	}


	@Override
	public List<BusinessAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public BusinessAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(BusinessAnswerVotes businessAnswerVotes) {
		dao.update(businessAnswerVotes);
		
	}
	
}
