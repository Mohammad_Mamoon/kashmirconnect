package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.MedicineAnswersDao;

import com.KashmirConnect.model.MedicineAnswers;



@Service("medicineAnswersService")
@Transactional
public class MedicineAnswersServiceImpl implements MedicineAnswersService {
	
	@Autowired
	MedicineAnswersDao dao;

	

	@Override
	public void save(MedicineAnswers medicineAnswers) {
		dao.save(medicineAnswers);
		
	}

	@Override
	public List<MedicineAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(MedicineAnswers medicineAnswers) {
		dao.update(medicineAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public MedicineAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public MedicineAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	
	
}

