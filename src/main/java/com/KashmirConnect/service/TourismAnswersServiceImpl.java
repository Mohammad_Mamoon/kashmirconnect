package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.KashmirConnect.dao.TourismAnswersDao;
import com.KashmirConnect.model.TourismAnswers;


@Service("tourismAnswersService")
@Transactional
public class TourismAnswersServiceImpl implements TourismAnswersService {
	
	@Autowired
	TourismAnswersDao dao;

	

	@Override
	public void save(TourismAnswers tourismAnswers) {
		dao.save(tourismAnswers);
		
	}

	@Override
	public List<TourismAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(TourismAnswers tourismAnswers) {
		dao.update(tourismAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public TourismAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public TourismAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	@Override
	public void remove(long questionId) {
		dao.remove(questionId);
	}

	
}
