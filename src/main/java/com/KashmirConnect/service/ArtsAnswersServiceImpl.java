package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.ArtsAnswersDao;

import com.KashmirConnect.model.ArtsAnswers;



@Service("artsAnswersService")
@Transactional
public class ArtsAnswersServiceImpl  implements ArtsAnswersService {
	
	@Autowired
	ArtsAnswersDao dao;

	

	@Override
	public void save(ArtsAnswers artsAnswers) {
		dao.save(artsAnswers);
		
	}

	@Override
	public List<ArtsAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(ArtsAnswers artsAnswers) {
		dao.update(artsAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public ArtsAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public ArtsAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	
}
