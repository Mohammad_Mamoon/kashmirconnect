package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TourismQuestionsDao;

import com.KashmirConnect.model.TourismQuestions;


@Service("tourismQuestionsService")
@Transactional
public class TourismQuestionsServiceImpl implements TourismQuestionsService {

	@Autowired 
	TourismQuestionsDao dao;
	
	@Override
	public void save(TourismQuestions tourismQuestions) {
		dao.save(tourismQuestions);
		
	}

	
	public List<TourismQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public TourismQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<TourismQuestions> getQuestionList(String principalEmail) {
		return (List<TourismQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<TourismQuestions> getQuestions(String principalEmail) {
		return (List<TourismQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(TourismQuestions tourismQuestions) {
		dao.update(tourismQuestions);
		
	}


	@Override
	public void delete(TourismQuestions tourismQuestions) {
		dao.delete(tourismQuestions);
		
	}


	

}
