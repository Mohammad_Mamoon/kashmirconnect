package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.BookmarkQuestionsDao;
import com.KashmirConnect.model.BookmarkQuestions;

@Service("bookmarkQuestionsService")
@Transactional
public class BookmarkQuestionsServiceImpl implements BookmarkQuestionsService {

	@Autowired 
	BookmarkQuestionsDao dao;
	
	@Override
	public void save(BookmarkQuestions bookmarkQuestions) {
	  dao.save(bookmarkQuestions);

	}

	@Override
	public void delete(long questionId, String community) {
		dao.delete(questionId ,community);
		
	}

	@Override
	public List<BookmarkQuestions> findByEmail(String email) {
		
		return dao.findByEmail(email);
	}

}
