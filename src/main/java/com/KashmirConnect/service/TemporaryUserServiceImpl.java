package com.KashmirConnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TemporaryUserDao;

import com.KashmirConnect.model.TemporaryUser;


@Service("temporaryUserService")
@Transactional
public class TemporaryUserServiceImpl implements TemporaryUserService {

	@Autowired TemporaryUserDao dao;
	
	@Override
	public void save(TemporaryUser temporaryUser) {
		
		dao.save(temporaryUser);
		
	}

	@Override
	public TemporaryUser findByUniqueCode(String confirmCode) {
		 return  (TemporaryUser) dao.findByUnqiueCode(confirmCode);
	}

	@Override
	public TemporaryUser findByEmail(String checkEmail) {
		return  (TemporaryUser) dao.findByEmail(checkEmail);
	}

	@Override
	public void delete(String email) {
		dao.delete(email);
		
	}

}
