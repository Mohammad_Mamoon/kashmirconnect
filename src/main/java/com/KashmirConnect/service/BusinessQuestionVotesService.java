package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.BusinessQuestionVotes;


public interface BusinessQuestionVotesService {

	void save(BusinessQuestionVotes businessQuestionVotes);

	List<BusinessQuestionVotes> findUpvoteCount(long questionId);

	BusinessQuestionVotes checkEntry(String email, long questionId);

	void update(BusinessQuestionVotes businessQuestionVotes);

	void delete(long questionId);
}
