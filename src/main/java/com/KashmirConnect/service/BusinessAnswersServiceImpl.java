package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.BusinessAnswersDao;

import com.KashmirConnect.model.BusinessAnswers;



@Service("businessAnswersService")
@Transactional
public class BusinessAnswersServiceImpl implements BusinessAnswersService {
	
	@Autowired
	BusinessAnswersDao dao;

	

	@Override
	public void save(BusinessAnswers businessAnswers) {
		dao.save(businessAnswers);
		
	}

	@Override
	public List<BusinessAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(BusinessAnswers businessAnswers) {
		dao.update(businessAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public BusinessAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public BusinessAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	
}
