package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.EnvironmentQuestionVotesDao;

import com.KashmirConnect.model.EnvironmentQuestionVotes;


@Service("environmentQuestionVotesService")
@Transactional
public class EnvironmentQuestionVotesServiceImpl implements EnvironmentQuestionVotesService  {

	@Autowired 
	EnvironmentQuestionVotesDao dao;


	@Override
	public void save(EnvironmentQuestionVotes environmentQuestionVotes) {
		dao.save(environmentQuestionVotes);
		
	}


	@Override
	public List<EnvironmentQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public EnvironmentQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(EnvironmentQuestionVotes environmentQuestionVotes) {
		dao.update(environmentQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	
}
