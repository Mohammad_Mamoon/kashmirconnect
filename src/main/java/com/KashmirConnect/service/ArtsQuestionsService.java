package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.ArtsQuestions;



public interface ArtsQuestionsService {

	void save(ArtsQuestions artsQuestions);

	List<ArtsQuestions> pullAllQuestions();

	ArtsQuestions findQuestion(long questionId);

	List<ArtsQuestions> getQuestionList(String principalEmail);
	
	List<ArtsQuestions> getQuestions(String principalEmail);

	void update(ArtsQuestions artsQuestions);

	void delete(ArtsQuestions artsQuestions);
}
