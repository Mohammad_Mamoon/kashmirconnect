package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.RealUserDao;
import com.KashmirConnect.model.RealUser;

@Service("realUserService")
@Transactional
public class RealUserServiceImpl implements RealUserService {
	
	@Autowired 
	RealUserDao dao;

	@Override
	public void save(RealUser realUser) {
		dao.save(realUser);
		
	}

	@Override
	public RealUser findByEmail(String email) {
		 return  (RealUser) dao.findByEmail(email);
	}

	@Override
	public void update(RealUser realUser) {
		dao.update(realUser);
		
	}
	
	@Override
	public List<RealUser> sendEmailToGroup(String role) {
		
		return dao.sendEmailToGroup(role);
	}

}
