package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.EnvironmentQuestionsDao;

import com.KashmirConnect.model.EnvironmentQuestions;



@Service("environmentQuestionsService")
@Transactional
public class EnvironmentQuestionsServiceImpl implements EnvironmentQuestionsService {

	@Autowired 
	EnvironmentQuestionsDao dao;
	
	@Override
	public void save(EnvironmentQuestions environmentQuestions) {
		dao.save(environmentQuestions);
		
	}

	
	public List<EnvironmentQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public EnvironmentQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<EnvironmentQuestions> getQuestionList(String principalEmail) {
		return (List<EnvironmentQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<EnvironmentQuestions> getQuestions(String principalEmail) {
		return (List<EnvironmentQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(EnvironmentQuestions environmentQuestions) {
		dao.update(environmentQuestions);
		
	}


	@Override
	public void delete(EnvironmentQuestions environmentQuestions) {
		dao.delete(environmentQuestions);
		
	}


}
