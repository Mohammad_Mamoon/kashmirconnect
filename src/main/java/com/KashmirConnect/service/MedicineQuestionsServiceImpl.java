package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.MedicineQuestionsDao;

import com.KashmirConnect.model.MedicineQuestions;



@Service("medicineQuestionsService")
@Transactional
public class MedicineQuestionsServiceImpl implements MedicineQuestionsService {
	
	@Autowired 
	MedicineQuestionsDao dao;
	
	@Override
	public void save(MedicineQuestions medicineQuestions) {
		dao.save(medicineQuestions);
		
	}

	
	public List<MedicineQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public MedicineQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<MedicineQuestions> getQuestionList(String principalEmail) {
		return (List<MedicineQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<MedicineQuestions> getQuestions(String principalEmail) {
		return (List<MedicineQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(MedicineQuestions medicineQuestions) {
		dao.update(medicineQuestions);
		
	}


	@Override
	public void delete(MedicineQuestions medicineQuestions) {
		dao.delete(medicineQuestions);
		
	}


	

}
