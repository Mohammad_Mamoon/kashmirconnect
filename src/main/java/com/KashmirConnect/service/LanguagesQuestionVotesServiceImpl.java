package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.LanguagesQuestionVotesDao;

import com.KashmirConnect.model.LanguagesQuestionVotes;


@Service("languagesQuestionVotesService")
@Transactional
public class LanguagesQuestionVotesServiceImpl implements LanguagesQuestionVotesService  {

	@Autowired 
	LanguagesQuestionVotesDao dao;


	@Override
	public void save(LanguagesQuestionVotes languagesQuestionVotes) {
		dao.save(languagesQuestionVotes);
		
	}


	@Override
	public List<LanguagesQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public LanguagesQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(LanguagesQuestionVotes languagesQuestionVotes) {
		dao.update(languagesQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	
}
