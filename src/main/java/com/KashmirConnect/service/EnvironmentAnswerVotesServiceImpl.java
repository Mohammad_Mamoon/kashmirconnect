package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.EnvironmentAnswerVotesDao;

import com.KashmirConnect.model.EnvironmentAnswerVotes;


@Service("environmentAnswerVotesService")
@Transactional
public class EnvironmentAnswerVotesServiceImpl implements EnvironmentAnswerVotesService  {

	@Autowired 
	EnvironmentAnswerVotesDao dao;


	@Override
	public void save(EnvironmentAnswerVotes environmentAnswerVotes) {
		dao.save(environmentAnswerVotes);
		
	}


	@Override
	public List<EnvironmentAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public EnvironmentAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(EnvironmentAnswerVotes environmentAnswerVotes) {
		dao.update(environmentAnswerVotes);
		
	}
	
}
