package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.ArtsQuestionVotesDao;

import com.KashmirConnect.model.ArtsQuestionVotes;


@Service("artsQuestionVotesService")
@Transactional
public class ArtsQuestionVotesServiceImpl implements ArtsQuestionVotesService  {

	@Autowired 
	ArtsQuestionVotesDao dao;


	@Override
	public void save(ArtsQuestionVotes artsQuestionVotes) {
		dao.save(artsQuestionVotes);
		
	}


	@Override
	public List<ArtsQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public ArtsQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(ArtsQuestionVotes artsQuestionVotes) {
		dao.update(artsQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	
}
