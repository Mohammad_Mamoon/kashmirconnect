package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.EnvironmentQuestionVotes;


public interface EnvironmentQuestionVotesService {

	void save(EnvironmentQuestionVotes environmentQuestionVotes);

	List<EnvironmentQuestionVotes> findUpvoteCount(long questionId);

	EnvironmentQuestionVotes checkEntry(String email, long questionId);

	void update(EnvironmentQuestionVotes environmentQuestionVotes);

	void delete(long questionId);
}
