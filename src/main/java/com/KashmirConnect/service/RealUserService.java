package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.RealUser;


public interface RealUserService {
	
	void save(RealUser realUser);
	RealUser findByEmail(String email);
	void update(RealUser realUser);
	List<RealUser> sendEmailToGroup(String role);
	

}
