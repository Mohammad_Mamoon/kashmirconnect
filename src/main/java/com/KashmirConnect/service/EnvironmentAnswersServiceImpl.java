package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.EnvironmentAnswersDao;

import com.KashmirConnect.model.EnvironmentAnswers;



@Service("environmentAnswersService")
@Transactional
public class EnvironmentAnswersServiceImpl implements EnvironmentAnswersService {

	@Autowired
	EnvironmentAnswersDao dao;

	

	@Override
	public void save(EnvironmentAnswers environmentAnswers) {
		dao.save(environmentAnswers);
		
	}

	@Override
	public List<EnvironmentAnswers> pullAllAnswers(long questionId) {
		return dao.pullAllAnswers(questionId);
	}


	@Override
	public void update(EnvironmentAnswers environmentAnswers) {
		dao.update(environmentAnswers);
		
	}

	@Override
	public void delete(String email, long answerId) {
		dao.delete(email,answerId);
		
	}

	@Override
	public boolean findIfAnswered(long questionId, String principalEmail) {
		return dao.findIfAnswered(questionId, principalEmail);
		
	}

	@Override
	public EnvironmentAnswers findByEmailAndAnswerId(String email, long answerId) {
		return dao.findByEmailAndAnswerId(email,answerId);
	}

	@Override
	public EnvironmentAnswers findAnswer(long answerId) {
		return dao.findAnswer(answerId);
	}

	
}
