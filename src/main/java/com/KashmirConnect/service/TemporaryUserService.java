package com.KashmirConnect.service;

import com.KashmirConnect.model.TemporaryUser;

public interface TemporaryUserService {
	
	void save(TemporaryUser temporaryUser);
	TemporaryUser findByUniqueCode(String confirmCode);
	TemporaryUser findByEmail(String checkEmail);
	void delete(String email);
	
	
}
