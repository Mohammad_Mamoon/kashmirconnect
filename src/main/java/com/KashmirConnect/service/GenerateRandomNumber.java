package com.KashmirConnect.service;

import java.util.Random;

public class GenerateRandomNumber {
	
	public static int generateNumber() {
		
		Random random = new Random(System.nanoTime());
		int rand = random.nextInt(10000000);
		return rand;
	}

}
