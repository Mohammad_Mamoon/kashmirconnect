package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.LanguagesAnswerVotesDao;

import com.KashmirConnect.model.LanguagesAnswerVotes;


@Service("languagesAnswerVotesService")
@Transactional
public class LanguagesAnswerVotesServiceImpl implements LanguagesAnswerVotesService  {

	@Autowired 
	LanguagesAnswerVotesDao dao;


	@Override
	public void save(LanguagesAnswerVotes languagesAnswerVotes) {
		dao.save(languagesAnswerVotes);
		
	}


	@Override
	public List<LanguagesAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public LanguagesAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(LanguagesAnswerVotes languagesAnswerVotes) {
		dao.update(languagesAnswerVotes);
		
	}
	
}
