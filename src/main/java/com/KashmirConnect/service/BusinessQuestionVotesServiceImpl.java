package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.BusinessQuestionVotesDao;

import com.KashmirConnect.model.BusinessQuestionVotes;


@Service("businessQuestionVotesService")
@Transactional
public class BusinessQuestionVotesServiceImpl implements BusinessQuestionVotesService  {

	@Autowired 
	BusinessQuestionVotesDao dao;


	@Override
	public void save(BusinessQuestionVotes businessQuestionVotes) {
		dao.save(businessQuestionVotes);
		
	}


	@Override
	public List<BusinessQuestionVotes> findUpvoteCount(long questionId) {
		return dao.findUpvoteCount(questionId);
	}


	@Override
	public BusinessQuestionVotes checkEntry(String email, long questionId) {
		return dao.checkEntry(email,questionId);
	}


	@Override
	public void update(BusinessQuestionVotes businessQuestionVotes) {
		dao.update(businessQuestionVotes);
		
	}


	@Override
	public void delete(long questionId) {
		dao.delete(questionId);
		
	}
	
}
