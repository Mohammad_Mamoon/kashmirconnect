package com.KashmirConnect.service;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.model.RealUser;

@Service("sendGroupEmailService")
@Transactional
public class SendGroupEmailService {
	
	@Autowired
	RealUserService realUserService;
	
	 @Autowired
    JavaMailSender mailSender;
	 
	
	 
    public String sendEmailToGroup(String email) {
    	
    	RealUser realUser;
    	realUser = realUserService.findByEmail(email);
    	
    	List<RealUser> list  = realUserService.sendEmailToGroup(realUser.getRole());
    	list.remove(realUserService.findByEmail(email));
    	String status = null;
    	
    	for(RealUser user : list) {
    		

    	        try {
    	            MimeMessage message = mailSender.createMimeMessage();
    	            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
    	            helper.setFrom("Administrator");
    	            helper.setTo(user.getEmail());
    	            helper.setSubject("A new question has been posed");
    	            String text = 
    	                    "Hey, " + "<b>" +user.getUserName() +"</b>" + ", a new question has been asked! " +  "<br/>"
    	                    + "You can click here " + "http://localhost/www.KashmirConnect/" + user.getRole() + " to directly see the question list"
    	                    		+ " in the community "
    	                    + "or you can check the homepage " + "http://localhost/www.KashmirConnect/home"; 
    	                    

    	            helper.setText(text, true);
    	            mailSender.send(message);
    	            status = "Your community members have been alerted!";
    	        } catch (MessagingException e) {
    	            status = "Error in sending email to every community member"; 
    	        }
    	        
    	}
    	
    	return status;

    }
    

}
