package com.KashmirConnect.service;

import java.util.Comparator;

import com.KashmirConnect.model.TourismQuestions;

public class TimeCompare implements Comparator<TourismQuestions> {

	

	@Override
	public int compare(TourismQuestions t1, TourismQuestions t2) {
		
		return t2.getTimeAsked().compareTo(t1.getTimeAsked());
	}
	

}
