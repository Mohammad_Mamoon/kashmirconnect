package com.KashmirConnect.service;

import java.util.List;
import com.KashmirConnect.model.MedicineQuestions;


public interface MedicineQuestionsService {

	void save(MedicineQuestions medicineQuestions);

	List<MedicineQuestions> pullAllQuestions();

	MedicineQuestions findQuestion(long questionId);

	List<MedicineQuestions> getQuestionList(String principalEmail);
	
	List<MedicineQuestions> getQuestions(String principalEmail);

	void update(MedicineQuestions medicineQuestions);

	void delete(MedicineQuestions medicineQuestions);
}
