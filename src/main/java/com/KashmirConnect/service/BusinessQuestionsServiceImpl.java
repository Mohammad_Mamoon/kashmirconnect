package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.BusinessQuestionsDao;

import com.KashmirConnect.model.BusinessQuestions;



@Service("businessQuestionsService")
@Transactional
public class BusinessQuestionsServiceImpl implements BusinessQuestionsService {

	@Autowired 
	BusinessQuestionsDao dao;
	
	@Override
	public void save(BusinessQuestions businessQuestions) {
		dao.save(businessQuestions);
		
	}

	
	public List<BusinessQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public BusinessQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<BusinessQuestions> getQuestionList(String principalEmail) {
		return (List<BusinessQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<BusinessQuestions> getQuestions(String principalEmail) {
		return (List<BusinessQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(BusinessQuestions businessQuestions) {
		dao.update(businessQuestions);
		
	}


	@Override
	public void delete(BusinessQuestions businessQuestions) {
		dao.delete(businessQuestions);
		
	}


}
