package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.ArtsAnswerVotesDao;

import com.KashmirConnect.model.ArtsAnswerVotes;


@Service("artsAnswerVotesService")
@Transactional
public class ArtsAnswerVotesServiceImpl implements ArtsAnswerVotesService  {

	@Autowired 
	ArtsAnswerVotesDao dao;


	@Override
	public void save(ArtsAnswerVotes artsAnswerVotes) {
		dao.save(artsAnswerVotes);
		
	}


	@Override
	public List<ArtsAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public ArtsAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(ArtsAnswerVotes artsAnswerVotes) {
		dao.update(artsAnswerVotes);
		
	}
	
}
