package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.EnvironmentQuestions;


public interface EnvironmentQuestionsService {

	void save(EnvironmentQuestions environmentQuestions);

	List<EnvironmentQuestions> pullAllQuestions();

	EnvironmentQuestions findQuestion(long questionId);

	List<EnvironmentQuestions> getQuestionList(String principalEmail);
	
	List<EnvironmentQuestions> getQuestions(String principalEmail);

	void update(EnvironmentQuestions environmentQuestions);

	void delete(EnvironmentQuestions environmentQuestions);
}
