package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.MedicineAnswerVotesDao;

import com.KashmirConnect.model.MedicineAnswerVotes;


@Service("medicineAnswerVotesService")
@Transactional
public class MedicineAnswerVotesServiceImpl implements MedicineAnswerVotesService  {

	@Autowired 
	MedicineAnswerVotesDao dao;


	@Override
	public void save(MedicineAnswerVotes medicineAnswerVotes) {
		dao.save(medicineAnswerVotes);
		
	}


	@Override
	public List<MedicineAnswerVotes> findUpvoteCount(long answerId) {
		return dao.findUpvoteCount(answerId);
	}


	@Override
	public MedicineAnswerVotes checkEntry(String email, long answerId) {
		return dao.checkEntry(email, answerId);
	}


	@Override
	public void update(MedicineAnswerVotes medicineAnswerVotes) {
		dao.update(medicineAnswerVotes);
		
	}
	
}
