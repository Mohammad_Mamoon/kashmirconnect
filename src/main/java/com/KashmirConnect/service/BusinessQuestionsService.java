package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.BusinessQuestions;



public interface BusinessQuestionsService {

	void save(BusinessQuestions businessQuestions);

	List<BusinessQuestions> pullAllQuestions();

	BusinessQuestions findQuestion(long questionId);

	List<BusinessQuestions> getQuestionList(String principalEmail);
	
	List<BusinessQuestions> getQuestions(String principalEmail);

	void update(BusinessQuestions businessQuestions);

	void delete(BusinessQuestions businessQuestions);
}
