package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.LanguagesQuestions;



public interface LanguagesQuestionsService {

	void save(LanguagesQuestions languagesQuestions);

	List<LanguagesQuestions> pullAllQuestions();

	LanguagesQuestions findQuestion(long questionId);

	List<LanguagesQuestions> getQuestionList(String principalEmail);
	
	List<LanguagesQuestions> getQuestions(String principalEmail);

	void update(LanguagesQuestions languagesQuestions);

	void delete(LanguagesQuestions languagesQuestions);
}
