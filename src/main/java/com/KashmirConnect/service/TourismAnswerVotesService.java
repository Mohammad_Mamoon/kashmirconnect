package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.TourismAnswerVotes;


public interface TourismAnswerVotesService {
	
	void save(TourismAnswerVotes tourismAnswerVotes);

	List<TourismAnswerVotes> findUpvoteCount(long answerId);

	TourismAnswerVotes checkEntry(String email, long answerId);

	void update(TourismAnswerVotes tourismAnswerVotes);

	void delete(long questionId);

}
