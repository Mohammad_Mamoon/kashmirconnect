package com.KashmirConnect.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.ArtsQuestionsDao;

import com.KashmirConnect.model.ArtsQuestions;



@Service("artsQuestionsService")
@Transactional
public class ArtsQuestionsServiceImpl implements ArtsQuestionsService{

	@Autowired 
	ArtsQuestionsDao dao;
	
	@Override
	public void save(ArtsQuestions artsQuestions) {
		dao.save(artsQuestions);
		
	}

	
	public List<ArtsQuestions> pullAllQuestions() {
		return dao.pullAllQuestions();
	}


	@Override
	public ArtsQuestions findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}


	@Override
	public List<ArtsQuestions> getQuestionList(String principalEmail) {
		return (List<ArtsQuestions>) dao.getQuestionList(principalEmail);
	}


	@Override
	public List<ArtsQuestions> getQuestions(String principalEmail) {
		return (List<ArtsQuestions>) dao.getQuestions(principalEmail);
	}


	@Override
	public void update(ArtsQuestions artsQuestions) {
		dao.update(artsQuestions);
		
	}


	@Override
	public void delete(ArtsQuestions artsQuestions) {
		dao.delete(artsQuestions);
		
	}


}
