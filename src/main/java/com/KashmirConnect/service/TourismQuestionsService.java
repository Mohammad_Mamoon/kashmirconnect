package com.KashmirConnect.service;

import java.util.List;

import com.KashmirConnect.model.TourismQuestions;

public interface TourismQuestionsService {
	
	void save(TourismQuestions tourismQuestions);

	List<TourismQuestions> pullAllQuestions();

	TourismQuestions findQuestion(long questionId);

	List<TourismQuestions> getQuestionList(String principalEmail);
	
	List<TourismQuestions> getQuestions(String principalEmail);

	void update(TourismQuestions tourismQuestions);

	void delete(TourismQuestions tourismQuestions);
	

}
