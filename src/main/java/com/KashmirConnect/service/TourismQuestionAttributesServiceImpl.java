package com.KashmirConnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.KashmirConnect.dao.TourismQuestionAttributesDao;

import com.KashmirConnect.model.TourismQuestionAttributes;

@Service("tourismQuestionAttributesService")
@Transactional
public class TourismQuestionAttributesServiceImpl implements TourismQuestionAttributesService {

	
	@Autowired 
	TourismQuestionAttributesDao dao;
	
	


	@Override
	public void increaseUpvoteCountValue(long questionId) {
		dao.increaseUpvoteCountValue(questionId);
		
	}

	@Override
	public void increaseDownvoteCountValue(long questionId) {
		dao.increaseDownvoteCountValue(questionId);
		
	}

	@Override
	public TourismQuestionAttributes findQuestion(long questionId) {
		return dao.findQuestion(questionId);
	}

	@Override
	public void update(TourismQuestionAttributes tourismQuestionAttributes) {
		dao.update(tourismQuestionAttributes);
		
	}

	@Override
	public void save(TourismQuestionAttributes tourismQuestionAttributes) {
		dao.save(tourismQuestionAttributes);
	}

	@Override
	public TourismQuestionAttributes findDetails(long questionId) {
		
		return dao.findDetails(questionId);
	}

}
