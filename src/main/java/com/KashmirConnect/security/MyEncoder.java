package com.KashmirConnect.security;

import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

public class MyEncoder {
	public static String encode(String rawPassword) {
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
		 return encoder.encodePassword(rawPassword, null);
				 
	}

}
