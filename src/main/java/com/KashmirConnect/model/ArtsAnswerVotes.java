package com.KashmirConnect.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity ( name = "Arts_Answer_Votes_Table")
public class ArtsAnswerVotes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	
	@Column(name="Downvote" ,columnDefinition = "tinyint(1) default 0")
	private boolean downvote = Boolean.FALSE;
	
	@Column(name="Upvote",columnDefinition = "tinyint(1) default 0")
	private boolean upvote = Boolean.FALSE;

	@Column(name="Flag",columnDefinition = "tinyint(1) default 0")
	private boolean flag = Boolean.FALSE;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Answer_ID" )
	private ArtsAnswers artsAnswers;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDownvote() {
		return downvote;
	}

	public void setDownvote(boolean downvote) {
		this.downvote = downvote;
	}

	public boolean isUpvote() {
		return upvote;
	}

	public void setUpvote(boolean upvote) {
		this.upvote = upvote;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public ArtsAnswers getArtsAnswers() {
		return artsAnswers;
	}

	public void setArtsAnswers(ArtsAnswers artsAnswers) {
		this.artsAnswers = artsAnswers;
	}

	public RealUser getRealUser() {
		return realUser;
	}

	public void setRealUser(RealUser realUser) {
		this.realUser = realUser;
	}
	
	
}
