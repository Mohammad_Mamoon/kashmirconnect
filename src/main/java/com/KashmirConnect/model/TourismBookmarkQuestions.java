package com.KashmirConnect.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity( name = "Tourism_Bookmark_Question_Table")
public class TourismBookmarkQuestions {

	@Id
	@Column(name="Id", nullable=false)
	private int Id;
	
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;
	
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Question_ID" )
	private TourismQuestions tourismQuestions;
	
	
}