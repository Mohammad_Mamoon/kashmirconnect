package com.KashmirConnect.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity( name = "Environment_Question_Table")
public class EnvironmentQuestions implements Comparable<EnvironmentQuestions>{

	@Id
	@Column(name="QuestionId", nullable=false)
	private long questionId;
	
	@Column(name="QuestionList", nullable=false)
	private String question;
	
	@Column(name="TimeAsked", nullable=false)
	private Date timeAsked;


	@Column(name="UpvotesCount")
	private int upvotesCount;
	
	@Column(name="DownvotesCount")
	private int downvotesCount;
	
	@Column(name="FlagCount")
	private int flagCount;
	
	@Column(name="BookmarkCount")
	private int bookmarkCount;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;
	
	
	public long getQuestionId() {
		return questionId;
	}
	
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Date getTimeAsked() {
		return timeAsked;
	}
	public void setTimeAsked(Date timeAsked) {
		this.timeAsked = timeAsked;
	}

	public RealUser getRealUser() {
		return realUser;
	}

	public void setRealUser(RealUser realUser) {
		this.realUser = realUser;
	}

	public int getUpvotesCount() {
		return upvotesCount;
	}

	public void setUpvotesCount(int upvotesCount) {
		this.upvotesCount = upvotesCount;
	}

	public int getDownvotesCount() {
		return downvotesCount;
	}

	public void setDownvotesCount(int downvotesCount) {
		this.downvotesCount = downvotesCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}
	
	
	
	public int getBookmarkCount() {
		return bookmarkCount;
	}

	public void setBookmarkCount(int bookmarkCount) {
		this.bookmarkCount = bookmarkCount;
	}

	@Override
	public int compareTo(EnvironmentQuestions obj2) {
		return obj2.getUpvotesCount() - this.getUpvotesCount(); 
	}
	
}
