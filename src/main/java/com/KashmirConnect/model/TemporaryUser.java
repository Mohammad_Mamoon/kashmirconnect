package com.KashmirConnect.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;




@Entity(name="Temporary_User")
public class TemporaryUser  {
	

	@Id 
	@Column(name="Email", unique=true, nullable=false)
	private String email;

	@Column(name="FirstName",nullable=false)
	private String firstName;
	
	@Column(name="LastName", nullable=false)
	private String lastName;
		
	@Column(name="Password", nullable=false)
	private String password;

	@Column(name="Unique_Code", nullable=false)
	private String uniqueCode;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	
	
}