package com.KashmirConnect.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;





@Entity(name="Real_User")
public class RealUser {

	@Id 
	@Column(name="Email", unique=true, nullable=false)
	private String email;

	@Column(name="FirstName", nullable=false)
	private String firstName;
	
	@Column(name="LastName", nullable=false)
	private String lastName;
		
	@Column(name="Password", nullable=false)
	private String password;

	@Column(name="Role", nullable=false)
	private String role;
	
	@Column(name="Enabled", nullable=false)
	private boolean enabled;
	
	@Column(name="Username", nullable=false)
	private String userName;
	
	@Column(name="Status", nullable=false)
	private String status;
	
	@Column(name="Profile", nullable=false)
	private String profile;
	
	@Column(name="Description", nullable=false)
	private String description;
	
	@Column(name="MySite", nullable=false)
	private String mySite;

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMySite() {
		return mySite;
	}

	public void setMySite(String mySite) {
		this.mySite = mySite;
	}
	
	

		
}