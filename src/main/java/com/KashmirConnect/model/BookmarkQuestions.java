package com.KashmirConnect.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity ( name = "Bookmark_Question_Table")
public class BookmarkQuestions {
	
	@Id
	@Column(name="Id")
	private int id;
	
	@Column(name="Community")
	private String commuity;
	
	
	@Column(name = "QuestionId" )
	private long questionId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getCommuity() {
		return commuity;
	}

	public void setCommuity(String commuity) {
		this.commuity = commuity;
	}

	

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public RealUser getRealUser() {
		return realUser;
	}

	public void setRealUser(RealUser realUser) {
		this.realUser = realUser;
	}
	
	

}
