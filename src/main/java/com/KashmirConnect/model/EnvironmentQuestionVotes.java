package com.KashmirConnect.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity ( name = "Environment_Question_Votes_Table")
public class EnvironmentQuestionVotes {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	
	@Column(name="Downvote" ,columnDefinition = "tinyint(1) default 0")
	private boolean downvote = Boolean.FALSE;
	
	@Column(name="Upvote",columnDefinition = "tinyint(1) default 0")
	private boolean upvote = Boolean.FALSE;

	@Column(name="Flag",columnDefinition = "tinyint(1) default 0")
	private boolean flag = Boolean.FALSE;
	
	@Column(name="Bookmark",columnDefinition = "tinyint(1) default 0")
	private boolean bookmark = Boolean.FALSE;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Question_ID" )
	private EnvironmentQuestions environmentQuestions;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDownvote() {
		return downvote;
	}

	public void setDownvote(boolean downvote) {
		this.downvote = downvote;
	}

	public boolean isUpvote() {
		return upvote;
	}

	public void setUpvote(boolean upvote) {
		this.upvote = upvote;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public EnvironmentQuestions getEnvironmentQuestions() {
		return environmentQuestions;
	}

	public void setEnvironmentQuestions(EnvironmentQuestions environmentQuestions) {
		this.environmentQuestions = environmentQuestions;
	}

	public RealUser getRealUser() {
		return realUser;
	}

	public void setRealUser(RealUser realUser) {
		this.realUser = realUser;
	}

	public boolean isBookmark() {
		return bookmark;
	}

	public void setBookmark(boolean bookmark) {
		this.bookmark = bookmark;
	}

	
}
