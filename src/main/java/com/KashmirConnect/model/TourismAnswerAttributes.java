package com.KashmirConnect.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity(name = "Tourism_Answer_Attributes_Table")
public class TourismAnswerAttributes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@Column(name="UpvotesCount")
	private int upvotesCount;
	
	@Column(name="DownvotesCount")
	private int downvotesCount;
	
	@Column(name="FlagCount")
	private int flagCount;


	@OneToOne(optional = false)
	@JoinColumn(name = "Answer_ID" )
	private TourismAnswers tourismAnswers;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUpvotesCount() {
		return upvotesCount;
	}


	public void setUpvotesCount(int upvotesCount) {
		this.upvotesCount = upvotesCount;
	}


	public int getDownvotesCount() {
		return downvotesCount;
	}


	public void setDownvotesCount(int downvotesCount) {
		this.downvotesCount = downvotesCount;
	}


	public int getFlagCount() {
		return flagCount;
	}


	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}


	public TourismAnswers getTourismAnswers() {
		return tourismAnswers;
	}


	public void setTourismAnswers(TourismAnswers tourismAnswers) {
		this.tourismAnswers = tourismAnswers;
	}


	
}