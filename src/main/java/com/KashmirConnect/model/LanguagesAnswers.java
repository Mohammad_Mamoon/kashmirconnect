package com.KashmirConnect.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "Languages_Answers_Table")
public class LanguagesAnswers {


	@Id
	@Column(name="Answer_ID")
	private long answerId;
	
	@Column(name="Answer" , nullable = false)
	private String answer;
	
	@Column(name="TimeAnswered", nullable=false)
	private Date timeAnswered;
	
	@Column(name="UpvotesCount")
	private int upvotesCount;
	
	@Column(name="DownvotesCount")
	private int downvotesCount;
	
	@Column(name="FlagCount")
	private int flagCount;

	@ManyToOne(optional = false)
	@JoinColumn(name = "Question_ID" )
	private LanguagesQuestions languagesQuestions;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "Email_ID" )
	private RealUser realUser;

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Date getTimeAnswered() {
		return timeAnswered;
	}

	public void setTimeAnswered(Date timeAnswered) {
		this.timeAnswered = timeAnswered;
	}

	public LanguagesQuestions getLanguagesQuestions() {
		return languagesQuestions;
	}

	public void setLanguagesQuestions(LanguagesQuestions languagesQuestions) {
		this.languagesQuestions = languagesQuestions;
	}

	

	

	public int getUpvotesCount() {
		return upvotesCount;
	}

	public void setUpvotesCount(int upvotesCount) {
		this.upvotesCount = upvotesCount;
	}

	public int getDownvotesCount() {
		return downvotesCount;
	}

	public void setDownvotesCount(int downvotesCount) {
		this.downvotesCount = downvotesCount;
	}

	public int getFlagCount() {
		return flagCount;
	}

	public void setFlagCount(int flagCount) {
		this.flagCount = flagCount;
	}

	public long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(long answerId) {
		this.answerId = answerId;
	}

	public RealUser getRealUser() {
		return realUser;
	}

	public void setRealUser(RealUser realUser) {
		this.realUser = realUser;
	}

}
