package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.KashmirConnect.model.ArtsAnswerVotes;
import com.KashmirConnect.model.ArtsAnswers;
import com.KashmirConnect.model.ArtsQuestionVotes;
import com.KashmirConnect.model.ArtsQuestions;
import com.KashmirConnect.model.BookmarkQuestions;
import com.KashmirConnect.model.BusinessAnswerVotes;
import com.KashmirConnect.model.BusinessAnswers;
import com.KashmirConnect.model.BusinessQuestionVotes;
import com.KashmirConnect.model.BusinessQuestions;
import com.KashmirConnect.model.EnvironmentAnswerVotes;
import com.KashmirConnect.model.EnvironmentAnswers;
import com.KashmirConnect.model.EnvironmentQuestionVotes;
import com.KashmirConnect.model.EnvironmentQuestions;
import com.KashmirConnect.model.LanguagesAnswerVotes;
import com.KashmirConnect.model.LanguagesAnswers;
import com.KashmirConnect.model.LanguagesQuestionVotes;
import com.KashmirConnect.model.LanguagesQuestions;
import com.KashmirConnect.model.MedicineAnswerVotes;
import com.KashmirConnect.model.MedicineAnswers;
import com.KashmirConnect.model.MedicineQuestionVotes;
import com.KashmirConnect.model.MedicineQuestions;
import com.KashmirConnect.model.RealUser;

import com.KashmirConnect.model.TemporaryUser;

import com.KashmirConnect.model.TourismAnswerVotes;
import com.KashmirConnect.model.TourismAnswers;

import com.KashmirConnect.model.TourismQuestionVotes;
import com.KashmirConnect.model.TourismQuestions;
import com.KashmirConnect.security.MyEncoder;
import com.KashmirConnect.service.ArtsAnswerVotesService;
import com.KashmirConnect.service.ArtsAnswersService;
import com.KashmirConnect.service.ArtsQuestionVotesService;
import com.KashmirConnect.service.ArtsQuestionsService;
import com.KashmirConnect.service.BookmarkQuestionsService;
import com.KashmirConnect.service.BusinessAnswerVotesService;
import com.KashmirConnect.service.BusinessAnswersService;
import com.KashmirConnect.service.BusinessQuestionVotesService;
import com.KashmirConnect.service.BusinessQuestionsService;
import com.KashmirConnect.service.EnvironmentAnswerVotesService;
import com.KashmirConnect.service.EnvironmentAnswersService;
import com.KashmirConnect.service.EnvironmentQuestionVotesService;
import com.KashmirConnect.service.EnvironmentQuestionsService;
import com.KashmirConnect.service.GenerateRandomNumber;
import com.KashmirConnect.service.LanguagesAnswerVotesService;
import com.KashmirConnect.service.LanguagesAnswersService;
import com.KashmirConnect.service.LanguagesQuestionVotesService;
import com.KashmirConnect.service.LanguagesQuestionsService;
import com.KashmirConnect.service.MedicineAnswerVotesService;
import com.KashmirConnect.service.MedicineAnswersService;
import com.KashmirConnect.service.MedicineQuestionVotesService;
import com.KashmirConnect.service.MedicineQuestionsService;
import com.KashmirConnect.service.OldTimeCompare;
import com.KashmirConnect.service.RealUserService;
import com.KashmirConnect.service.SendGroupEmailService;
import com.KashmirConnect.service.TemporaryUserService;
import com.KashmirConnect.service.TimeCompare;
import com.KashmirConnect.service.TourismAnswerAttributesService;
import com.KashmirConnect.service.TourismAnswerVotesService;
import com.KashmirConnect.service.TourismAnswersService;
import com.KashmirConnect.service.TourismQuestionAttributesService;
import com.KashmirConnect.service.TourismQuestionVotesService;
import com.KashmirConnect.service.TourismQuestionsService;


@Controller
public class MainController {
	
	
	@Autowired
	TemporaryUserService temporaryUserService;
	
	@Autowired
	SendGroupEmailService sendGroupEmailService;
	
	@Autowired
	RealUserService realUserService;
	
	@Autowired
	TourismQuestionsService tourismQuestionsService;
	
	@Autowired
	MedicineQuestionsService medicineQuestionsService;
	
	@Autowired
	LanguagesQuestionsService languagesQuestionsService;
	
	@Autowired
	BusinessQuestionsService businessQuestionsService;
	
	@Autowired
	EnvironmentQuestionsService environmentQuestionsService;
	
	@Autowired
	ArtsQuestionsService artsQuestionsService;
	
	@Autowired
	TourismAnswersService tourismAnswersService;
	
	@Autowired
	MedicineAnswersService medicineAnswersService;
	
	@Autowired
	LanguagesAnswersService languagesAnswersService;
	
	@Autowired
	BusinessAnswersService businessAnswersService;
	
	@Autowired
	EnvironmentAnswersService environmentAnswersService;
	
	@Autowired
	ArtsAnswersService artsAnswersService;
	
	@Autowired
	TourismQuestionVotesService tourismQuestionVotesService;
	
	@Autowired
	MedicineQuestionVotesService medicineQuestionVotesService;
	
	@Autowired
	BusinessQuestionVotesService businessQuestionVotesService;
	
	@Autowired
	LanguagesQuestionVotesService languagesQuestionVotesService;
	
	@Autowired
	EnvironmentQuestionVotesService environmentQuestionVotesService;
	
	@Autowired
	ArtsQuestionVotesService artsQuestionVotesService;
	
	@Autowired
	TourismQuestionAttributesService tourismQuestionAttributesService;
	
	@Autowired
	TourismAnswerVotesService tourismAnswerVotesService;
	
	@Autowired
	MedicineAnswerVotesService medicineAnswerVotesService;
	
	@Autowired
	LanguagesAnswerVotesService languagesAnswerVotesService;
	
	@Autowired
	EnvironmentAnswerVotesService environmentAnswerVotesService;
	
	@Autowired
	BusinessAnswerVotesService businessAnswerVotesService;
	
	@Autowired
	ArtsAnswerVotesService artsAnswerVotesService;
	
	@Autowired
	TourismAnswerAttributesService tourismAnswerAttributesService;
	
	@Autowired
	BookmarkQuestionsService bookmarkQuestionsService;

	
    private JavaMailSender mailSender;
    
    
    @RequestMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }
    
    
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ModelAndView signUp(@Valid @ModelAttribute("TemporaryUser") TemporaryUser user,
            BindingResult result) {

        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("index");
            model.addObject("error", "An error occured, try again!");
            return model;

        }
        
        RealUser realUser;
        TemporaryUser temporaryUser;
        
        String checkEmail = user.getEmail();
        temporaryUser = temporaryUserService.findByEmail(checkEmail);
        realUser = realUserService.findByEmail(checkEmail);
		

		if (realUser != null || temporaryUser != null) {
			ModelAndView model = new ModelAndView("index");
			model.addObject("message",
					"We already have an entry with this email address , try with other email address");
			return model;
		}

		else {
			String rawPassword = user.getPassword();
			user.setPassword(MyEncoder.encode(rawPassword));
			String uniqueCode = UUID.randomUUID().toString();
			user.setUniqueCode(uniqueCode);
			temporaryUserService.save(user);
			
			String status = null;

			try {
				MimeMessage message = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
				helper.setFrom("Administrator");
				helper.setTo(user.getEmail());
				helper.setSubject("Registration confirmation");
				String text = "Thank you for your registration.Your login details are:<br/>" + "Username: <b>"
						+ user.getFirstName() + " " + user.getLastName() + "</b><br/>" + "Password: <b>" + rawPassword
						+ "</b><br/>" + "Click on the link to confirm: <b>"
						+ "http://localhost/KashmirConnect/confirmation?confirmCode=" + uniqueCode;

				helper.setText(text, true);
				mailSender.send(message);
				status = "We have sent email to " + "<b>" + user.getEmail() +"</b>" + " to confirm the validity of our email address." +
    					" After receiving the email follow the link provided to complete you registered.";
			} catch (MessagingException e) {
				status = "There was an error in sending email. Please check your email address: " + user.getEmail();
			}
			
			

			ModelAndView model = new ModelAndView("emailconfirmation");
			model.addObject("status", status);
			model.addObject("email", user.getEmail());
			model.addObject("firstName", user.getFirstName());
			model.addObject("lastName", user.getLastName());
			model.addObject("password", rawPassword);
			model.addObject("uniqueCode", user.getUniqueCode());
			return model;
		}

	}
    
    

    @RequestMapping("/resend")
    public ModelAndView resend(@Valid @ModelAttribute("TemporaryUser") TemporaryUser user,
            BindingResult result) {

        if (result.hasErrors()) {
            ModelAndView model = new ModelAndView("index");
            model.addObject("error", "An error occured, try again!");
            return model;
    	
        }
    	
    	
    	String status = null;

		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
			helper.setFrom("Administrator");
			helper.setTo(user.getEmail());
			helper.setSubject("Registration confirmation");
			String text = "Thank you for your registration.Your login details are:<br/>" + "Username:<b>"
					+ user.getFirstName() + " " + user.getLastName() + "</b><br/>" + "Password:<b>" + user.getPassword()
					+ "</b><br/>" + "Click on the link to confirm:<b>"
					+ "http://localhost/KashmirConnect/confirmation?confirmCode=" + user.getUniqueCode();

			helper.setText(text, true);
			mailSender.send(message);
			status = "We have sent email to " + user.getEmail() + " to confirm the validity of our email address." +
					" After receiving the email follow the link provided to complete you registeration.";
		} catch (MessagingException e) {
			status = "There was an error in sending email to you. Please check your email address: " + user.getEmail();
		}

		ModelAndView model = new ModelAndView("emailconfirmation");
		model.addObject("status", status);
		model.addObject("email", user.getEmail());
		model.addObject("firstName", user.getFirstName());
		model.addObject("lastName", user.getLastName());
		model.addObject("password", user.getPassword());
		model.addObject("uniqueCode", user.getUniqueCode());
		return model;
    }

    @Autowired
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }
    
    
    
    @RequestMapping(value = "/confirmation/{confirmCode}" , method = RequestMethod.GET)
    public ModelAndView confirm(@PathVariable("confirmCode") String confirmCode) {
        
    	RealUser realUser = new RealUser();
    	
    	TemporaryUser temporaryUser;
    	temporaryUser = temporaryUserService.findByUniqueCode(confirmCode);
    	
    	if(temporaryUser != null) {
    		
    		realUser.setEmail(temporaryUser.getEmail());
    		realUser.setFirstName(temporaryUser.getFirstName());
    		realUser.setLastName(temporaryUser.getLastName());
    		realUser.setPassword(temporaryUser.getPassword());
    		realUser.setRole("user");
    		realUser.setEnabled(true);
    		realUser.setDescription("");
    		realUser.setMySite("");
    		realUser.setProfile("");
    		realUser.setStatus("");
    		realUser.setUserName(realUser.getFirstName()+ " " + realUser.getLastName() );
    		realUserService.save(realUser);
    		
    		
    		temporaryUserService.delete(temporaryUser.getEmail());
    		
    			
    	}
    		
    	
        ModelAndView modelAndView = new ModelAndView("builduserprofile");
        modelAndView.addObject("email",realUser.getEmail());
        modelAndView.addObject("name",realUser.getFirstName() + " " + realUser.getLastName());
        return modelAndView;
    }
    
    @RequestMapping(value ="/generateuserprofile" , method = RequestMethod.POST)
    public ModelAndView generateUserProfile(@RequestParam Map<String,String> reqPar) {
    	
    	if(reqPar.isEmpty()) {
    		ModelAndView model = new ModelAndView("error");
    		return  model;
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("home");
    	
    	
        RealUser realUser;
    	realUser = realUserService.findByEmail(reqPar.get("email"));
    	realUser.setRole(reqPar.get("role"));
    	realUser.setUserName(reqPar.get("userName"));
    	realUser.setProfile(reqPar.get("profile"));
    	realUser.setStatus(reqPar.get("status"));
    	realUser.setDescription(reqPar.get("description"));
    	realUser.setMySite(reqPar.get("mySite"));
    	
    	realUserService.update(realUser);
    	
    	List<Object> newList = new ArrayList<>();
    	
        if(realUser.getRole().equals("tourism")) {
        	List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
        	Collections.sort(list, new TimeCompare());
        	
        	List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(realUser.getEmail());
			
			for(TourismQuestions question : list1) {
				
				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("tourismNotifyList", newList);
		
        }
        
        else if(realUser.getRole().equals("medicine")) {
        	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
      		
      		List<MedicineQuestions> list1 = medicineQuestionsService.getQuestions(realUser.getEmail());
			
			for(MedicineQuestions question : list1) {
				
				List<MedicineAnswers> list2 = medicineAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("medicineNotifyList", newList);
        }
        
        else if(realUser.getRole().equals("arts")) {
        	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
      		
      		List<ArtsQuestions> list1 = artsQuestionsService.getQuestionList(realUser.getEmail());
			
			for(ArtsQuestions question : list1) {
				
				List<ArtsAnswers> list2 = artsAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("artsNotifyList", newList);
        }
        
        else if(realUser.getRole().equals("languages")) {
        	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
      		
      		List<LanguagesQuestions> list1 = languagesQuestionsService.getQuestionList(realUser.getEmail());
			
			for(LanguagesQuestions question : list1) {
				
				List<LanguagesAnswers> list2 = languagesAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("languagesNotifyList", newList);		
        }
        
        else if(realUser.getRole().equals("business")) {
        	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
      		
      		
			
			List<BusinessQuestions> list1 = businessQuestionsService.getQuestionList(realUser.getEmail());
			
			for(BusinessQuestions question : list1) {
				
				List<BusinessAnswers> list2 = businessAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("businessNotifyList", newList);
        }
        
        else if(realUser.getRole().equals("environment")) {
        	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
      		
      		List<EnvironmentQuestions> list1 = environmentQuestionsService.getQuestionList(realUser.getEmail());
			
			for(EnvironmentQuestions question : list1) {
				
				List<EnvironmentAnswers> list2 = environmentAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("environmentNotifyList", newList);
        }
    	
     	modelAndView.addObject("email",realUser.getEmail());
    	modelAndView.addObject("userName",realUser.getUserName());
    	modelAndView.addObject("role",realUser.getRole());
        return modelAndView;
    }
      
    
    @RequestMapping( value ="/home" , method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("home");
        
        RealUser realUser;
        
        
    	realUser = realUserService.findByEmail(getCurrentUser().getUsername());
    
    	
    	modelAndView.addObject("email",realUser.getEmail());
    	modelAndView.addObject("userName",realUser.getUserName());
    	modelAndView.addObject("role",realUser.getRole());
    	
    	List<Object> newList = new ArrayList<>();
    	
    	if(realUser.getRole().equals("tourism")) {
    	        List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
    	        Collections.sort(list,new TimeCompare());
    			
    			
    			List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(realUser.getEmail());
    			
    			for(TourismQuestions question : list1) {
    				
    				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
    				newList.addAll(list2);
    				
    			}
    			
    			modelAndView.addObject("list",list);
    			modelAndView.addObject("tourismNotifyList", newList);
        }
    	        
    	else if(realUser.getRole().equals("medicine")) {
	        	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
	      		
	      		
    			List<MedicineQuestions> list1 = medicineQuestionsService.getQuestions(realUser.getEmail());
    			
    			for(MedicineQuestions question : list1) {
    				
    				List<MedicineAnswers> list2 = medicineAnswersService.pullAllAnswers(question.getQuestionId());
    				newList.addAll(list2);
    				
    			}
    			
    			modelAndView.addObject("list",list);
    			modelAndView.addObject("medicineNotifyList", newList);
        }
    	
    	else if(realUser.getRole().equals("arts")) {
        	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
      		
      		
			
			List<ArtsQuestions> list1 = artsQuestionsService.getQuestionList(realUser.getEmail());
			
			for(ArtsQuestions question : list1) {
				
				List<ArtsAnswers> list2 = artsAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("artsNotifyList", newList);
    	}
    	
    	else if(realUser.getRole().equals("business")) {
        	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
      		
      		
			
			List<BusinessQuestions> list1 = businessQuestionsService.getQuestionList(realUser.getEmail());
			
			for(BusinessQuestions question : list1) {
				
				List<BusinessAnswers> list2 = businessAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("businessNotifyList", newList);
    	}
    	
    	else if(realUser.getRole().equals("languages")) {
        	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
      		
      		
			
			List<LanguagesQuestions> list1 = languagesQuestionsService.getQuestionList(realUser.getEmail());
			
			for(LanguagesQuestions question : list1) {
				
				List<LanguagesAnswers> list2 = languagesAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("languagesNotifyList", newList);
    	}
    	
    	else if(realUser.getRole().equals("environment")) {
        	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
      		
      		
			
			List<EnvironmentQuestions> list1 = environmentQuestionsService.getQuestionList(realUser.getEmail());
			
			for(EnvironmentQuestions question : list1) {
				
				List<EnvironmentAnswers> list2 = environmentAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("list",list);
			modelAndView.addObject("environmentNotifyList", newList);
    	}
    	
        return modelAndView;
    }
     
    
    @RequestMapping("/login")
    public ModelAndView login(
    		@RequestParam(value = "error", required = false) String error,
    		@RequestParam(value = "logout", required = false) String logout) {

    	    ModelAndView modelAndView = new ModelAndView("login");
    		if (error != null) {
    			modelAndView.addObject("error", " Invalid username or password!");
    		}

    		if (logout != null) {
    			modelAndView.addObject("msg", "You've been logged out successfully.");
    		} 
       
            return modelAndView;
    }
    
    
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
        
    }
    
      
    @RequestMapping("/about")
    public ModelAndView aboutUs() {
        ModelAndView modelAndView = new ModelAndView("about");
        return modelAndView;
    }
  
  
    @RequestMapping("/contact")
    public ModelAndView contactUs() {
        ModelAndView modelAndView = new ModelAndView("contact");
        return modelAndView;
    }
     
    
    
	@RequestMapping(value = "/submitquestion", method = RequestMethod.POST)
	public ModelAndView questionAsked(@RequestParam Map<String, String> reqPar) {

		if (reqPar.isEmpty()) {
			ModelAndView model = new ModelAndView("home");
			model.addObject("error", "Something went wrong , please try again");
			return model;
		}
	
		ModelAndView model = new ModelAndView("home");
		

		RealUser realUser;
		realUser = realUserService.findByEmail(reqPar.get("email"));
		
		if(realUser.getRole().equals("tourism")) {
			
			TourismQuestions tourismQuestions = new TourismQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			tourismQuestions.setQuestionId(questionId);
			tourismQuestions.setTimeAsked(new Date());
			tourismQuestions.setQuestion(reqPar.get("question"));
			tourismQuestions.setUpvotesCount(0);
			tourismQuestions.setDownvotesCount(0);
			tourismQuestions.setFlagCount(0);
			tourismQuestions.setRealUser(realUser);
			
			
			tourismQuestionsService.save(tourismQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());
			
		
		}
		
		else if(realUser.getRole().equals("medicine")) {
			
			MedicineQuestions medicineQuestions = new MedicineQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			medicineQuestions.setQuestionId(questionId);
			medicineQuestions.setTimeAsked(new Date());
			medicineQuestions.setQuestion(reqPar.get("question"));
			medicineQuestions.setUpvotesCount(0);
			medicineQuestions.setDownvotesCount(0);
			medicineQuestions.setFlagCount(0);
			medicineQuestions.setRealUser(realUser);
			
			
			medicineQuestionsService.save(medicineQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());			
		}
		
		else if(realUser.getRole().equals("arts")) {
			
			ArtsQuestions artsQuestions = new ArtsQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			artsQuestions.setQuestionId(questionId);
			artsQuestions.setTimeAsked(new Date());
			artsQuestions.setQuestion(reqPar.get("question"));
			artsQuestions.setUpvotesCount(0);
			artsQuestions.setDownvotesCount(0);
			artsQuestions.setFlagCount(0);
			artsQuestions.setRealUser(realUser);
			
			
			artsQuestionsService.save(artsQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());		
		
		}
		
		else if(realUser.getRole().equals("business")) {
			
			BusinessQuestions businessQuestions = new BusinessQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			businessQuestions.setQuestionId(questionId);
			businessQuestions.setTimeAsked(new Date());
			businessQuestions.setQuestion(reqPar.get("question"));
			businessQuestions.setUpvotesCount(0);
			businessQuestions.setDownvotesCount(0);
			businessQuestions.setFlagCount(0);
			businessQuestions.setRealUser(realUser);
			
			
			businessQuestionsService.save(businessQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());		
		
		}
		
		else if(realUser.getRole().equals("languages")) {
			
			LanguagesQuestions languagesQuestions = new LanguagesQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			languagesQuestions.setQuestionId(questionId);
			languagesQuestions.setTimeAsked(new Date());
			languagesQuestions.setQuestion(reqPar.get("question"));
			languagesQuestions.setUpvotesCount(0);
			languagesQuestions.setDownvotesCount(0);
			languagesQuestions.setFlagCount(0);
			languagesQuestions.setRealUser(realUser);
			
			
			languagesQuestionsService.save(languagesQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());		
		}
		
		else if(realUser.getRole().equals("environment")) {
			
			EnvironmentQuestions environmentQuestions = new EnvironmentQuestions();
			long questionId = GenerateRandomNumber.generateNumber();
			environmentQuestions.setQuestionId(questionId);
			environmentQuestions.setTimeAsked(new Date());
			environmentQuestions.setQuestion(reqPar.get("question"));
			environmentQuestions.setUpvotesCount(0);
			environmentQuestions.setDownvotesCount(0);
			environmentQuestions.setFlagCount(0);
			environmentQuestions.setRealUser(realUser);
			
			
			environmentQuestionsService.save(environmentQuestions);
			
			sendGroupEmailService.sendEmailToGroup(realUser.getEmail());		
		}
		
		
		

		return model;

	}

    
    @RequestMapping(value ="/answer" , method = RequestMethod.GET)
    public ModelAndView answer(@RequestParam Map<String,String> reqPar) {
    	
    	RealUser realUser;
		realUser = realUserService.findByEmail(getCurrentUser().getUsername());

    	 if (reqPar.isEmpty()) {
         	
         	
         	ModelAndView model = new ModelAndView("home");
             model.addObject("error", "An error occured, try again!");
             
             if(realUser.getRole().equals("tourism")) {
             	List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
             	model.addObject("list",list);
     		
             }
             
             else if(realUser.getRole().equals("medicine")) {
             	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
             	model.addObject("list",list);
         
             }
             
             else if(realUser.getRole().equals("arts")) {
             	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
         		model.addObject("list",list);
             
             }
             
             else if(realUser.getRole().equals("business")) {
             	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
         		model.addObject("list",list);
             
             }
             
             else if(realUser.getRole().equals("languages")) {
             	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
         		model.addObject("list",list);
             
             }
             
             else if(realUser.getRole().equals("environment")) {
             	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
         		model.addObject("list",list);
             
             }
             
   
     		
     		model.addObject("userName", realUser.getUserName());
     		model.addObject("email", realUser.getEmail());
     		model.addObject("role", realUser.getRole());
            return model;
             
         }
         
    	
    	 ModelAndView modelAndView = new ModelAndView("answer");
		
		
		
		MedicineQuestions medicineQuestions = null ;
		TourismQuestions tourismQuestions = null;
		ArtsQuestions artsQuestions = null;
		BusinessQuestions businessQuestions = null;
		LanguagesQuestions languagesQuestions = null;
		EnvironmentQuestions environmentQuestions = null;
		
		if(reqPar.get("realUser.role").equals("medicine")) {
			medicineQuestions = medicineQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
		else if(reqPar.get("realUser.role").equals("tourism")) {
			tourismQuestions = tourismQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
		else if(reqPar.get("realUser.role").equals("arts")) {
			artsQuestions = artsQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
		else if(reqPar.get("realUser.role").equals("languages")) {
			languagesQuestions = languagesQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
		else if(reqPar.get("realUser.role").equals("business")) {
			businessQuestions = businessQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
		else if(reqPar.get("realUser.role").equals("environment")) {
			environmentQuestions = environmentQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
		}
		
    	
       
        
        List<Object> newList = new ArrayList<>();
        
        
        if(medicineQuestions != null) {
	    	modelAndView.addObject("question", medicineQuestions.getQuestion());
	        modelAndView.addObject("questionId",medicineQuestions.getQuestionId());
	        modelAndView.addObject("upvote", medicineQuestions.getUpvotesCount());
	        modelAndView.addObject("downvote", medicineQuestions.getDownvotesCount());
	        modelAndView.addObject("userName",medicineQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",medicineQuestions.getTimeAsked());
	        modelAndView.addObject("loggedInUserEmail",realUser.getEmail());
	        modelAndView.addObject("role",realUser.getRole());
	        
	               
        }
        

        if(tourismQuestions != null) {
        
	        modelAndView.addObject("question", tourismQuestions.getQuestion());
	        modelAndView.addObject("questionId",tourismQuestions.getQuestionId());
	        modelAndView.addObject("upvote", tourismQuestions.getUpvotesCount());
	        modelAndView.addObject("downvote", tourismQuestions.getDownvotesCount());
	        modelAndView.addObject("userName",tourismQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",tourismQuestions.getTimeAsked());
	        modelAndView.addObject("role",realUser.getRole());
	        
	        List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(realUser.getEmail());
			
			for(TourismQuestions question : list1) {
				
				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			
			modelAndView.addObject("tourismNotifyList", newList);
	           
        }
        
        if(artsQuestions != null) {
            
	        modelAndView.addObject("question", artsQuestions.getQuestion());
	        modelAndView.addObject("questionId",artsQuestions.getQuestionId());
	        modelAndView.addObject("emailId",artsQuestions.getRealUser().getEmail());
	        modelAndView.addObject("userName",artsQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",artsQuestions.getTimeAsked());
	        modelAndView.addObject("loggedInUserEmail",realUser.getEmail());
	        modelAndView.addObject("loggedInUserName",realUser.getUserName());
	           
        }
        
        if(languagesQuestions != null) {
            
	        modelAndView.addObject("question", languagesQuestions.getQuestion());
	        modelAndView.addObject("questionId",languagesQuestions.getQuestionId());
	        modelAndView.addObject("emailId",languagesQuestions.getRealUser().getEmail());
	        modelAndView.addObject("userName",languagesQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",languagesQuestions.getTimeAsked());
	        modelAndView.addObject("loggedInUserEmail",realUser.getEmail());
	        modelAndView.addObject("loggedInUserName",realUser.getUserName());
	           
        }
        
        if(environmentQuestions != null) {
            
	        modelAndView.addObject("question", environmentQuestions.getQuestion());
	        modelAndView.addObject("questionId",environmentQuestions.getQuestionId());
	        modelAndView.addObject("emailId",environmentQuestions.getRealUser().getEmail());
	        modelAndView.addObject("userName",environmentQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",environmentQuestions.getTimeAsked());
	        modelAndView.addObject("loggedInUserEmail",realUser.getEmail());
	        modelAndView.addObject("loggedInUserName",realUser.getUserName());
	           
        }
        
        if(businessQuestions != null) {
            
	        modelAndView.addObject("question", businessQuestions.getQuestion());
	        modelAndView.addObject("questionId",businessQuestions.getQuestionId());
	        modelAndView.addObject("emailId",businessQuestions.getRealUser().getEmail());
	        modelAndView.addObject("userName",businessQuestions.getRealUser().getUserName());
	        modelAndView.addObject("timeAsked",businessQuestions.getTimeAsked());
	        modelAndView.addObject("loggedInUserEmail",realUser.getEmail());
	        modelAndView.addObject("loggedInUserName",realUser.getUserName());
	           
        }
        
        long questionId = (long)Integer.parseInt(reqPar.get("questionId"));

        if(reqPar.get("realUser.role").equals("tourism")) {
        	List<TourismAnswers> list = tourismAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("TourismList",list);
        }
        
        
        else if(reqPar.get("realUser.role").equals("medicine")) {
        	List<MedicineAnswers> list = medicineAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("MedicineList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("arts")) {
        	List<ArtsAnswers> list = artsAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("ArtsList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("business")) {
        	List<BusinessAnswers> list = businessAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("BusinessList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("languages")) {
        	List<LanguagesAnswers> list = languagesAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("LanguagesList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("environment")) {
        	List<EnvironmentAnswers> list = environmentAnswersService.pullAllAnswers(questionId);
        	modelAndView.addObject("EnvironmentList",list);
        }
       
      
        modelAndView.addObject("principalEmail", realUser.getEmail());
        
      
       
        return modelAndView;
    }
    
    
 
    
    public User getCurrentUser() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	if(auth != null) {
    		Object principal = auth.getPrincipal();
    		if(principal instanceof User) {
    			return ((User) principal);
    		}
    	}
    	
    	return null;
    	
    }
    
      
    
    @RequestMapping(value="/submitanswer" , method = RequestMethod.POST)
    public ModelAndView submitAnswer(@RequestParam Map<String,String> reqPar) {
    	
    	
    	
    	RealUser realUser;
		realUser = realUserService.findByEmail(getCurrentUser().getUsername());
	
		if(realUser.getRole().equals("tourism")) {
			TourismQuestions tourismQuestions;
			TourismAnswers tourismAnswers = new TourismAnswers();
			
			tourismQuestions = tourismQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			tourismAnswers.setTourismQuestions(tourismQuestions);
			
			tourismAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			tourismAnswers.setAnswer( reqPar.get("answer"));
			tourismAnswers.setTimeAnswered((new Date()));
			tourismAnswers.setFlagCount(0);
			tourismAnswers.setDownvotesCount(0);
			tourismAnswers.setUpvotesCount(0);
			tourismAnswers.setRealUser(realUser);
		
			
	    	tourismAnswersService.save(tourismAnswers);
			
		}
		
		else if(realUser.getRole().equals("medicine")) {
			MedicineQuestions medicineQuestions;
			MedicineAnswers medicineAnswers = new MedicineAnswers();
			
			medicineQuestions = medicineQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			medicineAnswers.setMedicineQuestions(medicineQuestions);
			medicineAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			medicineAnswers.setAnswer( reqPar.get("answer"));
			medicineAnswers.setTimeAnswered((new Date()));
			medicineAnswers.setFlagCount(0);
			medicineAnswers.setDownvotesCount(0);
			medicineAnswers.setUpvotesCount(0);
			medicineAnswers.setRealUser(realUser);
		
			
			medicineAnswersService.save(medicineAnswers);
			
		}
		
		else if(realUser.getRole().equals("arts")) {
			ArtsQuestions artsQuestions;
			ArtsAnswers artsAnswers = new ArtsAnswers();
			
			artsQuestions = artsQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			artsAnswers.setArtsQuestions(artsQuestions);
			artsAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			artsAnswers.setAnswer( reqPar.get("answer"));
			artsAnswers.setTimeAnswered((new Date()));
			artsAnswers.setFlagCount(0);
			artsAnswers.setDownvotesCount(0);
			artsAnswers.setUpvotesCount(0);
			artsAnswers.setRealUser(realUser);
		
			
			artsAnswersService.save(artsAnswers);
			
		}
		
		else if(realUser.getRole().equals("business")) {
			BusinessQuestions businessQuestions;
			BusinessAnswers businessAnswers = new BusinessAnswers();
			businessQuestions = businessQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			businessAnswers.setBusinessQuestions(businessQuestions);
			businessAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			businessAnswers.setAnswer( reqPar.get("answer"));
			businessAnswers.setTimeAnswered((new Date()));
			businessAnswers.setFlagCount(0);
			businessAnswers.setDownvotesCount(0);
			businessAnswers.setUpvotesCount(0);
			businessAnswers.setRealUser(realUser);
		
			
			businessAnswersService.save(businessAnswers);
			
		}
		
		else if(realUser.getRole().equals("languages")) {
			LanguagesQuestions languagesQuestions;
			LanguagesAnswers languagesAnswers = new LanguagesAnswers();
			languagesQuestions = languagesQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			languagesAnswers.setLanguagesQuestions(languagesQuestions);
			languagesAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			languagesAnswers.setAnswer( reqPar.get("answer"));
			languagesAnswers.setTimeAnswered((new Date()));
			languagesAnswers.setFlagCount(0);
			languagesAnswers.setDownvotesCount(0);
			languagesAnswers.setUpvotesCount(0);
			languagesAnswers.setRealUser(realUser);
		
			
			languagesAnswersService.save(languagesAnswers);
			
		}
		
		else if(realUser.getRole().equals("environment")) {
			EnvironmentQuestions environmentQuestions;
			EnvironmentAnswers environmentAnswers = new EnvironmentAnswers();
			environmentQuestions = environmentQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
			environmentAnswers.setEnvironmentQuestions(environmentQuestions);
			environmentAnswers.setAnswerId(GenerateRandomNumber.generateNumber());
			environmentAnswers.setAnswer( reqPar.get("answer"));
			environmentAnswers.setTimeAnswered((new Date()));
			environmentAnswers.setFlagCount(0);
			environmentAnswers.setDownvotesCount(0);
			environmentAnswers.setUpvotesCount(0);
			environmentAnswers.setRealUser(realUser);
		
			
			environmentAnswersService.save(environmentAnswers);
			
		}
		
		
    	
    	ModelAndView modelAndView = new ModelAndView("answer");
      
    	
        
        
        return modelAndView;
    }

  
    
    @RequestMapping(value = "/editcomment" , method = RequestMethod.POST)
    public ModelAndView editcomment(@RequestParam Map<String,String> reqPar) {
    	
    	
    	
    	
    	
    	ModelAndView model = new ModelAndView("answer");
        
    	
    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    
    	
    	
    	
    
		if(realUser.getRole().equals("tourism")) {
    	
	    	TourismAnswers tourismAnswers = tourismAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
	    	
	    	tourismAnswers.setAnswer(reqPar.get("editComment"));
	    	tourismAnswers.setTimeAnswered(new Date());
	    	
	    	tourismAnswersService.update(tourismAnswers);
	    	
	    	TourismQuestions tourismQuestions;
	    	long questionId = tourismAnswers.getTourismQuestions().getQuestionId();
	    	tourismQuestions = tourismQuestionsService.findQuestion(questionId);
	    	
	    	
	    	List<TourismAnswers> list = tourismAnswersService.pullAllAnswers(questionId);
	    	model.addObject("TourismList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", tourismQuestions.getQuestion());
	        model.addObject("upvote", tourismQuestions.getUpvotesCount());
	        model.addObject("downvote", tourismQuestions.getDownvotesCount());
	        model.addObject("timeAsked", tourismQuestions.getTimeAsked());
	        model.addObject("userName", tourismQuestions.getRealUser().getUserName()); 
	        model.addObject("role", tourismQuestions.getRealUser().getRole());
    	
		}
		
		else if(realUser.getRole().equals("medicine")) {
			MedicineAnswers medicineAnswers = medicineAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
			medicineAnswers.setAnswer(reqPar.get("editComment"));
			medicineAnswers.setTimeAnswered(new Date());
	    	
			medicineAnswersService.update(medicineAnswers);
	    	
			MedicineQuestions medicineQuestions;
	    	long questionId = medicineAnswers.getMedicineQuestions().getQuestionId();
	    	medicineQuestions = medicineQuestionsService.findQuestion(questionId);
	    	
	    	model.addObject("question", medicineQuestions);
	    	
	    	List<MedicineAnswers> list = medicineAnswersService.pullAllAnswers(questionId);
	    	model.addObject("MedicineList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", medicineQuestions.getQuestion());
	        model.addObject("userName", medicineQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", medicineQuestions.getRealUser().getUserName()); 
    	   
		}
		
		else if(realUser.getRole().equals("arts")) {
			ArtsAnswers artsAnswers = artsAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
	    	//String editComment = reqPar.get("editComment");
			artsAnswers.setAnswer(reqPar.get("editComment"));
			artsAnswers.setTimeAnswered(new Date());
	    	
			artsAnswersService.update(artsAnswers);
	    	
	    	ArtsQuestions artsQuestions;
	    	long questionId = artsAnswers.getArtsQuestions().getQuestionId();
	    	artsQuestions = artsQuestionsService.findQuestion(questionId);
	    	
	    	model.addObject("question", artsQuestions);
	    	
	    	List<ArtsAnswers> list = artsAnswersService.pullAllAnswers(questionId);
	    	model.addObject("ArtsList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", artsQuestions.getQuestion());
	        model.addObject("userName", artsQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", artsQuestions.getRealUser().getUserName()); 
    	   
		}
		
		else if(realUser.getRole().equals("business")) {
			BusinessAnswers businessAnswers = businessAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
	    	//String editComment = reqPar.get("editComment");
			businessAnswers.setAnswer(reqPar.get("editComment"));
			businessAnswers.setTimeAnswered(new Date());
	    	
			businessAnswersService.update(businessAnswers);
	    	
			BusinessQuestions businessQuestions;
	    	long questionId = businessAnswers.getBusinessQuestions().getQuestionId();
	    	businessQuestions = businessQuestionsService.findQuestion(questionId);
	    	
	    	model.addObject("question", businessQuestions);
	    	
	    	List<BusinessAnswers> list = businessAnswersService.pullAllAnswers(questionId);
	    	model.addObject("BusinessList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", businessQuestions.getQuestion());
	        model.addObject("userName", businessQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", businessQuestions.getRealUser().getUserName()); 
    	    
		}
		
		else if(realUser.getRole().equals("languages")) {
			LanguagesAnswers languagesAnswers = languagesAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
	    	//String editComment = reqPar.get("editComment");
			languagesAnswers.setAnswer(reqPar.get("editComment"));
			languagesAnswers.setTimeAnswered(new Date());
	    	
			languagesAnswersService.update(languagesAnswers);
	    	
			LanguagesQuestions languagesQuestions;
	    	long questionId = languagesAnswers.getLanguagesQuestions().getQuestionId();
	    	languagesQuestions = languagesQuestionsService.findQuestion(questionId);
	    	
	    	model.addObject("question", languagesQuestions);
	    	
	    	List<LanguagesAnswers> list = languagesAnswersService.pullAllAnswers(questionId);
	    	model.addObject("LanguagesList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", languagesQuestions.getQuestion());
	        model.addObject("userName", languagesQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", languagesQuestions.getRealUser().getUserName()); 
    	   
		}
		
		else if(realUser.getRole().equals("environment")) {
			EnvironmentAnswers environmentAnswers = environmentAnswersService.findByEmailAndAnswerId(email,(long)Integer.parseInt(reqPar.get("answerId")));
	    	//String editComment = reqPar.get("editComment");
			environmentAnswers.setAnswer(reqPar.get("editComment"));
			environmentAnswers.setTimeAnswered(new Date());
	    	
			environmentAnswersService.update(environmentAnswers);
	    	
			EnvironmentQuestions environmentQuestions;
	    	long questionId = environmentAnswers.getEnvironmentQuestions().getQuestionId();
	    	environmentQuestions = environmentQuestionsService.findQuestion(questionId);
	    	
	    	model.addObject("question", environmentQuestions);
	    	
	    	List<EnvironmentAnswers> list = environmentAnswersService.pullAllAnswers(questionId);
	    	model.addObject("EnvironmentList",list);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",questionId);
	        model.addObject("question", environmentQuestions.getQuestion());
	        model.addObject("userName", environmentQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", environmentQuestions.getRealUser().getUserName()); 
    	     
		}
		
		return model;
    	
    }
    
  
    
    

    
  
    
    
    @RequestMapping(value = "/deletecomment" , method = RequestMethod.POST)
    public ModelAndView deletecomment(@RequestParam Map<String,String> reqPar) {
    	
    	ModelAndView model = new ModelAndView("answer");
    	
    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	
    	
    	
    	
		
		if(realUser.getRole().equals("tourism")) {
			tourismAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			TourismQuestions tourismQuestions;
			
	    	tourismQuestions = tourismQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",tourismQuestions.getQuestion());
	    	
	    	List<TourismAnswers> list = tourismAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("TourismList",list);
	    	
	    	
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", tourismQuestions.getQuestion());
	        model.addObject("upvote", tourismQuestions.getUpvotesCount());
	        model.addObject("downvote", tourismQuestions.getDownvotesCount());
	        model.addObject("role", tourismQuestions.getRealUser().getRole());
	        model.addObject("timeAsked", tourismQuestions.getTimeAsked());
	        
	        model.addObject("userName", tourismQuestions.getRealUser().getUserName());
	      
		}
		
		else if(realUser.getRole().equals("medicine")) {
			medicineAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			MedicineQuestions medicineQuestions;
			
			medicineQuestions = medicineQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",medicineQuestions.getQuestion());
	    	
	    	List<MedicineAnswers> list = medicineAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("MedicineList",list);
	    	
	    	model.addObject("medicineQuestion", medicineQuestions);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", medicineQuestions.getQuestion());
	        model.addObject("userName", medicineQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", medicineQuestions.getRealUser().getUserName());
		}
		
		else if(realUser.getRole().equals("arts")) {
			artsAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			ArtsQuestions artsQuestions;
			
			artsQuestions = artsQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",artsQuestions.getQuestion());
	    	
	    	List<ArtsAnswers> list = artsAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("ArtsList",list);
	    	
	    	model.addObject("artsQuestion", artsQuestions);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", artsQuestions.getQuestion());
	        model.addObject("userName", artsQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", artsQuestions.getRealUser().getUserName());
		}
		
		else if(realUser.getRole().equals("business")) {
			businessAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			BusinessQuestions businessQuestions;
			
			businessQuestions = businessQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",businessQuestions.getQuestion());
	    	
	    	List<BusinessAnswers> list = businessAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("BusinessList",list);
	    	
	    	model.addObject("businessQuestion", businessQuestions);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", businessQuestions.getQuestion());
	        model.addObject("userName", businessQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", businessQuestions.getRealUser().getUserName());
		}
		
		else if(realUser.getRole().equals("languages")) {
			languagesAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			LanguagesQuestions languagesQuestions;
			
			languagesQuestions = languagesQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",languagesQuestions.getQuestion());
	    	
	    	List<LanguagesAnswers> list = languagesAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("LanguagesList",list);
	    	
	    	model.addObject("languagesQuestion", languagesQuestions);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", languagesQuestions.getQuestion());
	        model.addObject("userName", languagesQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", languagesQuestions.getRealUser().getUserName());
		}
		
		else if(realUser.getRole().equals("environment")) {
			environmentAnswersService.delete(email,(long)Integer.parseInt(reqPar.get("answerId")));
			
			EnvironmentQuestions environmentQuestions;
			
			environmentQuestions = environmentQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
	    	
	    	model.addObject("question",environmentQuestions.getQuestion());
	    	
	    	List<EnvironmentAnswers> list = environmentAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
	    	model.addObject("EnvironmentList",list);
	    	
	    	model.addObject("environmentQuestion", environmentQuestions);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
	        model.addObject("question", environmentQuestions.getQuestion());
	        model.addObject("userName", environmentQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("emailId",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	        model.addObject("userName", environmentQuestions.getRealUser().getUserName());
		}
    	
    	
    	
        
        return model;
    }
    
    @RequestMapping(value = "/unanswered" , method = RequestMethod.GET)
    public ModelAndView answered() {
    	
    	ModelAndView model = new ModelAndView("unanswered");
    	
    	String principalEmail = getCurrentUser().getUsername();
    	RealUser realUser;
		realUser = realUserService.findByEmail(principalEmail);
		String role = realUser.getRole();
		String userName = realUser.getUserName();
		
		
		
		
		int i = 0;
		
	
		if(role.equals("tourism")) {	
			List<TourismQuestions>	otherList =  tourismQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(TourismQuestions tourismQuestions : otherList) {
				
				
				boolean find = tourismAnswersService.findIfAnswered(tourismQuestions.getQuestionId(),principalEmail);
				
				if( find == false) {
					questionIds[i++] = tourismQuestions.getQuestionId();
				}
			}
			
			int count = 0;
			
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
			}
			
			List<TourismQuestions> list = new ArrayList<>();
		
			for(int j=0; j<count; j++) {
			 
				list.add(tourismQuestionsService.findQuestion(questionIds[j]));
			}
			
			model.addObject("list",list);
		}
		
		else if(role.equals("medicine")) {
			List<MedicineQuestions>	otherList =  medicineQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(MedicineQuestions medicineQuestions : otherList) {
				long questionId = medicineQuestions.getQuestionId();
				System.out.println(questionId);
				boolean find = medicineAnswersService.findIfAnswered(questionId,principalEmail);
				System.out.println(find);
				if( find == false) {
					questionIds[i++] = questionId;
				}
			}
			int count = 0 ;
			
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
			}
			
			List<MedicineQuestions> list = new ArrayList<>();
			
			for(int j=0; j<count; j++) {
				 
				list.add(medicineQuestionsService.findQuestion(questionIds[j]));
			}
				
			model.addObject("list",list);
		}
		
		else if(role.equals("arts")) {
			List<ArtsQuestions>	otherList =  artsQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(ArtsQuestions artsQuestions : otherList) {
				long questionId = artsQuestions.getQuestionId();
				System.out.println(questionId);
				boolean find = artsAnswersService.findIfAnswered(questionId,principalEmail);
				System.out.println(find);
				
				if( find == false) {
					questionIds[i++] = questionId;
				}
			}
			int count = 0 ;
			
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
				
			}
			List<ArtsQuestions> list = new ArrayList<>();
			
			for(int j=0; j<count; j++) {
				 
				list.add(artsQuestionsService.findQuestion(questionIds[j]));
			}
				
			model.addObject("list",list);
		}
		
		
		else if(role.equals("business")) {
			List<BusinessQuestions>	otherList =  businessQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(BusinessQuestions artsQuestions : otherList) {
				long questionId = artsQuestions.getQuestionId();
				System.out.println(questionId);
				boolean find = businessAnswersService.findIfAnswered(questionId,principalEmail);
				System.out.println(find);
				
				if( find == false) {
					questionIds[i++] = questionId;
				}
			}
			int count = 0 ;
			
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
				
			}
			List<BusinessQuestions> list = new ArrayList<>();
			
			for(int j=0; j<count; j++) {
				 
				list.add(businessQuestionsService.findQuestion(questionIds[j]));
			}
				
			model.addObject("list",list);
		}
		
		
		else if(role.equals("languages")) {
	
			List<LanguagesQuestions>	otherList =  languagesQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(LanguagesQuestions languagesQuestions : otherList) {
				long questionId = languagesQuestions.getQuestionId();
		
				System.out.println(questionId);
		
				boolean find = languagesAnswersService.findIfAnswered(questionId,principalEmail);
		
				System.out.println(find);
		
				if( find == false) {
					questionIds[i++] = questionId;
				}
			}
			int count = 0 ;
	
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
			}
			List<LanguagesQuestions> list = new ArrayList<>();
	
			for(int j=0; j<count; j++) {
		 
				list.add(languagesQuestionsService.findQuestion(questionIds[j]));
			}
		
			model.addObject("list",list);
		}


		else if(role.equals("environment")) {
			List<EnvironmentQuestions>	otherList =  environmentQuestionsService.getQuestionList(principalEmail);
			long[] questionIds = new long[otherList.size()];
			for(EnvironmentQuestions environmentQuestions : otherList) {
				long questionId = environmentQuestions.getQuestionId();
		
				System.out.println(questionId);
		
				boolean find = environmentAnswersService.findIfAnswered(questionId,principalEmail);
		
				System.out.println(find);
		
				if( find == false) {
					questionIds[i++] = questionId;
				}
			}
			int count = 0 ;
	
			for(int length = 0; length < questionIds.length; length++) {
				if(questionIds[length] != 0) {
					count++;
				}	
			}
			List<EnvironmentQuestions> list = new ArrayList<>();
	
			for(int j=0; j<count; j++) {
		 
				list.add(environmentQuestionsService.findQuestion(questionIds[j]));
			}
		
			model.addObject("list",list);
		}

		
		model.addObject("email", realUser.getEmail());
		model.addObject("role", realUser.getRole());
        return model;
    	
    }
    
    


    @RequestMapping(value ="/medicine" , method=RequestMethod.GET)
    public ModelAndView medicine() {
        ModelAndView model = new ModelAndView("medicine");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	
    	String role = realUser.getRole();
    	model.addObject("email",email);
    	model.addObject("role",role);
    	
        
        List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
		model.addObject("list",list);
        
        return model;
     
    }
    
    
    @RequestMapping(value ="/tourism" , method=RequestMethod.GET)
    public ModelAndView tourism() {
        ModelAndView model = new ModelAndView("tourism");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	
    	String role = realUser.getRole();
    	model.addObject("email",email);
 
    	model.addObject("role",role);
    	
        
        List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
		model.addObject("list",list);
        
        return model;
     
    }
    
    @RequestMapping(value ="/arts" , method=RequestMethod.GET)
    public ModelAndView arts() {
        ModelAndView model = new ModelAndView("arts");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	model.addObject("email",email);
    	model.addObject("userName",userName);
    	model.addObject("role",role);
    	
        
        List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
		model.addObject("list",list);
        
        return model;
     
    }
    
    @RequestMapping(value ="/languages" , method=RequestMethod.GET)
    public ModelAndView languages() {
        ModelAndView model = new ModelAndView("languages");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	model.addObject("email",email);
    	model.addObject("userName",userName);
    	model.addObject("role",role);
    	
        
        List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
		model.addObject("list",list);
        
        return model;
     
    }
    
    @RequestMapping(value ="/business" , method=RequestMethod.GET)
    public ModelAndView business() {
        ModelAndView model = new ModelAndView("business");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	model.addObject("email",email);
    	model.addObject("userName",userName);
    	model.addObject("role",role);
    	
        
        List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
		model.addObject("list",list);
        
        return model;
     
    }
    
    @RequestMapping(value ="/environment" , method=RequestMethod.GET)
    public ModelAndView environment() {
        ModelAndView model = new ModelAndView("environment");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	model.addObject("email",email);
    	model.addObject("userName",userName);
    	model.addObject("role",role);
    	
        
        List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
		model.addObject("list",list);
	
        
        return model;
     
    }
    
    
    @RequestMapping(value ="/viewanswers" , method = RequestMethod.POST)
    public ModelAndView viewAnswers(@RequestParam Map<String,String> reqPar) {

    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	
        
       
        ModelAndView modelAndView = new ModelAndView("viewanswers");
       
        modelAndView.addObject("principalEmail", realUser.getEmail());
    	
        
        
       if(reqPar.get("role").equals("tourism")) {
    	   
    	   	TourismQuestions tourismQuestions;
    	    tourismQuestions = tourismQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
    	    modelAndView.addObject("question", tourismQuestions.getQuestion());
    	    modelAndView.addObject("userName", tourismQuestions.getRealUser().getUserName());
    	    modelAndView.addObject("timeAsked", tourismQuestions.getTimeAsked());
    	    modelAndView.addObject("role", tourismQuestions.getRealUser().getRole());
            modelAndView.addObject("email", tourismQuestions.getRealUser().getEmail());
       
        	List<TourismAnswers> list = tourismAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("TourismList",list);
        	
        }
        
        
        else if(reqPar.get("role").equals("medicine")) {
        	
        	MedicineQuestions medicineQuestions;
    	    medicineQuestions = medicineQuestionsService.findQuestion((long)Integer.parseInt(reqPar.get("questionId")));
    	    modelAndView.addObject("record", medicineQuestions);
    	   // modelAndView.addObject("userName", medicineQuestions.getRealUser().getUserName());
    	   // modelAndView.addObject("timeAsked", medicineQuestions.getTimeAsked());
    	    modelAndView.addObject("role", medicineQuestions.getRealUser().getRole());
            modelAndView.addObject("email", medicineQuestions.getRealUser().getEmail());
       
        	
        	List<MedicineAnswers> list = medicineAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("MedicineList",list);
        	
        	
        }
        
        
        else if(reqPar.get("artsCommunity").equals("arts")) {
        	List<ArtsAnswers> list = artsAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("ArtsList",list);
        }
        
        else if(reqPar.get("businessCommunity").equals("business")) {
        	List<BusinessAnswers> list = businessAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("BusinessList",list);
        }
        
        else if(reqPar.get("languagesCommunity").equals("languages")) {
        	List<LanguagesAnswers> list = languagesAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("LanguagesList",list);
        }
        
        else if(reqPar.get("environmentCommunity").equals("environment")) {
        	List<EnvironmentAnswers> list = environmentAnswersService.pullAllAnswers((long)Integer.parseInt(reqPar.get("questionId")));
        	modelAndView.addObject("EnvironmentList",list);
        }
      
        
        return modelAndView;
    }
		
    @RequestMapping("/userprofile")
    public ModelAndView userProfile(@RequestParam Map<String,String> reqPar) {
        ModelAndView modelAndView = new ModelAndView("userprofile");
        RealUser realUser =  realUserService.findByEmail(reqPar.get("email"));
        modelAndView.addObject("realUser", realUser);
        return modelAndView;
    }
    
    
    @RequestMapping(value = "/searchstring" , method = RequestMethod.POST)
    public ModelAndView searchString(@RequestParam(value = "searchString") String searchString ) {
        ModelAndView modelAndView = new ModelAndView("result");
        
        
         searchString = searchString.toLowerCase();
         
         String[] words = searchString.split(" ");
         int length = words.length;
         
         
        
         List<TourismQuestions> tourismQuestionsList = tourismQuestionsService.pullAllQuestions();
         List<TourismQuestions> newTourismQuestionsList = new ArrayList<>();
         
         for(TourismQuestions tourismQuestionsObject  : tourismQuestionsList ) {
        	 
        	 int count = 0;
        	 String otherString = tourismQuestionsObject.getQuestion().toLowerCase();
        	 
        	 for(String word : words) {
        		 if(otherString.contains(word)) {
        			 count++;
        		 }
        	 }
        	 
        	 
        	 if(count >= length/1.5) {
        		 newTourismQuestionsList.add(tourismQuestionsObject);
        	 }
        	 
         }
         
         
        modelAndView.addObject("TourismList", newTourismQuestionsList);
        
        

        List<MedicineQuestions> medicineQuestionsList = medicineQuestionsService.pullAllQuestions();
        List<MedicineQuestions> newMedicineQuestionsList = new ArrayList<>();
        
        for(MedicineQuestions medicineQuestionsObject  : medicineQuestionsList ) {
       	 
       	 int count = 0;
       	 String otherString = medicineQuestionsObject.getQuestion().toLowerCase();
       	 
       	 for(String word : words) {
       		 if(otherString.contains(word)) {
       			 count++;
       		 }
       	 }
       	 
       	 
       	 if(count >= length/1.5) {
       		 newMedicineQuestionsList.add(medicineQuestionsObject);
       	 }
       	 
        }
        
        
       modelAndView.addObject("MedicineList", newMedicineQuestionsList);
       
       
       List<ArtsQuestions> artsQuestionsList = artsQuestionsService.pullAllQuestions();
       List<ArtsQuestions> newArtsQuestionsList = new ArrayList<>();
       
       for(ArtsQuestions artsQuestionsObject  : artsQuestionsList ) {
      	 
      	 int count = 0;
      	 String otherString = artsQuestionsObject.getQuestion().toLowerCase();
      	 
      	 for(String word : words) {
      		 if(otherString.contains(word)) {
      			 count++;
      		 }
      	 }
      	 
      	 
      	 if(count >= length/1.5) {
      		 newArtsQuestionsList.add(artsQuestionsObject);
      	 }
      	 
       }
       
       
      modelAndView.addObject("ArtsList", newArtsQuestionsList);
      
      
      List<BusinessQuestions> businessQuestionsList = businessQuestionsService.pullAllQuestions();
      List<BusinessQuestions> newBusinessQuestionsList = new ArrayList<>();
      
      for(BusinessQuestions businessQuestionsObject  : businessQuestionsList ) {
     	 
     	 int count = 0;
     	 String otherString = businessQuestionsObject.getQuestion().toLowerCase();
     	 
     	 for(String word : words) {
     		 if(otherString.contains(word)) {
     			 count++;
     		 }
     	 }
     	 
     	 
     	 if(count >= length/1.5) {
     		 newBusinessQuestionsList.add(businessQuestionsObject);
     	 }
     	 
      }
      
      
     modelAndView.addObject("BusinessList", newBusinessQuestionsList);
     
     
     List<LanguagesQuestions> languagesQuestionsList = languagesQuestionsService.pullAllQuestions();
     List<LanguagesQuestions> newLanguagesQuestionsList = new ArrayList<>();
     
     for(LanguagesQuestions languagesQuestionsObject  : languagesQuestionsList ) {
    	 
    	 int count = 0;
    	 String otherString = languagesQuestionsObject.getQuestion().toLowerCase();
    	 
    	 for(String word : words) {
    		 if(otherString.contains(word)) {
    			 count++;
    		 }
    	 }
    	 
    	 
    	 if(count >= length/1.5) {
    		 newLanguagesQuestionsList.add(languagesQuestionsObject);
    	 }
    	 
     }
     
     
    modelAndView.addObject("LanguagesList", newLanguagesQuestionsList);
    
    
    List<EnvironmentQuestions> environmentQuestionsList = environmentQuestionsService.pullAllQuestions();
    List<EnvironmentQuestions> newEnvironmentQuestionsList = new ArrayList<>();
    
    for(EnvironmentQuestions environmentQuestionsObject  : environmentQuestionsList ) {
   	 
   	 int count = 0;
   	 String otherString = environmentQuestionsObject.getQuestion().toLowerCase();
   	 
   	 for(String word : words) {
   		 if(otherString.contains(word)) {
   			 count++;
   		 }
   	 }
   	 
   	 
   	 if(count >= length/1.5) {
   		 newEnvironmentQuestionsList.add(environmentQuestionsObject);
   	 }
   	 
    }
    
    
   modelAndView.addObject("EnvironmentList", newEnvironmentQuestionsList);
        
        return modelAndView;
    }
    
    
	
    @RequestMapping(value = "/viewdetails" , method = RequestMethod.POST)
    public ModelAndView viewDetails(@RequestParam Map<String,String> reqPar) {
        ModelAndView modelAndView = new ModelAndView("viewdetails");
        
        RealUser realUser;
        String principalEmail = getCurrentUser().getUsername();
        realUser = realUserService.findByEmail(principalEmail);
        String principalRole = realUser.getRole();
        
        
        if(reqPar.get("realUser.role").equals("tourism")) {
        	long questionId = (long)Integer.parseInt(reqPar.get("questionId"));
           	List<TourismAnswers> list = tourismAnswersService.pullAllAnswers(questionId);
           	modelAndView.addObject("TourismList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("medicine")) {
        	long questionId = (long)Integer.parseInt(reqPar.get("questionId"));
           	List<MedicineAnswers> list = medicineAnswersService.pullAllAnswers(questionId);
           	modelAndView.addObject("MedicineList",list);
        }
        
        else if(reqPar.get("realUser.role").equals("business")) {
        	long questionId = (long)Integer.parseInt(reqPar.get("questionId"));
           	List<BusinessAnswers> list = businessAnswersService.pullAllAnswers(questionId);
           	modelAndView.addObject("BusinessList",list);
        } 
       
      	
        
       	
       	
       	modelAndView.addObject("question", reqPar.get("question"));
        modelAndView.addObject("questionId",(long)Integer.parseInt(reqPar.get("questionId")));
        modelAndView.addObject("userName",reqPar.get("realUser.userName"));
        modelAndView.addObject("timeAsked",reqPar.get("timeAsked"));
        modelAndView.addObject("email",reqPar.get("realUser.email"));
        modelAndView.addObject("role",reqPar.get("realUser.role"));
    	modelAndView.addObject("principalRole",principalRole);
        modelAndView.addObject("principalEmail",principalEmail);
        modelAndView.addObject("loggedInUserEmail",principalEmail);
      
   	
        
        return modelAndView;
    }
    
    
    
    @RequestMapping(value = "/notifications")
    public ModelAndView notifications() {
        ModelAndView modelAndView = new ModelAndView("notifications");
        
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	modelAndView.addObject("email",email);
    	modelAndView.addObject("userName",userName);
    	modelAndView.addObject("role",role);
    	
    	
    	
    	if(role.equals("tourism")) {
    	        List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
    			modelAndView.addObject("list",list);
    			
    			List<TourismAnswers> newList = new ArrayList<>();
    			
    			List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(email);
    			
    			for(TourismQuestions question : list1) {
    				
    				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
    				newList.addAll(list2);
    				
    			}
    			
    			modelAndView.addObject("tourismNotifyList", newList);
        }
    	        
    	else if(role.equals("medicine")) {
	        	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
	      		modelAndView.addObject("list",list);
	      		List<MedicineAnswers> newList = new ArrayList<>();
    			
    			List<MedicineQuestions> list1 = medicineQuestionsService.getQuestions(email);
    			
    			for(MedicineQuestions question : list1) {
    				
    				List<MedicineAnswers> list2 = medicineAnswersService.pullAllAnswers(question.getQuestionId());
    				newList.addAll(list2);
    				
    			}
    			
    			modelAndView.addObject("medicineNotifyList", newList);
        }
    	
    	else if(role.equals("arts")) {
        	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		List<ArtsAnswers> newList = new ArrayList<>();
			
			List<ArtsQuestions> list1 = artsQuestionsService.getQuestionList(email);
			
			for(ArtsQuestions question : list1) {
				
				List<ArtsAnswers> list2 = artsAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("artsNotifyList", newList);
    	}
    	
    	else if(role.equals("business")) {
        	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		List<BusinessAnswers> newList = new ArrayList<>();
			
			List<BusinessQuestions> list1 = businessQuestionsService.getQuestionList(email);
			
			for(BusinessQuestions question : list1) {
				
				List<BusinessAnswers> list2 = businessAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("businessNotifyList", newList);
    	}
    	
    	else if(role.equals("languages")) {
        	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		List<LanguagesAnswers> newList = new ArrayList<>();
			
			List<LanguagesQuestions> list1 = languagesQuestionsService.getQuestionList(email);
			
			for(LanguagesQuestions question : list1) {
				
				List<LanguagesAnswers> list2 = languagesAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("languagesNotifyList", newList);
    	}
    	
    	else if(role.equals("environment")) {
        	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		List<EnvironmentAnswers> newList = new ArrayList<>();
			
			List<EnvironmentQuestions> list1 = environmentQuestionsService.getQuestionList(email);
			
			for(EnvironmentQuestions question : list1) {
				
				List<EnvironmentAnswers> list2 = environmentAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
			
			modelAndView.addObject("environmentNotifyList", newList);
    	}
    	
        return modelAndView;
       
    }
    
    
    
    @RequestMapping( value ="/oldquestions" , method = RequestMethod.GET)
    public ModelAndView oldQuestions() {
        ModelAndView modelAndView = new ModelAndView("oldquestions");
        
        RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	String userName = realUser.getUserName();
    	String role = realUser.getRole();
    	modelAndView.addObject("email",email);
    	modelAndView.addObject("userName",userName);
    	modelAndView.addObject("role",role);
    	
    	
    	
    	if(role.equals("tourism")) {
    	        List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
    	        Collections.sort(list,new OldTimeCompare());
    	        
    	        List<TourismAnswers> newList = new ArrayList<>();
    			List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(email);
    			
    			for(TourismQuestions question : list1) {
    				
    				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
    				newList.addAll(list2);
    				
    			}
    			
    			modelAndView.addObject("tourismNotifyList",newList);
    			modelAndView.addObject("list",list);
    			
        }
    	        
    	else if(role.equals("medicine")) {
	        	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
	      		modelAndView.addObject("list",list);
	      		
        }
    	
    	else if(role.equals("arts")) {
        	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		
		
    	}
    	
    	else if(role.equals("business")) {
        	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		
    	}
    	
    	else if(role.equals("languages")) {
        	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      
    	}
    	
    	else if(role.equals("environment")) {
        	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
      		modelAndView.addObject("list",list);
      		
    	}
    	
        return modelAndView;
    }
    
    
    @RequestMapping(value ="/questionupvote" , method = RequestMethod.POST)
    public ModelAndView questionUpvote(@RequestParam("questionId") long questionId) {
    	
    	 
    	
    	RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
     	
     	if(role.equals("tourism")) {
     		
     		
     		TourismQuestionVotes tourismQuestionVotes;
     		tourismQuestionVotes = tourismQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(tourismQuestionVotes == null) {
     			
     			
     			TourismQuestions tourismQuestions; 
         		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
         		int newValue  = tourismQuestions.getUpvotesCount() + 1;
         		tourismQuestions.setUpvotesCount(newValue);
         		tourismQuestionsService.update(tourismQuestions);
         		
         		tourismQuestionVotes = new TourismQuestionVotes();
         		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
         		tourismQuestionVotes.setRealUser(realUser);
         		tourismQuestionVotes.setUpvote(true);
         		tourismQuestionVotesService.save(tourismQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismQuestions tourismQuestions; 
             		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
             		
             		if (tourismQuestions.getUpvotesCount() == 1) {
             			tourismQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			tourismQuestions.setUpvotesCount(1);
             			if(tourismQuestions.getDownvotesCount() == 1) {
             				tourismQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		tourismQuestionsService.update(tourismQuestions);
             		
             		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
             		tourismQuestionVotes.setRealUser(realUser);
             		if(tourismQuestionVotes.isUpvote() == true) {
             			tourismQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			tourismQuestionVotes.setUpvote(true);
             		}
             		
             		tourismQuestionVotesService.update(tourismQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineQuestionVotes medicineQuestionVotes;
     		medicineQuestionVotes = medicineQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(medicineQuestionVotes == null) {
     			
     			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
         		int newValue  = medicineQuestions.getUpvotesCount() + 1;
         		medicineQuestions.setUpvotesCount(newValue);
         		medicineQuestionsService.update(medicineQuestions);
         		
         		medicineQuestionVotes = new MedicineQuestionVotes();
         		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
         		medicineQuestionVotes.setRealUser(realUser);
         		medicineQuestionVotes.setUpvote(true);
         		medicineQuestionVotesService.save(medicineQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
             		
             		if (medicineQuestions.getUpvotesCount() == 1) {
             			medicineQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			medicineQuestions.setUpvotesCount(1);
             			if(medicineQuestions.getDownvotesCount() == 1) {
             				medicineQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		medicineQuestionsService.update(medicineQuestions);
             		
             		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
             		medicineQuestionVotes.setRealUser(realUser);
             		if(medicineQuestionVotes.isUpvote() == true) {
             			medicineQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			medicineQuestionVotes.setUpvote(true);
             		}
             		
             		medicineQuestionVotesService.update(medicineQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("languages")) {
     		
     		
     		LanguagesQuestionVotes languagesQuestionVotes;
     		languagesQuestionVotes = languagesQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(languagesQuestionVotes == null) {
     			
     			
     			LanguagesQuestions languagesQuestions; 
     			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
         		int newValue  = languagesQuestions.getUpvotesCount() + 1;
         		languagesQuestions.setUpvotesCount(newValue);
         		languagesQuestionsService.update(languagesQuestions);
         		
         		languagesQuestionVotes = new LanguagesQuestionVotes();
         		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
         		languagesQuestionVotes.setRealUser(realUser);
         		languagesQuestionVotes.setUpvote(true);
         		languagesQuestionVotesService.save(languagesQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			LanguagesQuestions languagesQuestions; 
     			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
             		
             		if (languagesQuestions.getUpvotesCount() == 1) {
             			languagesQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			languagesQuestions.setUpvotesCount(1);
             			if(languagesQuestions.getDownvotesCount() == 1) {
             				languagesQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		languagesQuestionsService.update(languagesQuestions);
             		
             		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
             		languagesQuestionVotes.setRealUser(realUser);
             		if(languagesQuestionVotes.isUpvote() == true) {
             			languagesQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			languagesQuestionVotes.setUpvote(true);
             		}
             		
             		languagesQuestionVotesService.update(languagesQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("business")) {
     		
     		
     		BusinessQuestionVotes businessQuestionVotes;
     		businessQuestionVotes = businessQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(businessQuestionVotes == null) {
     			
     			
     			BusinessQuestions businessQuestions; 
     			businessQuestions =  businessQuestionsService.findQuestion(questionId);
         		int newValue  = businessQuestions.getUpvotesCount() + 1;
         		businessQuestions.setUpvotesCount(newValue);
         		businessQuestionsService.update(businessQuestions);
         		
         		businessQuestionVotes = new BusinessQuestionVotes();
         		businessQuestionVotes.setBusinessQuestions(businessQuestions);
         		businessQuestionVotes.setRealUser(realUser);
         		businessQuestionVotes.setUpvote(true);
         		businessQuestionVotesService.save(businessQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			BusinessQuestions businessQuestions; 
     			businessQuestions =  businessQuestionsService.findQuestion(questionId);
             		
             		if (businessQuestions.getUpvotesCount() == 1) {
             			businessQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			businessQuestions.setUpvotesCount(1);
             			if(businessQuestions.getDownvotesCount() == 1) {
             				businessQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		businessQuestionsService.update(businessQuestions);
             		
             		businessQuestionVotes.setBusinessQuestions(businessQuestions);
             		businessQuestionVotes.setRealUser(realUser);
             		if(businessQuestionVotes.isUpvote() == true) {
             			businessQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			businessQuestionVotes.setUpvote(true);
             		}
             		
             		businessQuestionVotesService.update(businessQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("environment")) {
     		
     		
     		EnvironmentQuestionVotes environmentQuestionVotes;
     		environmentQuestionVotes = environmentQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(environmentQuestionVotes == null) {
     			
     			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
         		int newValue  = environmentQuestions.getUpvotesCount() + 1;
         		environmentQuestions.setUpvotesCount(newValue);
         		environmentQuestionsService.update(environmentQuestions);
         		
         		environmentQuestionVotes = new EnvironmentQuestionVotes();
         		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
         		environmentQuestionVotes.setRealUser(realUser);
         		environmentQuestionVotes.setUpvote(true);
         		environmentQuestionVotesService.save(environmentQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
             		
             		if (environmentQuestions.getUpvotesCount() == 1) {
             			environmentQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			environmentQuestions.setUpvotesCount(1);
             			if(environmentQuestions.getDownvotesCount() == 1) {
             				environmentQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		environmentQuestionsService.update(environmentQuestions);
             		
             		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
             		environmentQuestionVotes.setRealUser(realUser);
             		if(environmentQuestionVotes.isUpvote() == true) {
             			environmentQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			environmentQuestionVotes.setUpvote(true);
             		}
             		
             		environmentQuestionVotesService.update(environmentQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("arts")) {
     		
     		
     		ArtsQuestionVotes artsQuestionVotes;
     		artsQuestionVotes = artsQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(artsQuestionVotes == null) {
     			
     			
     			ArtsQuestions artsQuestions; 
     			artsQuestions =  artsQuestionsService.findQuestion(questionId);
         		int newValue  = artsQuestions.getUpvotesCount() + 1;
         		artsQuestions.setUpvotesCount(newValue);
         		artsQuestionsService.update(artsQuestions);
         		
         		artsQuestionVotes = new ArtsQuestionVotes();
         		artsQuestionVotes.setArtsQuestions(artsQuestions);
         		artsQuestionVotes.setRealUser(realUser);
         		artsQuestionVotes.setUpvote(true);
         		artsQuestionVotesService.save(artsQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			ArtsQuestions artsQuestions; 
     			artsQuestions = artsQuestionsService.findQuestion(questionId);
             		
             		if (artsQuestions.getUpvotesCount() == 1) {
             			artsQuestions.setUpvotesCount(0);
             		}
             		
             		else {
             			artsQuestions.setUpvotesCount(1);
             			if(artsQuestions.getDownvotesCount() == 1) {
             				artsQuestions.setDownvotesCount(0);
             			}
             		}
             		
             		artsQuestionsService.update(artsQuestions);
             		
             		artsQuestionVotes.setArtsQuestions(artsQuestions);
             		artsQuestionVotes.setRealUser(realUser);
             		if(artsQuestionVotes.isUpvote() == true) {
             			artsQuestionVotes.setUpvote(false);
             		}
             		
             		else {
             			artsQuestionVotes.setUpvote(true);
             		}
             		
             		artsQuestionVotesService.update(artsQuestionVotes);
         		}
         		
     			
 		}
     	
     	   	
     	ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    
    }  
    
    @RequestMapping(value ="/questiondownvote" , method = RequestMethod.POST)
    public ModelAndView questionDownvote(@RequestParam("questionId") long questionId) {
    	
    	 
    	

    	RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
     	
     	if(role.equals("tourism")) {
     		
     		
     		TourismQuestionVotes tourismQuestionVotes;
     		tourismQuestionVotes = tourismQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(tourismQuestionVotes == null) {
     			
     			
     			TourismQuestions tourismQuestions; 
         		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
         		int newValue  = tourismQuestions.getDownvotesCount() + 1;
         		tourismQuestions.setDownvotesCount(newValue);
         		tourismQuestionsService.update(tourismQuestions);
         		
         		tourismQuestionVotes = new TourismQuestionVotes();
         		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
         		tourismQuestionVotes.setRealUser(realUser);
         		tourismQuestionVotes.setDownvote(true);
         		tourismQuestionVotesService.save(tourismQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismQuestions tourismQuestions; 
             		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
             		
             		if (tourismQuestions.getDownvotesCount() == 1) {
             			tourismQuestions.setDownvotesCount(0);
             		}
             		
             		else {
             			tourismQuestions.setDownvotesCount(1);
             			if(tourismQuestions.getUpvotesCount() == 1) {
             				tourismQuestions.setUpvotesCount(0);
             			}
             		}
             		
             		tourismQuestionsService.update(tourismQuestions);
             		
             		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
             		tourismQuestionVotes.setRealUser(realUser);
             		if(tourismQuestionVotes.isDownvote() == true) {
             			tourismQuestionVotes.setDownvote(false);
             		}
             		
             		else {
             			tourismQuestionVotes.setDownvote(true);
             		}
             		
             		tourismQuestionVotesService.update(tourismQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineQuestionVotes medicineQuestionVotes;
     		medicineQuestionVotes = medicineQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(medicineQuestionVotes == null) {
     			
     			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
         		int newValue  = medicineQuestions.getDownvotesCount() + 1;
         		medicineQuestions.setDownvotesCount(newValue);
         		medicineQuestionsService.update(medicineQuestions);
         		
         		medicineQuestionVotes = new MedicineQuestionVotes();
         		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
         		medicineQuestionVotes.setRealUser(realUser);
         		medicineQuestionVotes.setDownvote(true);
         		medicineQuestionVotesService.save(medicineQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
             		
             		if (medicineQuestions.getDownvotesCount() == 1) {
             			medicineQuestions.setDownvotesCount(0);
             		}
             		
             		else {
             			medicineQuestions.setDownvotesCount(1);
             			if(medicineQuestions.getUpvotesCount() == 1) {
             				medicineQuestions.setUpvotesCount(0);
             			}
             		}
             		
             		medicineQuestionsService.update(medicineQuestions);
             		
             		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
             		medicineQuestionVotes.setRealUser(realUser);
             		if(medicineQuestionVotes.isDownvote() == true) {
             			medicineQuestionVotes.setDownvote(false);
             		}
             		
             		else {
             			medicineQuestionVotes.setDownvote(true);
             		}
             		
             		medicineQuestionVotesService.update(medicineQuestionVotes);
         		}
         		
     			
 		}
     	
     	else if(role.equals("languages")) {
     		
     		
     		LanguagesQuestionVotes languagesQuestionVotes;
     		languagesQuestionVotes = languagesQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(languagesQuestionVotes == null) {
     			
     			
     			LanguagesQuestions languagesQuestions; 
     			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
         		int newValue  = languagesQuestions.getDownvotesCount() + 1;
         		languagesQuestions.setDownvotesCount(newValue);
         		languagesQuestionsService.update(languagesQuestions);
         		
         		languagesQuestionVotes = new LanguagesQuestionVotes();
         		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
         		languagesQuestionVotes.setRealUser(realUser);
         		languagesQuestionVotes.setDownvote(true);
         		languagesQuestionVotesService.save(languagesQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			LanguagesQuestions languagesQuestions; 
     			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
             		
             		if (languagesQuestions.getDownvotesCount() == 1) {
             			languagesQuestions.setDownvotesCount(0);
             		}
             		
             		else {
             			languagesQuestions.setDownvotesCount(1);
             			if(languagesQuestions.getUpvotesCount() == 1) {
             				languagesQuestions.setUpvotesCount(0);
             			}
             		}
             		
             		languagesQuestionsService.update(languagesQuestions);
             		
             		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
             		languagesQuestionVotes.setRealUser(realUser);
             		if(languagesQuestionVotes.isDownvote() == true) {
             			languagesQuestionVotes.setDownvote(false);
             		}
             		
             		else {
             			languagesQuestionVotes.setDownvote(true);
             		}
             		
             		languagesQuestionVotesService.update(languagesQuestionVotes);
         		}
         		
     			
 		}
   
     	
     	else if(role.equals("environment")) {
     		
     		
     		EnvironmentQuestionVotes environmentQuestionVotes;
     		environmentQuestionVotes = environmentQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(environmentQuestionVotes == null) {
     			
     			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
         		int newValue  = environmentQuestions.getDownvotesCount() + 1;
         		environmentQuestions.setDownvotesCount(newValue);
         		environmentQuestionsService.update(environmentQuestions);
         		
         		environmentQuestionVotes = new EnvironmentQuestionVotes();
         		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
         		environmentQuestionVotes.setRealUser(realUser);
         		environmentQuestionVotes.setDownvote(true);
         		environmentQuestionVotesService.save(environmentQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
             		
             		if (environmentQuestions.getDownvotesCount() == 1) {
             			environmentQuestions.setDownvotesCount(0);
             		}
             		
             		else {
             			environmentQuestions.setDownvotesCount(1);
             			if(environmentQuestions.getUpvotesCount() == 1) {
             				environmentQuestions.setUpvotesCount(0);
             			}
             		}
             		
             		environmentQuestionsService.update(environmentQuestions);
             		
             		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
             		environmentQuestionVotes.setRealUser(realUser);
             		if(environmentQuestionVotes.isDownvote() == true) {
             			environmentQuestionVotes.setDownvote(false);
             		}
             		
             		else {
             			environmentQuestionVotes.setDownvote(true);
             		}
             		
             		environmentQuestionVotesService.update(environmentQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("business")) {
     		
     		
     		BusinessQuestionVotes businessQuestionVotes;
     		businessQuestionVotes = businessQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(businessQuestionVotes == null) {
     			
     			
     			BusinessQuestions businessQuestions; 
     			businessQuestions =  businessQuestionsService.findQuestion(questionId);
         		int newValue  = businessQuestions.getDownvotesCount() + 1;
         		businessQuestions.setDownvotesCount(newValue);
         		businessQuestionsService.update(businessQuestions);
         		
         		businessQuestionVotes = new BusinessQuestionVotes();
         		businessQuestionVotes.setBusinessQuestions(businessQuestions);
         		businessQuestionVotes.setRealUser(realUser);
         		businessQuestionVotes.setDownvote(true);
         		businessQuestionVotesService.save(businessQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			BusinessQuestions businessQuestions; 
     			businessQuestions =  businessQuestionsService.findQuestion(questionId);
             		
             		if (businessQuestions.getDownvotesCount() == 1) {
             			businessQuestions.setDownvotesCount(0);
             		}
             		
             		else {
             			businessQuestions.setDownvotesCount(1);
             			if(businessQuestions.getUpvotesCount() == 1) {
             				businessQuestions.setUpvotesCount(0);
             			}
             		}
             		
             		businessQuestionsService.update(businessQuestions);
             		
             		businessQuestionVotes.setBusinessQuestions(businessQuestions);
             		businessQuestionVotes.setRealUser(realUser);
             		if(businessQuestionVotes.isDownvote() == true) {
             			businessQuestionVotes.setDownvote(false);
             		}
             		
             		else {
             			businessQuestionVotes.setDownvote(true);
             		}
             		
             		businessQuestionVotesService.update(businessQuestionVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("arts")) {
		
		
		ArtsQuestionVotes artsQuestionVotes;
		artsQuestionVotes = artsQuestionVotesService.checkEntry(email,questionId);
		
		
		
		if(artsQuestionVotes == null) {
			
			
			ArtsQuestions artsQuestions; 
			artsQuestions =  artsQuestionsService.findQuestion(questionId);
			int newValue  = artsQuestions.getDownvotesCount() + 1;
			artsQuestions.setDownvotesCount(newValue);
			artsQuestionsService.update(artsQuestions);
 		
			artsQuestionVotes = new ArtsQuestionVotes();
			artsQuestionVotes.setArtsQuestions(artsQuestions);
			artsQuestionVotes.setRealUser(realUser);
			artsQuestionVotes.setDownvote(true);
			artsQuestionVotesService.save(artsQuestionVotes);
 		
		}
		
		
		else {
 			
			ArtsQuestions artsQuestions; 
			artsQuestions =  artsQuestionsService.findQuestion(questionId);
     		
     		if (artsQuestions.getDownvotesCount() == 1) {
     			artsQuestions.setDownvotesCount(0);
     		}
     		
     		else {
     			artsQuestions.setDownvotesCount(1);
     			if(artsQuestions.getUpvotesCount() == 1) {
     				artsQuestions.setUpvotesCount(0);
     			}
     		}
     		
     		artsQuestionsService.update(artsQuestions);
     		
     		artsQuestionVotes.setArtsQuestions(artsQuestions);
     		artsQuestionVotes.setRealUser(realUser);
     		if(artsQuestionVotes.isDownvote() == true) {
     			artsQuestionVotes.setDownvote(false);
     		}
     		
     		else {
     			artsQuestionVotes.setDownvote(true);
     		}
     		
     		artsQuestionVotesService.update(artsQuestionVotes);
 		}
 		
			
	}
     	
     	ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    
   
    }
    
    

    @RequestMapping(value ="/questionflag" , method = RequestMethod.POST)
    public ModelAndView questionFlag(@RequestParam("questionId") long questionId) {
    	
    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
     	
     	if(role.equals("tourism")) {
     		
     		
     		TourismQuestionVotes tourismQuestionVotes;
     		tourismQuestionVotes = tourismQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(tourismQuestionVotes == null) {
     			
     			
     			TourismQuestions tourismQuestions; 
         		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
         		int newValue  = tourismQuestions.getFlagCount() + 1;
         		tourismQuestions.setFlagCount(newValue);
         		tourismQuestionsService.update(tourismQuestions);
         		
         		tourismQuestionVotes = new TourismQuestionVotes();
         		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
         		tourismQuestionVotes.setRealUser(realUser);
         		tourismQuestionVotes.setFlag(true);
         		tourismQuestionVotesService.save(tourismQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismQuestions tourismQuestions; 
             		tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
             		
             		
             		if(tourismQuestions.getFlagCount() == 1) {
             			System.out.println("hi");
             			tourismQuestionVotesService.delete(tourismQuestionVotes.getTourismQuestions().getQuestionId());
             			tourismQuestionsService.delete(tourismQuestions);
             		}
             		
             	
             		else {
             			
             			
             			if (tourismQuestions.getFlagCount() == 1) {
                 			tourismQuestions.setFlagCount(0);
                 		}
                 		
                 		else {
                 			tourismQuestions.setFlagCount(1);
                 		}
                 		
                 		tourismQuestionsService.update(tourismQuestions);
                 		
                 		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
                 		tourismQuestionVotes.setRealUser(realUser);
                 		if(tourismQuestionVotes.isFlag() == true) {
                 			tourismQuestionVotes.setFlag(false);
                 		}
                 		
                 		else {
                 			tourismQuestionVotes.setFlag(true);
                 		}
                 		
                 		tourismQuestionVotesService.update(tourismQuestionVotes);
             		}
             		
            
     		}
     		
        }
     	
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineQuestionVotes medicineQuestionVotes;
     		medicineQuestionVotes = medicineQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(medicineQuestionVotes == null) {
     			
     			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
         		int newValue  = medicineQuestions.getFlagCount() + 1;
         		medicineQuestions.setFlagCount(newValue);
         		medicineQuestionsService.update(medicineQuestions);
         		
         		medicineQuestionVotes = new MedicineQuestionVotes();
         		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
         		medicineQuestionVotes.setRealUser(realUser);
         		medicineQuestionVotes.setFlag(true);
         		medicineQuestionVotesService.save(medicineQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
             		
             		
             		if(medicineQuestions.getFlagCount() == 1) {
             			
             			medicineQuestionVotesService.delete(medicineQuestionVotes.getMedicineQuestions().getQuestionId());
             			medicineQuestionsService.delete(medicineQuestions);
             		}
             		
             	
             		else {
             			
             			
             			if (medicineQuestions.getFlagCount() == 1) {
             				medicineQuestions.setFlagCount(0);
                 		}
                 		
                 		else {
                 			medicineQuestions.setFlagCount(1);
                 		}
                 		
             			medicineQuestionsService.update(medicineQuestions);
                 		
             			medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
             			medicineQuestionVotes.setRealUser(realUser);
                 		if(medicineQuestionVotes.isFlag() == true) {
                 			medicineQuestionVotes.setFlag(false);
                 		}
                 		
                 		else {
                 			medicineQuestionVotes.setFlag(true);
                 		}
                 		
                 		medicineQuestionVotesService.update(medicineQuestionVotes);
             		}
             		
            
     		}
     		
 		}
     	
     	
     	else if(role.equals("environment")) {
     		
     		
     		EnvironmentQuestionVotes environmentQuestionVotes;
     		environmentQuestionVotes = environmentQuestionVotesService.checkEntry(email,questionId);
     		
     		
     		
     		if(environmentQuestionVotes == null) {
     			
     			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
         		int newValue  = environmentQuestions.getFlagCount() + 1;
         		environmentQuestions.setFlagCount(newValue);
         		environmentQuestionsService.update(environmentQuestions);
         		
         		environmentQuestionVotes = new EnvironmentQuestionVotes();
         		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
         		environmentQuestionVotes.setRealUser(realUser);
         		environmentQuestionVotes.setFlag(true);
         		environmentQuestionVotesService.save(environmentQuestionVotes);
         		
     		}
     		
     		
     		else {
         			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
             		
             		
             		if(environmentQuestions.getFlagCount() == 1) {
             			
             			environmentQuestionVotesService.delete(environmentQuestionVotes.getEnvironmentQuestions().getQuestionId());
             			environmentQuestionsService.delete(environmentQuestions);
             		}
             		
             	
             		else {
             			
             			
             			if (environmentQuestions.getFlagCount() == 1) {
             				environmentQuestions.setFlagCount(0);
                 		}
                 		
                 		else {
                 			environmentQuestions.setFlagCount(1);
                 		}
                 		
             			environmentQuestionsService.update(environmentQuestions);
                 		
             			environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
             			environmentQuestionVotes.setRealUser(realUser);
                 		if(environmentQuestionVotes.isFlag() == true) {
                 			environmentQuestionVotes.setFlag(false);
                 		}
                 		
                 		else {
                 			environmentQuestionVotes.setFlag(true);
                 		}
                 		
                 		environmentQuestionVotesService.update(environmentQuestionVotes);
             		}
             		
            
     		}
     		
 		}
     	
     	else if(role.equals("languages")) {
		
		
     		LanguagesQuestionVotes languagesQuestionVotes;
     		languagesQuestionVotes = languagesQuestionVotesService.checkEntry(email,questionId);
		
		
		
		if(languagesQuestionVotes == null) {
			
			
			LanguagesQuestions languagesQuestions; 
			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
 		int newValue  = languagesQuestions.getFlagCount() + 1;
 		languagesQuestions.setFlagCount(newValue);
 		languagesQuestionsService.update(languagesQuestions);
 		
 		languagesQuestionVotes = new LanguagesQuestionVotes();
 		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
 		languagesQuestionVotes.setRealUser(realUser);
 		languagesQuestionVotes.setFlag(true);
 		languagesQuestionVotesService.save(languagesQuestionVotes);
 		
		}
		
		
		else {
 			
			LanguagesQuestions languagesQuestions; 
			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
     		
     		
     		if(languagesQuestions.getFlagCount() == 1) {
     			
     			languagesQuestionVotesService.delete(languagesQuestionVotes.getLanguagesQuestions().getQuestionId());
     			languagesQuestionsService.delete(languagesQuestions);
     		}
     		
     	
     		else {
     			
     			
     			if (languagesQuestions.getFlagCount() == 1) {
     				languagesQuestions.setFlagCount(0);
         		}
         		
         		else {
         			languagesQuestions.setFlagCount(1);
         		}
         		
     			languagesQuestionsService.update(languagesQuestions);
         		
     			languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
     			languagesQuestionVotes.setRealUser(realUser);
         		if(languagesQuestionVotes.isFlag() == true) {
         			languagesQuestionVotes.setFlag(false);
         		}
         		
         		else {
         			languagesQuestionVotes.setFlag(true);
         		}
         		
         		languagesQuestionVotesService.update(languagesQuestionVotes);
     		}
     		
    
			}
			
     	}
     	
     	else if(role.equals("business")) {
		
		
     		BusinessQuestionVotes businessQuestionVotes;
     		businessQuestionVotes = businessQuestionVotesService.checkEntry(email,questionId);
		
		
		
		if(businessQuestionVotes == null) {
			
			
			BusinessQuestions businessQuestions; 
			businessQuestions =  businessQuestionsService.findQuestion(questionId);
 		int newValue  = businessQuestions.getFlagCount() + 1;
 		businessQuestions.setFlagCount(newValue);
 		businessQuestionsService.update(businessQuestions);
 		
 		businessQuestionVotes = new BusinessQuestionVotes();
 		businessQuestionVotes.setBusinessQuestions(businessQuestions);
 		businessQuestionVotes.setRealUser(realUser);
 		businessQuestionVotes.setFlag(true);
 		businessQuestionVotesService.save(businessQuestionVotes);
 		
		}
		
		
		else {
 			
			BusinessQuestions businessQuestions; 
			businessQuestions =  businessQuestionsService.findQuestion(questionId);
     		
     		
     		if(businessQuestions.getFlagCount() == 1) {
     			
     			businessQuestionVotesService.delete(businessQuestionVotes.getBusinessQuestions().getQuestionId());
     			businessQuestionsService.delete(businessQuestions);
     		}
     		
     	
     		else {
     			
     			
     			if (businessQuestions.getFlagCount() == 1) {
     				businessQuestions.setFlagCount(0);
         		}
         		
         		else {
         			businessQuestions.setFlagCount(1);
         		}
         		
     			businessQuestionsService.update(businessQuestions);
         		
     			businessQuestionVotes.setBusinessQuestions(businessQuestions);
     			businessQuestionVotes.setRealUser(realUser);
         		if(businessQuestionVotes.isFlag() == true) {
         			businessQuestionVotes.setFlag(false);
         		}
         		
         		else {
         			businessQuestionVotes.setFlag(true);
         		}
         		
         		businessQuestionVotesService.update(businessQuestionVotes);
     		}
     		
    
			}
		
     	}
     	
     	else if(role.equals("arts")) {
		
		
		ArtsQuestionVotes artsQuestionVotes;
		artsQuestionVotes = artsQuestionVotesService.checkEntry(email,questionId);
		
		
		
		if(artsQuestionVotes == null) {
			
			
			ArtsQuestions artsQuestions; 
			artsQuestions =  artsQuestionsService.findQuestion(questionId);
 		int newValue  = artsQuestions.getFlagCount() + 1;
 		artsQuestions.setFlagCount(newValue);
 		artsQuestionsService.update(artsQuestions);
 		
 		artsQuestionVotes = new ArtsQuestionVotes();
 		artsQuestionVotes.setArtsQuestions(artsQuestions);
 		artsQuestionVotes.setRealUser(realUser);
 		artsQuestionVotes.setFlag(true);
 		artsQuestionVotesService.save(artsQuestionVotes);
 		
		}
		
		
		else {
 			
			ArtsQuestions artsQuestions; 
			artsQuestions =  artsQuestionsService.findQuestion(questionId);
     		
     		
     		if(artsQuestions.getFlagCount() == 1) {
     			
     			artsQuestionVotesService.delete(artsQuestionVotes.getArtsQuestions().getQuestionId());
     			artsQuestionsService.delete(artsQuestions);
     		}
     		
     	
     		else {
     			
     			
     			if (artsQuestions.getFlagCount() == 1) {
     				artsQuestions.setFlagCount(0);
         		}
         		
         		else {
         			artsQuestions.setFlagCount(1);
         		}
         		
     			artsQuestionsService.update(artsQuestions);
         		
     			artsQuestionVotes.setArtsQuestions(artsQuestions);
     			artsQuestionVotes.setRealUser(realUser);
         		if(artsQuestionVotes.isFlag() == true) {
         			artsQuestionVotes.setFlag(false);
         		}
         		
         		else {
         			artsQuestionVotes.setFlag(true);
         		}
         		
         		artsQuestionVotesService.update(artsQuestionVotes);
     		}
     		
    
			}
		
     	}
   
   
     	ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    	

    
    
    }
    
    
    @RequestMapping(value ="/commentupvote" , method = RequestMethod.POST)
    public ModelAndView commentUpvote(@RequestParam("answerId") long answerId) {
    	
    	
    	RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
     	

     	if(role.equals("tourism")) {
     		
     		
     		TourismAnswerVotes tourismAnswerVotes;
     		tourismAnswerVotes = tourismAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(tourismAnswerVotes == null) {
     			
     			
     			TourismAnswers tourismAnswers; 
         		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
         		int newValue  = tourismAnswers.getUpvotesCount() + 1;
         		tourismAnswers.setUpvotesCount(newValue);
         		tourismAnswersService.update(tourismAnswers);
         		
         		tourismAnswerVotes = new TourismAnswerVotes();
         		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
         		tourismAnswerVotes.setRealUser(realUser);
         		tourismAnswerVotes.setUpvote(true);
         		tourismAnswerVotesService.save(tourismAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismAnswers tourismAnswers; 
             		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
             		
             		if (tourismAnswers.getUpvotesCount() == 1) {
             			tourismAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			tourismAnswers.setUpvotesCount(1);
             			if(tourismAnswers.getDownvotesCount() == 1) {
             				tourismAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		tourismAnswersService.update(tourismAnswers);
             		
             		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
             		tourismAnswerVotes.setRealUser(realUser);
             		if(tourismAnswerVotes.isUpvote() == true) {
             			tourismAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			tourismAnswerVotes.setUpvote(true);
             		}
             		
             		tourismAnswerVotesService.update(tourismAnswerVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineAnswerVotes medicineAnswerVotes;
     		medicineAnswerVotes = medicineAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(medicineAnswerVotes == null) {
     			
     			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
         		int newValue  = medicineAnswers.getUpvotesCount() + 1;
         		medicineAnswers.setUpvotesCount(newValue);
         		medicineAnswersService.update(medicineAnswers);
         		
         		medicineAnswerVotes = new MedicineAnswerVotes();
         		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
         		medicineAnswerVotes.setRealUser(realUser);
         		medicineAnswerVotes.setUpvote(true);
         		medicineAnswerVotesService.save(medicineAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
             		
             		if (medicineAnswers.getUpvotesCount() == 1) {
             			medicineAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			medicineAnswers.setUpvotesCount(1);
             			if(medicineAnswers.getDownvotesCount() == 1) {
             				medicineAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		medicineAnswersService.update(medicineAnswers);
             		
             		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
             		medicineAnswerVotes.setRealUser(realUser);
             		if(medicineAnswerVotes.isUpvote() == true) {
             			medicineAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			medicineAnswerVotes.setUpvote(true);
             		}
             		
             		medicineAnswerVotesService.update(medicineAnswerVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("languages")) {
     		
     		
     		LanguagesAnswerVotes languagesAnswerVotes;
     		languagesAnswerVotes = languagesAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(languagesAnswerVotes == null) {
     			
     			
     			LanguagesAnswers languagesAnswers; 
     			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
         		int newValue  = languagesAnswers.getUpvotesCount() + 1;
         		languagesAnswers.setUpvotesCount(newValue);
         		languagesAnswersService.update(languagesAnswers);
         		
         		languagesAnswerVotes = new LanguagesAnswerVotes();
         		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
         		languagesAnswerVotes.setRealUser(realUser);
         		languagesAnswerVotes.setUpvote(true);
         		languagesAnswerVotesService.save(languagesAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			LanguagesAnswers languagesAnswers; 
     			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
             		
             		if (languagesAnswers.getUpvotesCount() == 1) {
             			languagesAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			languagesAnswers.setUpvotesCount(1);
             			if(languagesAnswers.getDownvotesCount() == 1) {
             				languagesAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		languagesAnswersService.update(languagesAnswers);
             		
             		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
             		languagesAnswerVotes.setRealUser(realUser);
             		if(languagesAnswerVotes.isUpvote() == true) {
             			languagesAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			languagesAnswerVotes.setUpvote(true);
             		}
             		
             		languagesAnswerVotesService.update(languagesAnswerVotes);
         		}
         		
     			
 		}
     	
     	else if(role.equals("environment")) {
     		
     		
     		EnvironmentAnswerVotes environmentAnswerVotes;
     		environmentAnswerVotes = environmentAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(environmentAnswerVotes == null) {
     			
     			
     			EnvironmentAnswers environmentAnswers; 
     			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
         		int newValue  = environmentAnswers.getUpvotesCount() + 1;
         		environmentAnswers.setUpvotesCount(newValue);
         		environmentAnswersService.update(environmentAnswers);
         		
         		environmentAnswerVotes = new EnvironmentAnswerVotes();
         		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
         		environmentAnswerVotes.setRealUser(realUser);
         		environmentAnswerVotes.setUpvote(true);
         		environmentAnswerVotesService.save(environmentAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			EnvironmentAnswers environmentAnswers; 
     			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
             		
             		if (environmentAnswers.getUpvotesCount() == 1) {
             			environmentAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			environmentAnswers.setUpvotesCount(1);
             			if(environmentAnswers.getDownvotesCount() == 1) {
             				environmentAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		environmentAnswersService.update(environmentAnswers);
             		
             		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
             		environmentAnswerVotes.setRealUser(realUser);
             		if(environmentAnswerVotes.isUpvote() == true) {
             			environmentAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			environmentAnswerVotes.setUpvote(true);
             		}
             		
             		environmentAnswerVotesService.update(environmentAnswerVotes);
         		}
         		
     			
 		}
     	
     	
     	
     	else if(role.equals("business")) {
     		
     		
     		BusinessAnswerVotes businessAnswerVotes;
     		businessAnswerVotes = businessAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(businessAnswerVotes == null) {
     			
     			
     			BusinessAnswers businessAnswers; 
     			businessAnswers =  businessAnswersService.findAnswer(answerId);
         		int newValue  = businessAnswers.getUpvotesCount() + 1;
         		businessAnswers.setUpvotesCount(newValue);
         		businessAnswersService.update(businessAnswers);
         		
         		businessAnswerVotes = new BusinessAnswerVotes();
         		businessAnswerVotes.setBusinessAnswers(businessAnswers);
         		businessAnswerVotes.setRealUser(realUser);
         		businessAnswerVotes.setUpvote(true);
         		businessAnswerVotesService.save(businessAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			BusinessAnswers businessAnswers; 
     			businessAnswers =  businessAnswersService.findAnswer(answerId);
             		
             		if (businessAnswers.getUpvotesCount() == 1) {
             			businessAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			businessAnswers.setUpvotesCount(1);
             			if(businessAnswers.getDownvotesCount() == 1) {
             				businessAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		businessAnswersService.update(businessAnswers);
             		
             		businessAnswerVotes.setBusinessAnswers(businessAnswers);
             		businessAnswerVotes.setRealUser(realUser);
             		if(businessAnswerVotes.isUpvote() == true) {
             			businessAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			businessAnswerVotes.setUpvote(true);
             		}
             		
             		businessAnswerVotesService.update(businessAnswerVotes);
         		}
         		
     			
 		}
     	
     	else if(role.equals("arts")) {
     		
     		
     		ArtsAnswerVotes artsAnswerVotes;
     		artsAnswerVotes = artsAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(artsAnswerVotes == null) {
     			
     			
     			ArtsAnswers artsAnswers; 
     			artsAnswers =  artsAnswersService.findAnswer(answerId);
         		int newValue  = artsAnswers.getUpvotesCount() + 1;
         		artsAnswers.setUpvotesCount(newValue);
         		artsAnswersService.update(artsAnswers);
         		
         		artsAnswerVotes = new ArtsAnswerVotes();
         		artsAnswerVotes.setArtsAnswers(artsAnswers);
         		artsAnswerVotes.setRealUser(realUser);
         		artsAnswerVotes.setUpvote(true);
         		artsAnswerVotesService.save(artsAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
         			ArtsAnswers artsAnswers; 
         			artsAnswers =  artsAnswersService.findAnswer(answerId);
             		
             		if (artsAnswers.getUpvotesCount() == 1) {
             			artsAnswers.setUpvotesCount(0);
             		}
             		
             		else {
             			artsAnswers.setUpvotesCount(1);
             			if(artsAnswers.getDownvotesCount() == 1) {
             				artsAnswers.setDownvotesCount(0);
             			}
             		}
             		
             		artsAnswersService.update(artsAnswers);
             		
             		artsAnswerVotes.setArtsAnswers(artsAnswers);
             		artsAnswerVotes.setRealUser(realUser);
             		if(artsAnswerVotes.isUpvote() == true) {
             			artsAnswerVotes.setUpvote(false);
             		}
             		
             		else {
             			artsAnswerVotes.setUpvote(true);
             		}
             		
             		artsAnswerVotesService.update(artsAnswerVotes);
         		}
         		
     			
 		}
     	
     	ModelAndView modelAndView = new ModelAndView("answer");
     	return modelAndView;
     	
        
    
    }  
   
    @RequestMapping(value ="/commentdownvote" , method = RequestMethod.POST)
    public ModelAndView commentDownvote(@RequestParam("answerId") long answerId) {
    	
    	 
    	

    	RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
     	
     	
     	if(role.equals("tourism")) {
     		
     		
     		TourismAnswerVotes tourismAnswerVotes;
     		tourismAnswerVotes = tourismAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(tourismAnswerVotes == null) {
     			
     			
     			TourismAnswers tourismAnswers; 
         		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
         		int newValue  = tourismAnswers.getDownvotesCount() + 1;
         		tourismAnswers.setDownvotesCount(newValue);
         		tourismAnswersService.update(tourismAnswers);
         		
         		tourismAnswerVotes = new TourismAnswerVotes();
         		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
         		tourismAnswerVotes.setRealUser(realUser);
         		tourismAnswerVotes.setDownvote(true);
         		tourismAnswerVotesService.save(tourismAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismAnswers tourismAnswers; 
             		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
             		
             		if (tourismAnswers.getDownvotesCount() == 1) {
             			tourismAnswers.setDownvotesCount(0);
             		}
             		
             		else {
             			tourismAnswers.setDownvotesCount(1);
             			if(tourismAnswers.getUpvotesCount() == 1) {
             				tourismAnswers.setUpvotesCount(0);
             			}
             		}
             		
             		tourismAnswersService.update(tourismAnswers);
             		
             		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
             		tourismAnswerVotes.setRealUser(realUser);
             		if(tourismAnswerVotes.isDownvote() == true) {
             			tourismAnswerVotes.setDownvote(false);
             		}
             		
             		else {
             			tourismAnswerVotes.setDownvote(true);
             		}
             		tourismAnswerVotesService.update(tourismAnswerVotes);
         		}
         		
     			
 		}
     	
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineAnswerVotes medicineAnswerVotes;
     		medicineAnswerVotes = medicineAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(medicineAnswerVotes == null) {
     			
     			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
         		int newValue  = medicineAnswers.getDownvotesCount() + 1;
         		medicineAnswers.setDownvotesCount(newValue);
         		medicineAnswersService.update(medicineAnswers);
         		
         		medicineAnswerVotes = new MedicineAnswerVotes();
         		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
         		medicineAnswerVotes.setRealUser(realUser);
         		medicineAnswerVotes.setDownvote(true);
         		medicineAnswerVotesService.save(medicineAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
             		
             		if (medicineAnswers.getDownvotesCount() == 1) {
             			medicineAnswers.setDownvotesCount(0);
             		}
             		
             		else {
             			medicineAnswers.setDownvotesCount(1);
             			if(medicineAnswers.getUpvotesCount() == 1) {
             				medicineAnswers.setUpvotesCount(0);
             			}
             		}
             		
             		medicineAnswersService.update(medicineAnswers);
             		
             		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
             		medicineAnswerVotes.setRealUser(realUser);
             		if(medicineAnswerVotes.isDownvote() == true) {
             			medicineAnswerVotes.setDownvote(false);
             		}
             		
             		else {
             			medicineAnswerVotes.setDownvote(true);
             		}
             		medicineAnswerVotesService.update(medicineAnswerVotes);
         		}
         		
     			
 		}

     	else if(role.equals("environment")) {
		
		
     		EnvironmentAnswerVotes environmentAnswerVotes;
     		environmentAnswerVotes = environmentAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(environmentAnswerVotes == null) {
			
			
			EnvironmentAnswers environmentAnswers; 
			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
	 		int newValue  = environmentAnswers.getDownvotesCount() + 1;
	 		environmentAnswers.setDownvotesCount(newValue);
	 		environmentAnswersService.update(environmentAnswers);
	 		
	 		environmentAnswerVotes = new EnvironmentAnswerVotes();
	 		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
	 		environmentAnswerVotes.setRealUser(realUser);
	 		environmentAnswerVotes.setDownvote(true);
	 		environmentAnswerVotesService.save(environmentAnswerVotes);
 		
		}
		
		
		else {
 			
			EnvironmentAnswers environmentAnswers; 
			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
     		
     		if (environmentAnswers.getDownvotesCount() == 1) {
     			environmentAnswers.setDownvotesCount(0);
     		}
     		
     		else {
     			environmentAnswers.setDownvotesCount(1);
     			if(environmentAnswers.getUpvotesCount() == 1) {
     				environmentAnswers.setUpvotesCount(0);
     			}
     		}
     		
     		environmentAnswersService.update(environmentAnswers);
     		
     		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
     		environmentAnswerVotes.setRealUser(realUser);
     		if(environmentAnswerVotes.isDownvote() == true) {
     			environmentAnswerVotes.setDownvote(false);
     		}
     		
     		else {
     			environmentAnswerVotes.setDownvote(true);
     		}
     		environmentAnswerVotesService.update(environmentAnswerVotes);
 		}
 		
			
     	}

     	else if(role.equals("languages")) {
		
		
     		LanguagesAnswerVotes languagesAnswerVotes;
     		languagesAnswerVotes = languagesAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(languagesAnswerVotes == null) {
			
			
			LanguagesAnswers languagesAnswers; 
			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
 		int newValue  = languagesAnswers.getDownvotesCount() + 1;
 		languagesAnswers.setDownvotesCount(newValue);
 		languagesAnswersService.update(languagesAnswers);
 		
 		languagesAnswerVotes = new LanguagesAnswerVotes();
 		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
 		languagesAnswerVotes.setRealUser(realUser);
 		languagesAnswerVotes.setDownvote(true);
 		languagesAnswerVotesService.save(languagesAnswerVotes);
 		
		}
		
		
		else {
 			
			LanguagesAnswers languagesAnswers; 
			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
     		
     		if (languagesAnswers.getDownvotesCount() == 1) {
     			languagesAnswers.setDownvotesCount(0);
     		}
     		
     		else {
     			languagesAnswers.setDownvotesCount(1);
     			if(languagesAnswers.getUpvotesCount() == 1) {
     				languagesAnswers.setUpvotesCount(0);
     			}
     		}
     		
     		languagesAnswersService.update(languagesAnswers);
     		
     		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
     		languagesAnswerVotes.setRealUser(realUser);
     		if(languagesAnswerVotes.isDownvote() == true) {
     			languagesAnswerVotes.setDownvote(false);
     		}
     		
     		else {
     			languagesAnswerVotes.setDownvote(true);
     		}
     		languagesAnswerVotesService.update(languagesAnswerVotes);
 		}
 		
			
     	}

     	else if(role.equals("business")) {
		
		
     		BusinessAnswerVotes businessAnswerVotes;
     		businessAnswerVotes = businessAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(businessAnswerVotes == null) {
			
			
			BusinessAnswers businessAnswers; 
			businessAnswers =  businessAnswersService.findAnswer(answerId);
 		int newValue  = businessAnswers.getDownvotesCount() + 1;
 		businessAnswers.setDownvotesCount(newValue);
 		businessAnswersService.update(businessAnswers);
 		
 		businessAnswerVotes = new BusinessAnswerVotes();
 		businessAnswerVotes.setBusinessAnswers(businessAnswers);
 		businessAnswerVotes.setRealUser(realUser);
 		businessAnswerVotes.setDownvote(true);
 		businessAnswerVotesService.save(businessAnswerVotes);
 		
		}
		
		
		else {
 			
			BusinessAnswers businessAnswers; 
			businessAnswers =  businessAnswersService.findAnswer(answerId);
     		
     		if (businessAnswers.getDownvotesCount() == 1) {
     			businessAnswers.setDownvotesCount(0);
     		}
     		
     		else {
     			businessAnswers.setDownvotesCount(1);
     			if(businessAnswers.getUpvotesCount() == 1) {
     				businessAnswers.setUpvotesCount(0);
     			}
     		}
     		
     		businessAnswersService.update(businessAnswers);
     		
     		businessAnswerVotes.setBusinessAnswers(businessAnswers);
     		businessAnswerVotes.setRealUser(realUser);
     		if(businessAnswerVotes.isDownvote() == true) {
     			businessAnswerVotes.setDownvote(false);
     		}
     		
     		else {
     			businessAnswerVotes.setDownvote(true);
     		}
     		businessAnswerVotesService.update(businessAnswerVotes);
 		}
 		
			
     	}

     	else if(role.equals("arts")) {
		
		
		ArtsAnswerVotes artsAnswerVotes;
		artsAnswerVotes = artsAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(artsAnswerVotes == null) {
			
			
			ArtsAnswers artsAnswers; 
			artsAnswers =  artsAnswersService.findAnswer(answerId);
 		int newValue  = artsAnswers.getDownvotesCount() + 1;
 		artsAnswers.setDownvotesCount(newValue);
 		artsAnswersService.update(artsAnswers);
 		
 		artsAnswerVotes = new ArtsAnswerVotes();
 		artsAnswerVotes.setArtsAnswers(artsAnswers);
 		artsAnswerVotes.setRealUser(realUser);
 		artsAnswerVotes.setDownvote(true);
 		artsAnswerVotesService.save(artsAnswerVotes);
 		
		}
		
		
		else {
 			
 			ArtsAnswers artsAnswers; 
 			artsAnswers =  artsAnswersService.findAnswer(answerId);
     		
     		if (artsAnswers.getDownvotesCount() == 1) {
     			artsAnswers.setDownvotesCount(0);
     		}
     		
     		else {
     			artsAnswers.setDownvotesCount(1);
     			if(artsAnswers.getUpvotesCount() == 1) {
     				artsAnswers.setUpvotesCount(0);
     			}
     		}
     		
     		artsAnswersService.update(artsAnswers);
     		
     		artsAnswerVotes.setArtsAnswers(artsAnswers);
     		artsAnswerVotes.setRealUser(realUser);
     		if(artsAnswerVotes.isDownvote() == true) {
     			artsAnswerVotes.setDownvote(false);
     		}
     		
     		else {
     			artsAnswerVotes.setDownvote(true);
     		}
     		artsAnswerVotesService.update(artsAnswerVotes);
 		}
 		
			
     	}
   
     	
     	ModelAndView modelAndView = new ModelAndView("answer");
     	return modelAndView;
     	
    }  
    
    
    @RequestMapping(value ="/commentflag" , method = RequestMethod.POST)
    public ModelAndView commentFlag(@RequestParam("answerId") long answerId) {
    	
    	 
    	

    	RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String role = realUser.getRole();
   
     	if(role.equals("tourism")) {
     		
     		
     		TourismAnswerVotes tourismAnswerVotes;
     		tourismAnswerVotes = tourismAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(tourismAnswerVotes == null) {
     			
     			
     			TourismAnswers tourismAnswers; 
         		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
         		int newValue  = tourismAnswers.getFlagCount() + 1;
         		tourismAnswers.setFlagCount(newValue);
         		tourismAnswersService.update(tourismAnswers);
         		
         		tourismAnswerVotes = new TourismAnswerVotes();
         		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
         		tourismAnswerVotes.setRealUser(realUser);
         		tourismAnswerVotes.setFlag(true);
         		tourismAnswerVotesService.save(tourismAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
         			TourismAnswers tourismAnswers; 
             		tourismAnswers =  tourismAnswersService.findAnswer(answerId);
             		
             		if (tourismAnswers.getFlagCount() == 1) {
             			tourismAnswers.setFlagCount(0);
             		}
             		
             		else {
             			tourismAnswers.setFlagCount(1);
             		}
             		
             		tourismAnswersService.update(tourismAnswers);
             		
             		tourismAnswerVotes.setTourismAnswers(tourismAnswers);
             		tourismAnswerVotes.setRealUser(realUser);
             		if(tourismAnswerVotes.isFlag() == true) {
             			tourismAnswerVotes.setFlag(false);
             		}
             		
             		else {
             			tourismAnswerVotes.setFlag(true);
             		}
             		tourismAnswerVotesService.update(tourismAnswerVotes);
         		}
         		
     			
 		}
     	
     	else if(role.equals("medicine")) {
     		
     		
     		MedicineAnswerVotes medicineAnswerVotes;
     		medicineAnswerVotes = medicineAnswerVotesService.checkEntry(email,answerId);
     		
     		
     		
     		if(medicineAnswerVotes == null) {
     			
     			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
         		int newValue  = medicineAnswers.getFlagCount() + 1;
         		medicineAnswers.setFlagCount(newValue);
         		medicineAnswersService.update(medicineAnswers);
         		
         		medicineAnswerVotes = new MedicineAnswerVotes();
         		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
         		medicineAnswerVotes.setRealUser(realUser);
         		medicineAnswerVotes.setFlag(true);
         		medicineAnswerVotesService.save(medicineAnswerVotes);
         		
     		}
     		
     		
     		else {
         			
     			MedicineAnswers medicineAnswers; 
     			medicineAnswers =  medicineAnswersService.findAnswer(answerId);
             		
             		if (medicineAnswers.getFlagCount() == 1) {
             			medicineAnswers.setFlagCount(0);
             		}
             		
             		else {
             			medicineAnswers.setFlagCount(1);
             		}
             		
             		medicineAnswersService.update(medicineAnswers);
             		
             		medicineAnswerVotes.setMedicineAnswers(medicineAnswers);
             		medicineAnswerVotes.setRealUser(realUser);
             		if(medicineAnswerVotes.isFlag() == true) {
             			medicineAnswerVotes.setFlag(false);
             		}
             		
             		else {
             			medicineAnswerVotes.setFlag(true);
             		}
             		medicineAnswerVotesService.update(medicineAnswerVotes);
         		}
         		
     			
 		}
     	
     	else if(role.equals("environment")) {
		
		
     		EnvironmentAnswerVotes environmentAnswerVotes;
     		environmentAnswerVotes = environmentAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(environmentAnswerVotes == null) {
			
			
			EnvironmentAnswers environmentAnswers; 
			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
 		int newValue  = environmentAnswers.getFlagCount() + 1;
 		environmentAnswers.setFlagCount(newValue);
 		environmentAnswersService.update(environmentAnswers);
 		
 		environmentAnswerVotes = new EnvironmentAnswerVotes();
 		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
 		environmentAnswerVotes.setRealUser(realUser);
 		environmentAnswerVotes.setFlag(true);
 		environmentAnswerVotesService.save(environmentAnswerVotes);
 		
		}
		
		
		else {
 			
			EnvironmentAnswers environmentAnswers; 
			environmentAnswers =  environmentAnswersService.findAnswer(answerId);
     		
     		if (environmentAnswers.getFlagCount() == 1) {
     			environmentAnswers.setFlagCount(0);
     		}
     		
     		else {
     			environmentAnswers.setFlagCount(1);
     		}
     		
     		environmentAnswersService.update(environmentAnswers);
     		
     		environmentAnswerVotes.setEnvironmentAnswers(environmentAnswers);
     		environmentAnswerVotes.setRealUser(realUser);
     		if(environmentAnswerVotes.isFlag() == true) {
     			environmentAnswerVotes.setFlag(false);
     		}
     		
     		else {
     			environmentAnswerVotes.setFlag(true);
     		}
     		environmentAnswerVotesService.update(environmentAnswerVotes);
 		}
 		
			
		}

     	else if(role.equals("languages")) {
		
		
     		LanguagesAnswerVotes languagesAnswerVotes;
     		languagesAnswerVotes = languagesAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(languagesAnswerVotes == null) {
			
			
			LanguagesAnswers languagesAnswers; 
			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
 		int newValue  = languagesAnswers.getFlagCount() + 1;
 		languagesAnswers.setFlagCount(newValue);
 		languagesAnswersService.update(languagesAnswers);
 		
 		languagesAnswerVotes = new LanguagesAnswerVotes();
 		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
 		languagesAnswerVotes.setRealUser(realUser);
 		languagesAnswerVotes.setFlag(true);
 		languagesAnswerVotesService.save(languagesAnswerVotes);
 		
		}
		
		
		else {
 			
			LanguagesAnswers languagesAnswers; 
			languagesAnswers =  languagesAnswersService.findAnswer(answerId);
     		
     		if (languagesAnswers.getFlagCount() == 1) {
     			languagesAnswers.setFlagCount(0);
     		}
     		
     		else {
     			languagesAnswers.setFlagCount(1);
     		}
     		
     		languagesAnswersService.update(languagesAnswers);
     		
     		languagesAnswerVotes.setLanguagesAnswers(languagesAnswers);
     		languagesAnswerVotes.setRealUser(realUser);
     		if(languagesAnswerVotes.isFlag() == true) {
     			languagesAnswerVotes.setFlag(false);
     		}
     		
     		else {
     			languagesAnswerVotes.setFlag(true);
     		}
     		languagesAnswerVotesService.update(languagesAnswerVotes);
 		}
 		
			
		}

     	else if(role.equals("business")) {
		
		
     		BusinessAnswerVotes businessAnswerVotes;
     		businessAnswerVotes = businessAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(businessAnswerVotes == null) {
			
			
			BusinessAnswers businessAnswers; 
			businessAnswers =  businessAnswersService.findAnswer(answerId);
 		int newValue  = businessAnswers.getFlagCount() + 1;
 		businessAnswers.setFlagCount(newValue);
 		businessAnswersService.update(businessAnswers);
 		
 		businessAnswerVotes = new BusinessAnswerVotes();
 		businessAnswerVotes.setBusinessAnswers(businessAnswers);
 		businessAnswerVotes.setRealUser(realUser);
 		businessAnswerVotes.setFlag(true);
 		businessAnswerVotesService.save(businessAnswerVotes);
 		
		}
		
		
		else {
 			
			BusinessAnswers businessAnswers; 
			businessAnswers =  businessAnswersService.findAnswer(answerId);
     		
     		if (businessAnswers.getFlagCount() == 1) {
     			businessAnswers.setFlagCount(0);
     		}
     		
     		else {
     			businessAnswers.setFlagCount(1);
     		}
     		
     		businessAnswersService.update(businessAnswers);
     		
     		businessAnswerVotes.setBusinessAnswers(businessAnswers);
     		businessAnswerVotes.setRealUser(realUser);
     		if(businessAnswerVotes.isFlag() == true) {
     			businessAnswerVotes.setFlag(false);
     		}
     		
     		else {
     			businessAnswerVotes.setFlag(true);
     		}
     		businessAnswerVotesService.update(businessAnswerVotes);
 		}
 		
			
		}

     	else if(role.equals("arts")) {
		
		
		ArtsAnswerVotes artsAnswerVotes;
		artsAnswerVotes = artsAnswerVotesService.checkEntry(email,answerId);
		
		
		
		if(artsAnswerVotes == null) {
			
			
			ArtsAnswers artsAnswers; 
			artsAnswers =  artsAnswersService.findAnswer(answerId);
 		int newValue  = artsAnswers.getFlagCount() + 1;
 		artsAnswers.setFlagCount(newValue);
 		artsAnswersService.update(artsAnswers);
 		
 		artsAnswerVotes = new ArtsAnswerVotes();
 		artsAnswerVotes.setArtsAnswers(artsAnswers);
 		artsAnswerVotes.setRealUser(realUser);
 		artsAnswerVotes.setFlag(true);
 		artsAnswerVotesService.save(artsAnswerVotes);
 		
		}
		
		
		else {
 			
 			ArtsAnswers artsAnswers; 
 			artsAnswers =  artsAnswersService.findAnswer(answerId);
     		
     		if (artsAnswers.getFlagCount() == 1) {
     			artsAnswers.setFlagCount(0);
     		}
     		
     		else {
     			artsAnswers.setFlagCount(1);
     		}
     		
     		artsAnswersService.update(artsAnswers);
     		
     		artsAnswerVotes.setArtsAnswers(artsAnswers);
     		artsAnswerVotes.setRealUser(realUser);
     		if(artsAnswerVotes.isFlag() == true) {
     			artsAnswerVotes.setFlag(false);
     		}
     		
     		else {
     			artsAnswerVotes.setFlag(true);
     		}
     		artsAnswerVotesService.update(artsAnswerVotes);
 		}
 		
			
		}

   
     	
     	ModelAndView modelAndView = new ModelAndView("answer");
     	return modelAndView;
   
    }
    
    
    @RequestMapping(value ="/mostlikedquestions" , method = RequestMethod.GET)
    public ModelAndView mostLikedQuestions() {
     ModelAndView modelAndView = new ModelAndView("mostlikedquestions");
         
        RealUser realUser;
         
        String email = getCurrentUser().getUsername();
     	realUser = realUserService.findByEmail(email);
     	String userName = realUser.getUserName();
     	String role = realUser.getRole();
     	modelAndView.addObject("email",email);
     	modelAndView.addObject("userName",userName);
     	modelAndView.addObject("role",role);
     	
     	
     	
     	if(role.equals("tourism")) {
     	        List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
     	        Collections.sort(list);
     			modelAndView.addObject("list",list);
     			List<TourismAnswers> newList = new ArrayList<>();
     			List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(email);
     			
     			for(TourismQuestions question : list1) {
     				
     				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
     				newList.addAll(list2);
     				
     			}
     			
     			modelAndView.addObject("tourismNotifyList", newList);
         }
     	        
     	else if(role.equals("medicine")) {
 	        	List<MedicineQuestions> list = medicineQuestionsService.pullAllQuestions();
 	      		modelAndView.addObject("list",list);
 	      		List<MedicineAnswers> newList = new ArrayList<>();
     			List<MedicineQuestions> list1 = medicineQuestionsService.getQuestions(email);
     			
     			for(MedicineQuestions question : list1) {
     				
     				List<MedicineAnswers> list2 = medicineAnswersService.pullAllAnswers(question.getQuestionId());
     				newList.addAll(list2);
     				
     			}
     			
     			modelAndView.addObject("medicineNotifyList", newList);
         }
     	
     	else if(role.equals("arts")) {
         	List<ArtsQuestions> list = artsQuestionsService.pullAllQuestions();
       		modelAndView.addObject("list",list);
       		List<ArtsAnswers> newList = new ArrayList<>();
 			
 			List<ArtsQuestions> list1 = artsQuestionsService.getQuestionList(email);
 			
 			for(ArtsQuestions question : list1) {
 				
 				List<ArtsAnswers> list2 = artsAnswersService.pullAllAnswers(question.getQuestionId());
 				newList.addAll(list2);
 				
 			}
 			
 			modelAndView.addObject("artsNotifyList", newList);
     	}
     	
     	else if(role.equals("business")) {
         	List<BusinessQuestions> list = businessQuestionsService.pullAllQuestions();
       		modelAndView.addObject("list",list);
       		List<BusinessAnswers> newList = new ArrayList<>();
 			
 			List<BusinessQuestions> list1 = businessQuestionsService.getQuestionList(email);
 			
 			for(BusinessQuestions question : list1) {
 				
 				List<BusinessAnswers> list2 = businessAnswersService.pullAllAnswers(question.getQuestionId());
 				newList.addAll(list2);
 				
 			}
 			
 			modelAndView.addObject("businessNotifyList", newList);
     	}
     	
     	else if(role.equals("languages")) {
         	List<LanguagesQuestions> list = languagesQuestionsService.pullAllQuestions();
       		modelAndView.addObject("list",list);
       		List<LanguagesAnswers> newList = new ArrayList<>();
 			
 			List<LanguagesQuestions> list1 = languagesQuestionsService.getQuestionList(email);
 			
 			for(LanguagesQuestions question : list1) {
 				
 				List<LanguagesAnswers> list2 = languagesAnswersService.pullAllAnswers(question.getQuestionId());
 				newList.addAll(list2);
 				
 			}
 			
 			modelAndView.addObject("languagesNotifyList", newList);
     	}
     	
     	else if(role.equals("environment")) {
         	List<EnvironmentQuestions> list = environmentQuestionsService.pullAllQuestions();
       		modelAndView.addObject("list",list);
       		List<EnvironmentAnswers> newList = new ArrayList<>();
 			
 			List<EnvironmentQuestions> list1 = environmentQuestionsService.getQuestionList(email);
 			
 			for(EnvironmentQuestions question : list1) {
 				
 				List<EnvironmentAnswers> list2 = environmentAnswersService.pullAllAnswers(question.getQuestionId());
 				newList.addAll(list2);
 				
 			}
 			
 			modelAndView.addObject("environmentNotifyList", newList);
     	}
     	
         return modelAndView;
	
   
    } 
    
    @RequestMapping(value ="/bookmarkquestion" , method = RequestMethod.POST)
    public ModelAndView commentFlag(@RequestParam("questionId") long questionId ,@RequestParam("community") String community ) {
    	
    	ModelAndView model = new ModelAndView("home");
    	
    	
    	String email = getCurrentUser().getUsername();
    	RealUser realUser;
     	realUser = realUserService.findByEmail(email);
    	
    	if(community.equals("tourism")) {
    		 TourismQuestionVotes tourismQuestionVotes;
    		 tourismQuestionVotes = tourismQuestionVotesService.checkEntry(email, questionId);
    		 
    		 if(tourismQuestionVotes == null) {
    			 
    			TourismQuestions tourismQuestions; 
    			tourismQuestions =  tourismQuestionsService.findQuestion(questionId);
          		int newValue  = tourismQuestions.getBookmarkCount() + 1;
          		tourismQuestions.setBookmarkCount(newValue);
          		tourismQuestionsService.update(tourismQuestions);
          		
          		tourismQuestionVotes = new TourismQuestionVotes();
          		tourismQuestionVotes.setTourismQuestions(tourismQuestions);
          		tourismQuestionVotes.setRealUser(realUser);
          		tourismQuestionVotes.setBookmark(true);
          		tourismQuestionVotesService.save(tourismQuestionVotes);
          		
          		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
            	bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
            	bookmarkQuestions.setQuestionId(questionId);
            	bookmarkQuestions.setCommuity(community);
            	bookmarkQuestions.setRealUser(realUser);
            	bookmarkQuestionsService.save(bookmarkQuestions);
            	
          		
      		}
      		
      		
      		else {
          			
      			    TourismQuestions tourismQuestions; 
      			    tourismQuestions = tourismQuestionsService.findQuestion(questionId);
              		
              		if (tourismQuestions.getBookmarkCount() == 1) {
              			tourismQuestions.setBookmarkCount(0);
              			tourismQuestionsService.update(tourismQuestions);
              			tourismQuestionVotes.setBookmark(false);
              			tourismQuestionVotesService.update(tourismQuestionVotes);
              			bookmarkQuestionsService.delete(questionId,community);
              			
              		}
              		
              		else {
              			tourismQuestions.setBookmarkCount(1);
              			tourismQuestionsService.update(tourismQuestions);
              			tourismQuestionVotes.setBookmark(false);
              			tourismQuestionVotesService.update(tourismQuestionVotes);
              			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
                    	bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
                    	bookmarkQuestions.setQuestionId(questionId);
                    	bookmarkQuestions.setCommuity(community);
                    	bookmarkQuestions.setRealUser(realUser);
                    	bookmarkQuestionsService.save(bookmarkQuestions);
              		}
              		
              	
          		}
          		
      			
  		}
    	
    	

    	else if(community.equals("medicine")) {
    		MedicineQuestionVotes medicineQuestionVotes;
    		medicineQuestionVotes = medicineQuestionVotesService.checkEntry(email, questionId);
   		 
   		 if(medicineQuestionVotes == null) {
   			 
   			MedicineQuestions medicineQuestions; 
   			medicineQuestions =  medicineQuestionsService.findQuestion(questionId);
         		int newValue  = medicineQuestions.getBookmarkCount() + 1;
         		medicineQuestions.setBookmarkCount(newValue);
         		medicineQuestionsService.update(medicineQuestions);
         		
         		medicineQuestionVotes = new MedicineQuestionVotes();
         		medicineQuestionVotes.setMedicineQuestions(medicineQuestions);
         		medicineQuestionVotes.setRealUser(realUser);
         		medicineQuestionVotes.setBookmark(true);
         		medicineQuestionVotesService.save(medicineQuestionVotes);
         		
         		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
         		bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
         		bookmarkQuestions.setQuestionId(questionId);
         		bookmarkQuestions.setCommuity(community);
         		bookmarkQuestions.setRealUser(realUser);
         		bookmarkQuestionsService.save(bookmarkQuestions);
           	
         		
     		}
     		
     		
     		else {
         			
     			MedicineQuestions medicineQuestions; 
     			medicineQuestions = medicineQuestionsService.findQuestion(questionId);
             		
             		if (medicineQuestions.getBookmarkCount() == 1) {
             			medicineQuestions.setBookmarkCount(0);
             			medicineQuestionsService.update(medicineQuestions);
             			medicineQuestionVotes.setBookmark(false);
             			medicineQuestionVotesService.update(medicineQuestionVotes);
             			bookmarkQuestionsService.delete(questionId,community);
             			
             		}
             		
             		else {
             			medicineQuestions.setBookmarkCount(1);
             			medicineQuestionsService.update(medicineQuestions);
             			medicineQuestionVotes.setBookmark(false);
             			medicineQuestionVotesService.update(medicineQuestionVotes);
             			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
             			bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
             			bookmarkQuestions.setQuestionId(questionId);
             			bookmarkQuestions.setCommuity(community);
             			bookmarkQuestions.setRealUser(realUser);
             			bookmarkQuestionsService.save(bookmarkQuestions);
             		}
             		
             	
         		}
         		
     			
 		}
    	
    	else if(community.equals("environment")) {
    		EnvironmentQuestionVotes environmentQuestionVotes;
    		environmentQuestionVotes = environmentQuestionVotesService.checkEntry(email, questionId);
   		 
   		 if(environmentQuestionVotes == null) {
   			 
   			EnvironmentQuestions environmentQuestions; 
   			environmentQuestions =  environmentQuestionsService.findQuestion(questionId);
         		int newValue  = environmentQuestions.getBookmarkCount() + 1;
         		environmentQuestions.setBookmarkCount(newValue);
         		environmentQuestionsService.update(environmentQuestions);
         		
         		environmentQuestionVotes = new EnvironmentQuestionVotes();
         		environmentQuestionVotes.setEnvironmentQuestions(environmentQuestions);
         		environmentQuestionVotes.setRealUser(realUser);
         		environmentQuestionVotes.setBookmark(true);
         		environmentQuestionVotesService.save(environmentQuestionVotes);
         		
         		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
         		bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
         		bookmarkQuestions.setQuestionId(questionId);
         		bookmarkQuestions.setCommuity(community);
         		bookmarkQuestions.setRealUser(realUser);
         		bookmarkQuestionsService.save(bookmarkQuestions);
           	
         		
     		}
     		
     		
     		else {
         			
     			EnvironmentQuestions environmentQuestions; 
     			environmentQuestions = environmentQuestionsService.findQuestion(questionId);
             		
             		if (environmentQuestions.getBookmarkCount() == 1) {
             			environmentQuestions.setBookmarkCount(0);
             			environmentQuestionsService.update(environmentQuestions);
             			environmentQuestionVotes.setBookmark(false);
             			environmentQuestionVotesService.update(environmentQuestionVotes);
             			bookmarkQuestionsService.delete(questionId,community);
             			
             		}
             		
             		else {
             			environmentQuestions.setBookmarkCount(1);
             			environmentQuestionsService.update(environmentQuestions);
             			environmentQuestionVotes.setBookmark(false);
             			environmentQuestionVotesService.update(environmentQuestionVotes);
             			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
             			bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
             			bookmarkQuestions.setQuestionId(questionId);
             			bookmarkQuestions.setCommuity(community);
             			bookmarkQuestions.setRealUser(realUser);
             			bookmarkQuestionsService.save(bookmarkQuestions);
             		}
             		
             	
         		}
         		
     			
 		}
    	
    	else if(community.equals("languages")) {
    		LanguagesQuestionVotes languagesQuestionVotes;
    		languagesQuestionVotes = languagesQuestionVotesService.checkEntry(email, questionId);
   		 
   		 if(languagesQuestionVotes == null) {
   			 
   			LanguagesQuestions languagesQuestions; 
   			languagesQuestions =  languagesQuestionsService.findQuestion(questionId);
         		int newValue  = languagesQuestions.getBookmarkCount() + 1;
         		languagesQuestions.setBookmarkCount(newValue);
         		languagesQuestionsService.update(languagesQuestions);
         		
         		languagesQuestionVotes = new LanguagesQuestionVotes();
         		languagesQuestionVotes.setLanguagesQuestions(languagesQuestions);
         		languagesQuestionVotes.setRealUser(realUser);
         		languagesQuestionVotes.setBookmark(true);
         		languagesQuestionVotesService.save(languagesQuestionVotes);
         		
         		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
         		bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
         		bookmarkQuestions.setQuestionId(questionId);
         		bookmarkQuestions.setCommuity(community);
         		bookmarkQuestions.setRealUser(realUser);
         		bookmarkQuestionsService.save(bookmarkQuestions);
           	
         		
     		}
     		
     		
     		else {
         			
     			LanguagesQuestions languagesQuestions; 
     			languagesQuestions = languagesQuestionsService.findQuestion(questionId);
             		
             		if (languagesQuestions.getBookmarkCount() == 1) {
             			languagesQuestions.setBookmarkCount(0);
             			languagesQuestionsService.update(languagesQuestions);
             			languagesQuestionVotes.setBookmark(false);
             			languagesQuestionVotesService.update(languagesQuestionVotes);
             			bookmarkQuestionsService.delete(questionId,community);
             			
             		}
             		
             		else {
             			languagesQuestions.setBookmarkCount(1);
             			languagesQuestionsService.update(languagesQuestions);
             			languagesQuestionVotes.setBookmark(false);
             			languagesQuestionVotesService.update(languagesQuestionVotes);
             			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
             			bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
             			bookmarkQuestions.setQuestionId(questionId);
             			bookmarkQuestions.setCommuity(community);
             			bookmarkQuestions.setRealUser(realUser);
             			bookmarkQuestionsService.save(bookmarkQuestions);
             		}
             		
             	
         		}
         		
     			
 		}
    	
    	else if(community.equals("business")) {
    		BusinessQuestionVotes businessQuestionVotes;
    		businessQuestionVotes = businessQuestionVotesService.checkEntry(email, questionId);
   		 
   		 if(businessQuestionVotes == null) {
   			 
   			BusinessQuestions businessQuestions; 
   			businessQuestions =  businessQuestionsService.findQuestion(questionId);
         		int newValue  = businessQuestions.getBookmarkCount() + 1;
         		businessQuestions.setBookmarkCount(newValue);
         		businessQuestionsService.update(businessQuestions);
         		
         		businessQuestionVotes = new BusinessQuestionVotes();
         		businessQuestionVotes.setBusinessQuestions(businessQuestions);
         		businessQuestionVotes.setRealUser(realUser);
         		businessQuestionVotes.setBookmark(true);
         		businessQuestionVotesService.save(businessQuestionVotes);
         		
         		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
         		bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
         		bookmarkQuestions.setQuestionId(questionId);
         		bookmarkQuestions.setCommuity(community);
         		bookmarkQuestions.setRealUser(realUser);
         		bookmarkQuestionsService.save(bookmarkQuestions);
           	
         		
     		}
     		
     		
     		else {
         			
     			BusinessQuestions businessQuestions; 
     			businessQuestions = businessQuestionsService.findQuestion(questionId);
             		
             		if (businessQuestions.getBookmarkCount() == 1) {
             			businessQuestions.setBookmarkCount(0);
             			businessQuestionsService.update(businessQuestions);
             			businessQuestionVotes.setBookmark(false);
             			businessQuestionVotesService.update(businessQuestionVotes);
             			bookmarkQuestionsService.delete(questionId,community);
             			
             		}
             		
             		else {
             			businessQuestions.setBookmarkCount(1);
             			businessQuestionsService.update(businessQuestions);
             			businessQuestionVotes.setBookmark(false);
             			businessQuestionVotesService.update(businessQuestionVotes);
             			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
             			bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
             			bookmarkQuestions.setQuestionId(questionId);
             			bookmarkQuestions.setCommuity(community);
             			bookmarkQuestions.setRealUser(realUser);
             			bookmarkQuestionsService.save(bookmarkQuestions);
             		}
             		
             	
         		}
         		
     			
 		}
    	
    	else if(community.equals("arts")) {
   		 ArtsQuestionVotes artsQuestionVotes;
   		artsQuestionVotes = artsQuestionVotesService.checkEntry(email, questionId);
   		 
   		 if(artsQuestionVotes == null) {
   			 
   			ArtsQuestions artsQuestions; 
   			artsQuestions =  artsQuestionsService.findQuestion(questionId);
         		int newValue  = artsQuestions.getBookmarkCount() + 1;
         		artsQuestions.setBookmarkCount(newValue);
         		artsQuestionsService.update(artsQuestions);
         		
         		artsQuestionVotes = new ArtsQuestionVotes();
         		artsQuestionVotes.setArtsQuestions(artsQuestions);
         		artsQuestionVotes.setRealUser(realUser);
         		artsQuestionVotes.setBookmark(true);
         		artsQuestionVotesService.save(artsQuestionVotes);
         		
         		BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
         		bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
         		bookmarkQuestions.setQuestionId(questionId);
         		bookmarkQuestions.setCommuity(community);
         		bookmarkQuestions.setRealUser(realUser);
         		bookmarkQuestionsService.save(bookmarkQuestions);
           	
         		
     		}
     		
     		
     		else {
         			
 			    ArtsQuestions artsQuestions; 
 			   artsQuestions = artsQuestionsService.findQuestion(questionId);
             		
             		if (artsQuestions.getBookmarkCount() == 1) {
             			artsQuestions.setBookmarkCount(0);
             			artsQuestionsService.update(artsQuestions);
             			artsQuestionVotes.setBookmark(false);
             			artsQuestionVotesService.update(artsQuestionVotes);
             			bookmarkQuestionsService.delete(questionId,community);
             			
             		}
             		
             		else {
             			artsQuestions.setBookmarkCount(1);
             			artsQuestionsService.update(artsQuestions);
             			artsQuestionVotes.setBookmark(false);
             			artsQuestionVotesService.update(artsQuestionVotes);
             			BookmarkQuestions bookmarkQuestions = new BookmarkQuestions();
             			bookmarkQuestions.setId(GenerateRandomNumber.generateNumber());
             			bookmarkQuestions.setQuestionId(questionId);
             			bookmarkQuestions.setCommuity(community);
             			bookmarkQuestions.setRealUser(realUser);
             			bookmarkQuestionsService.save(bookmarkQuestions);
             		}
             		
             	
         		}
         		
     			
 		}
   	
    	return model;
    }
    
    
    @RequestMapping(value ="/viewbookmarkedquestions" , method = RequestMethod.GET)
    public ModelAndView viewBookmarkedQuestions() {
    	
    	ModelAndView model = new ModelAndView("viewbookmarkedquestions");
    	
    	String email = getCurrentUser().getUsername();
    	RealUser realUser;
     	realUser = realUserService.findByEmail(email);
     	
     	List<Object> realList = new ArrayList<>();
     	
     	List<BookmarkQuestions> list = bookmarkQuestionsService.findByEmail(email);
     	
     	for(BookmarkQuestions obj : list) {
     		if(obj.getCommuity().equals("tourism")) {
     			TourismQuestions tourismQuestions;
     			tourismQuestions = tourismQuestionsService.findQuestion(obj.getQuestionId());
     			realList.add(tourismQuestions);
     		}
     		
     		else if(obj.getCommuity().equals("medicine")) {
     			MedicineQuestions medicineQuestions;
     			medicineQuestions = medicineQuestionsService.findQuestion(obj.getQuestionId());
     			realList.add(medicineQuestions);
     		}
     	}
     	
     	model.addObject("list", realList);
     	model.addObject("role",realUser.getRole());
     
     	return model;
    	
    }
    
    
    @RequestMapping(value ="/removequestion" , method = RequestMethod.POST)
    public ModelAndView removequestion(@RequestParam("questionId") long questionId) {
    	
    	ModelAndView model = new ModelAndView("home");
    	
    	String email = getCurrentUser().getUsername();
    	RealUser realUser;
     	realUser = realUserService.findByEmail(email);
     	
     	if(realUser.getRole().equals("tourism")) {
     		
     		TourismQuestions tourismQuestions;
     		
     		tourismQuestions = tourismQuestionsService.findQuestion(questionId);
     		                   bookmarkQuestionsService.delete(questionId, "tourism");
     		                   tourismAnswerVotesService.delete(questionId);
     		                   tourismAnswersService.remove(questionId);
     		                   tourismQuestionVotesService.delete(questionId);
     		                   tourismQuestionsService.delete(tourismQuestions);
     		                   
     
     	}
     	
     	
     	return model;
    	
    }
    
    
    @RequestMapping(value ="/editquestion" , method = RequestMethod.POST)
    public ModelAndView editquestion(@RequestParam("questionId") long questionId ,@RequestParam("editQuestion") String editQuestion ) {
    	
    	
    	ModelAndView model = new ModelAndView("home");
    	
    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    
    	
    	
    
		if(realUser.getRole().equals("tourism")) {
    	
	    	TourismQuestions tourismQuestions = tourismQuestionsService.findQuestion(questionId);
	    	tourismQuestions.setQuestion(editQuestion);
	    	tourismQuestions.setTimeAsked(new Date());
	    	
	    	tourismQuestionsService.update(tourismQuestions);
	    	
	    	List<TourismAnswers> newList = new ArrayList<>();
			List<TourismQuestions> list1 = tourismQuestionsService.getQuestions(email);
			
			for(TourismQuestions question : list1) {
				
				List<TourismAnswers> list2 = tourismAnswersService.pullAllAnswers(question.getQuestionId());
				newList.addAll(list2);
				
			}
	    	
	    	
	    	
	    	
	    	List<TourismQuestions> list = tourismQuestionsService.pullAllQuestions();
	    	model.addObject("list",list);
	    	model.addObject("tourismNotifyList",newList);
	    	model.addObject("principalEmail", email);
	    	
	    	model.addObject("role",realUser.getRole());
	        model.addObject("userName", tourismQuestions.getRealUser().getUserName());
	        model.addObject("loggedInUserEmail",email);
	        model.addObject("email",email);
	        model.addObject("loggedInUserName",realUser.getUserName());
	     
		}
		
     	
     	return model;
    	
    }
    
    
  
   @RequestMapping(value ="/autocomplete" , method = RequestMethod.GET)
    public @ResponseBody List<Object> autoComplete(@RequestParam("tagName") String tagName) {
    	
	   
    	
    	
    	RealUser realUser;
        
        String email = getCurrentUser().getUsername();
    	realUser = realUserService.findByEmail(email);
    	
    	tagName = tagName.toLowerCase();
        
        String[] words = tagName.split(" ");
        int length = words.length;
        List<Object> questionsList = new ArrayList<>();
        
        if(realUser.getRole().equals("tourism")) {
        
        	 List<TourismQuestions> tourismQuestionsList = tourismQuestionsService.pullAllQuestions();
             
             
             for(TourismQuestions tourismQuestionsObject  : tourismQuestionsList ) {
            	 
            	 int count = 0;
            	 String otherString = tourismQuestionsObject.getQuestion().toLowerCase();
            	 
            	 for(String word : words) {
            		 if(otherString.contains(word)) {
            			 count++;
            		 }
            	 }
            	 
            	 
            	 if(count >= length/1.5) {
            		 questionsList.add(tourismQuestionsObject);
            	 }
            	 
             }
             
             
        }
        
        
        
        
        if(realUser.getRole().equals("medicine")) {
            
       	 List<MedicineQuestions> medicineQuestionsList = medicineQuestionsService.pullAllQuestions();
            
            
            for(MedicineQuestions medicineQuestionsObject  : medicineQuestionsList ) {
           	 
           	 int count = 0;
           	 String otherString = medicineQuestionsObject.getQuestion().toLowerCase();
           	 
           	 for(String word : words) {
           		 if(otherString.contains(word)) {
           			 count++;
           		 }
           	 }
           	 
           	 
           	 if(count >= length/1.5) {
           		 questionsList.add(medicineQuestionsObject);
           	 }
           	 
            }
            
            
       }
        
        
        return questionsList;
       
        
     
    	
    	
    
     }
    
   
   
    
    
}	
    
  
 
    

